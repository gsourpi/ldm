from matplotlib import pyplot as plt
from matplotlib.ticker import EngFormatter
import numpy as np
from pathlib import Path 

file_path ="/home/gsourpi/Desktop/git/ldm/Cronologic_xHPTDC8/Data_acquisition/crono_1_min_v11.txt"

with open(file_path, "r") as file:
    channel = []
    timestamps = []

    for line in file:
        parts = line.split()
        num = int(parts[0])
        timestamp = int(parts[1])

        # Append to the appropriate list
        channel.append(num)
        timestamps.append(timestamp)

timestamps_trigger = [ts for ts, ch in zip(timestamps, channel) if ch == 0]
timestamps_signal = [ts for ts, ch in zip(timestamps, channel) if ch == 1]

plt.scatter(timestamps_trigger, [0]*len(timestamps_trigger), color='red', label='Trigger')
plt.scatter(timestamps_signal, [2]*len(timestamps_signal), color='blue', label='Input')

# Uncomment to plot dotted vertical lines from the trigger timestamps
# for ts in timestamps_trigger:
#     plt.vlines(ts, ymin=0, ymax=2, colors='red', linestyle='dotted')

plt.xlabel('Timestamps')
plt.ylabel('Channel')
plt.title('Scatter Plot of Channels and Timestamps')

plt.legend()
plt.show(block = True)
