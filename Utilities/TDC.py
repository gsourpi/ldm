import time 
import pickle 
import matplotlib
from matplotlib import pyplot as plt 
import numpy as np
#from line_profiler import LineProfiler
from matplotlib.ticker import EngFormatter, FuncFormatter
from scipy.optimize import curve_fit
from Utilities.dataStorage import writeLists
import sys
import math 

def gaussian(x, mu, sigma, A):
    return A * np.exp(-0.5 * ((x - mu) / sigma)**2)

def meanFromGaussianFit(bins, counts, binWindowSize): 
    bins = np.array(bins)
    bin_centers = (bins[:-1] + bins[1:]) / 2
    bin_centers = np.append(bin_centers, (bins[-1] + bins[-2]) / 2)

    # Initial guesses for parameters [mu, sigma, A]
    p0 = [np.mean(bin_centers), binWindowSize/2 , np.max(counts)]
    
    counts = counts.tolist()
    counts.append(0)
    try:
        popt = curve_fit(gaussian, bin_centers, counts, p0=p0, maxfev=800)[0]
        print("Mean of the fitted distribution:", popt[0])
        
        plt.figure(figsize=(8,6))
        plt.hist(bin_centers, bins = bins, weights = counts, edgecolor = 'black', alpha = 0.6, label='Histogram')
        x_curve = np.linspace(np.min(bin_centers), np.max(bin_centers), 100)
        y_curve = gaussian(x_curve, *popt)
        plt.plot(x_curve, y_curve, 'r-', label='Gaussian fit')
        
        plt.xlabel('Time (ps)')
        plt.ylabel('Counts')
        plt.title('Histogram')
        plt.legend()
        plt.grid(True)
        plt.show(block = True)
        
        return int(popt[0])
    
    except RuntimeError as e:
        print(f"Error: {e}")
        sys.exit("Program terminated. Choose a more suitable bin window size.")

def meanCalculation(binWindow): 
    return np.mean(binWindow)
        
def getBinsOfCountPeaks(preallocateCounts, binsWithCounts, countThreshold): 
        binsOfCountPeaks = []

        for j, bin in enumerate(binsWithCounts):
            is_maxima = preallocateCounts[bin] > preallocateCounts[bin-1] and preallocateCounts[bin] > preallocateCounts[bin+1]
            if is_maxima and preallocateCounts[bin] > countThreshold: 
                binsOfCountPeaks.append([j,bin]) # j in the bins with counts list 

            print("The number of hits are: ", len(binsOfCountPeaks) )
        return binsOfCountPeaks

def TDCGroupHits(preallocateCounts, binWindowSize, binWindowStep):
    hits_lists, binWindow =  [], []
    optimalCenterBinFound = False
    preallocateCounts_mean = np.mean(preallocateCounts)
    
    if preallocateCounts.any(): 
        indexes = np.where(preallocateCounts)[0] # the bins where the counts are non zero 
        binsWithCounts = list(indexes) 
        num_windows = math.ceil(len(binsWithCounts)/binWindowSize) # number of times we will shift the windows to the right 
        if num_windows == 0: 
            print(f"Number of bins with counts: {len(binsWithCounts)}. Window size (in bins) is too large. Choose a smaller window size. \n")
    for i in range(num_windows):
        # Apply gaussian fit until the mean computed manually and the mean from the gaussian are approx equal 
        if optimalCenterBinFound == False: 
            if i ==0: 
                binWindow = list(range(binsWithCounts[i]-1, binsWithCounts[i] + binWindowSize ))
                # Initialize the window step counter
                windowStepCounter=0
            else:
                startOfBinWindow = centerBin - binWindowSize//2 + binWindowStep * windowStepCounter
                endOfBinWindow   = centerBin + binWindowSize//2 + binWindowStep * windowStepCounter

                # print('Bin Window: ', startOfBinWindow, ' until ', endOfBinWindow)
                # print('Prealloc: ', preallocateCounts[startOfBinWindow: endOfBinWindow])

                binWindow = list(range(startOfBinWindow, endOfBinWindow)) 
            
            estimatedCenterBin = meanCalculation(binWindow=binWindow)
            fittedCenterBin = meanFromGaussianFit(bins=binWindow, counts=preallocateCounts[binWindow[0]:binWindow[-1]], binWindowSize= binWindowSize)

            print('\nWindow estimated center bin in ps: ' , int(estimatedCenterBin) ) 
            print('Fitted Bin Center in ps: ', int(fittedCenterBin)) 
            
            if abs(estimatedCenterBin - fittedCenterBin) > 100:
                print("The difference is:", abs(estimatedCenterBin - fittedCenterBin))
                centerBin = fittedCenterBin
            else: 
                # Set this as the general bin center and don't do gaussian fit to find the mean again 
                centerBin = int(estimatedCenterBin)
                optimalCenterBinFound = True
                print('\nOptimal center found!')
            
            # Update the new window limits based on the new centered bin
            windowStepCounter = 0 
            if centerBin < binWindowSize//2: 
                startOfBinWindow = 0 
            else: 
                startOfBinWindow = centerBin - binWindowSize//2 + binWindowStep * windowStepCounter
            endOfBinWindow   = centerBin + binWindowSize//2 + binWindowStep * windowStepCounter

        else: 
            startOfBinWindow = centerBin - binWindowSize//2 + binWindowStep * windowStepCounter
            endOfBinWindow   = centerBin + binWindowSize//2 + binWindowStep * windowStepCounter
        
        groupedHit = preallocateCounts[startOfBinWindow: endOfBinWindow]
        
        if max(groupedHit) >= preallocateCounts_mean: # if it is not an empty list and there are at least 2 counts 
            hits_lists.append(list(range(startOfBinWindow, endOfBinWindow)))
        
        # if i % num_windows//2 == 0: 
        #     optimalCenterBinFound = False
        
        # Update window step counter 
        windowStepCounter += 1
        
    print('\nHits were grouped successfully.')
    return hits_lists
