from setuptools import setup, find_packages

setup(
    name='TDC_shared',
    version='1.0',
    packages=find_packages(),
)