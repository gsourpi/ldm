import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
import numpy as np
import scipy 

def plotHist(ax, bins, preallocateCounts, resultsInNanoSecs = False, plotGaussian=False):
    bar_width = bins[1] - bins[0]
    
    if isinstance(preallocateCounts, scipy.sparse.lil_matrix):
        if preallocateCounts.nnz == 0 or len(bins) == 0:
            return
        # Convert to a dense matrix 
        preallocateCounts = preallocateCounts.toarray().squeeze()

    if resultsInNanoSecs: 
        # Convert bins(picosecs) in nanoseconds
        picoToNanoMultiplier = 25 * 1e-3 

        def custom_formatter(x, pos):
            return '{:.2f}'.format(x * picoToNanoMultiplier) # Multiply x by 25 and then by 10^(-3) for the label
        ax.xaxis.set_major_formatter(custom_formatter)

    ax.bar(bins, preallocateCounts, width=bar_width, align='edge') # Add  color='blue' for consistent colouring 

def plotSomeOnSameGraph(data, block = False, startingHit = 0, endingHit = 4, resultsInNanoSecs = False, plotGaussian = False):
    plt.ion()
    fig, ax = plt.subplots(figsize=(8, 6))

    for i, bins in enumerate(data[1]):
        
        if isinstance(data[0], scipy.sparse.lil_matrix):
            plotHist(ax, bins=bins, preallocateCounts=data[0][0,bins[0]:bins[-1] + 1], resultsInNanoSecs = resultsInNanoSecs, plotGaussian=plotGaussian)
        else: 
            plotHist(ax, bins=bins, preallocateCounts=data[0][bins[0]:bins[-1] + 1], resultsInNanoSecs = resultsInNanoSecs, plotGaussian=plotGaussian)
        if i == endingHit: 
            break 
    x_min = min(data[1][startingHit]) - 1
    x_max = max(data[1][endingHit]) + 1 

    plt.xlim(x_min, x_max)
    if isinstance(data[0], scipy.sparse.lil_matrix):
        plt.ylim(0, max(data[0].data.max()))
    else: 
        plt.ylim(0, max(data[0]))
        
    plt.show(block=block)

def plotNumSubPlots(data, block, startingHit = 2, endingHit = 5, resultsInNanoSecs = False, plotGaussian = False):
    plt.ion()
    fig, axs = plt.subplots(endingHit-startingHit+1, 1, figsize=(8, 6))

    # Plot each histogram
    for i, ax in enumerate(axs):
        bins = data[1][i + startingHit]  # Get bins for the current histogram
        if isinstance(data[0], scipy.sparse.lil_matrix):
            plotHist(ax, bins=bins, preallocateCounts=data[0][0,bins[0]:bins[-1] + 1], resultsInNanoSecs = resultsInNanoSecs, plotGaussian=plotGaussian)
        else: 
            plotHist(ax, bins=bins, preallocateCounts=data[0][bins[0]:bins[-1] + 1], resultsInNanoSecs = resultsInNanoSecs, plotGaussian=plotGaussian)
        ax.grid(True)

    plt.show(block=block)

def plotAllOnSameGraph(data, block=True):
    non_zero_indices = data[1][0]
    xdata = non_zero_indices
    ydata = data[0][non_zero_indices]
    
    fig, ax = plt.subplots()
    ax.plot(xdata, ydata)
    plt.show(block=block)
    