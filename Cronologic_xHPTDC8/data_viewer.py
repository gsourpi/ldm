import pickle
from pathlib import Path 

from Utilities.postProcessingPlots import plotHist, plotSomeOnSameGraph, plotAllOnSameGraph, plotNumSubPlots

file_path = "/home/gsourpi/Desktop/git/ldm/crono_output_v11.pkl"

with open(file_path, 'rb') as f:
    data = pickle.load(f)
    # Saved in a file in the form: [ [Counts], [ [Hit 1], [Hit 2], ...] ]
print(len(data[1]))

#plotNumSubPlots(data, block = True, startingHit = 0, endingHit = 1)
#plotSomeOnSameGraph(data, block = True, startingHit = 0, endingHit = 0)
plotAllOnSameGraph(data, block = True)
