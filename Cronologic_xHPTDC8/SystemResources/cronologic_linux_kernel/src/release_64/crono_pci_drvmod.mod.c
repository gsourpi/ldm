#include <linux/module.h>
#define INCLUDE_VERMAGIC
#include <linux/build-salt.h>
#include <linux/elfnote-lto.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;
BUILD_LTO_INFO;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(".gnu.linkonce.this_module") = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section("__versions") = {
	{ 0x2f0167ad, "module_layout" },
	{ 0x6fbff976, "dma_map_sg_attrs" },
	{ 0xf24f1d68, "kmalloc_caches" },
	{ 0xb7477c45, "pci_release_region" },
	{ 0x7aa1756e, "kvfree" },
	{ 0x877092a7, "dma_unmap_sg_attrs" },
	{ 0x4109839, "dma_set_mask" },
	{ 0xb523fb4c, "pci_disable_device" },
	{ 0x837b7b09, "__dynamic_pr_debug" },
	{ 0xfbe215e4, "sg_next" },
	{ 0x52a8c08, "dma_free_attrs" },
	{ 0xa0e01472, "dma_set_coherent_mask" },
	{ 0x6b10bee1, "_copy_to_user" },
	{ 0x5b8239ca, "__x86_return_thunk" },
	{ 0x2a7e3c5c, "pci_set_master" },
	{ 0x3cf2a47b, "misc_register" },
	{ 0x25c843ee, "sg_alloc_table_from_pages_segment" },
	{ 0xe1537255, "__list_del_entry_valid" },
	{ 0xde80cd09, "ioremap" },
	{ 0x4c9d28b0, "phys_base" },
	{ 0x6e013bfa, "dma_alloc_attrs" },
	{ 0x68f31cbd, "__list_add_valid" },
	{ 0x599fb41c, "kvmalloc_node" },
	{ 0x7cd8d75e, "page_offset_base" },
	{ 0xee7e2cda, "pci_select_bars" },
	{ 0xd0da656b, "__stack_chk_fail" },
	{ 0x92997ed8, "_printk" },
	{ 0x5d6a15cb, "pin_user_pages" },
	{ 0xbdfb6dbb, "__fentry__" },
	{ 0x7c732ce4, "pci_unregister_driver" },
	{ 0xa3e49023, "kmem_cache_alloc_trace" },
	{ 0x932464ef, "pcpu_hot" },
	{ 0x14a6f438, "remap_pfn_range" },
	{ 0x49495b35, "unpin_user_pages" },
	{ 0xc5183adc, "__pci_register_driver" },
	{ 0xc8c85086, "sg_free_table" },
	{ 0x656e4a6e, "snprintf" },
	{ 0x4a453f53, "iowrite32" },
	{ 0x98dabf92, "pci_enable_device" },
	{ 0x13c49cc2, "_copy_from_user" },
	{ 0xbb346d4c, "misc_deregister" },
	{ 0xa97befc4, "pci_request_region" },
	{ 0x88db9f48, "__check_object_size" },
};

MODULE_INFO(depends, "");

MODULE_ALIAS("pci:v00001A13d*sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "2EB0AF5A983077829BC2378");
MODULE_INFO(rhelversion, "9.3");
