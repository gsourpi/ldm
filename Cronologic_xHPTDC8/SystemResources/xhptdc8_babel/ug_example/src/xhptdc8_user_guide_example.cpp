// xhptdc8_user_guide_example.cpp : Example application for the xHPTDC8-PCIe

#include <chrono>
#include <thread>
#include <stdio.h>
#include <stdlib.h>
#include "crono_interface.h"
#include "xHPTDC8_interface.h"
#include <fstream> 
#include <iostream>
#include <string>
#include <cmath>

const int MAX_TRYS_TO_READ_HITS = 1000;

const long long int period_of_trigger = round(10e12/3e2) ; // in picoseconds - assuming frequency in Hz

int64_t* last_hit = nullptr;


// utility function to check for error, print error message and exit
int exit_on_fail(int status, const char* message) {
	if (status == XHPTDC8_OK)
		return status;
	printf("%s: %s\n", message, xhptdc8_get_last_error_message(0));

	//If last_hit is not nullptr, it deallocates the memory pointed to by last_hit using delete[]
	if (last_hit != nullptr) {
		delete[] last_hit;
	}
	exit(1);
}


// create a manager object ('params') that provides access to all xHPTDC8s in the system
int initialize_xhptdc8(int buffer_size) {
	// prepare initialization
	xhptdc8_manager_init_parameters params;

	// get that default initialization parameters
	xhptdc8_get_default_init_parameters(&params);

	// customize buffer size
	params.buffer_size = buffer_size;

	// Do the initialization 
	int error_code;
	char* error_msg = NULL;
	error_code = xhptdc8_init(&params);
	exit_on_fail(error_code, error_msg);
	return error_code;
}


int get_device_count() {
	int error_code;
	char* error_msg;

	int device_count = xhptdc8_count_devices(&error_code, (const char**)&error_msg); 
	exit_on_fail(error_code, error_msg);
	return device_count;
}


int configure_xhptdc8(int device_count) {
	xhptdc8_manager_configuration* mgr_cfg = new xhptdc8_manager_configuration;
	// Copies the default configuration to the specified config pointer.
	xhptdc8_get_default_configuration(mgr_cfg);
	
	// configure all devices with an identical configuration
	for (int device_index = 0; device_index < device_count; device_index++) {
		xhptdc8_device_configuration* device_config = &(mgr_cfg->device_configs[device_index]);

		for (int channel_index = 0; channel_index < XHPTDC8_TDC_CHANNEL_COUNT; channel_index++)
		{
			// Set the threshold voltage approx 50% of the height if the input channel signal 
			// Currently a positive pulse with >= +2.36 V is expected
			device_config->trigger_threshold[channel_index] = XHPTDC8_THRESHOLD_P_CMOS;
			// Enable the TDC channels 
			device_config->channel[channel_index].enable = true;
			device_config->channel[channel_index].rising = true;
		}		
	}
	return xhptdc8_configure(mgr_cfg);
}


void print_device_information() {
	xhptdc8_static_info staticinfo;
	printf("-------------------------\n");
	for (int i = 0; i < get_device_count(); i++) {
		xhptdc8_get_static_info(i, &staticinfo);
		printf("Board Serial        : %d.%d\n", staticinfo.board_serial >> 24, staticinfo.board_serial & 0xffffff);
		printf("Board Configuration : %d\n", staticinfo.board_configuration);
		printf("Board Revision      : %d\n", staticinfo.board_revision);
		printf("Firmware Revision   : %d.%d\n", staticinfo.firmware_revision, staticinfo.subversion_revision);
		printf("Driver Revision     : %d.%d.%d\n", ((staticinfo.driver_revision >> 16) & 255), ((staticinfo.driver_revision >> 8) & 255), (staticinfo.driver_revision & 255));
		printf("Driver SVN Revision : %d\n", staticinfo.driver_build_revision);
	}
}


void print_hit(TDCHit* hit) {
	int channel = hit->channel;
	// Channel 8 and 9 are for the ADC Data and only. Not the TDC Data. 
	// We use modulo 10 because for any additional boards the channel number is incremented by board_id * 10
	bool adc_data = ((channel % 10) == 8) || ((channel % 10) == 9);

	//If the hit type contains the XHPTDC8_TDCHIT_TYPE_ERROR flag, indicating an error condition, an "Error" message is printed before printing the other hit information.
	if (hit->type & XHPTDC8_TDCHIT_TYPE_ERROR)
		printf("Error hit at %lld: \n", hit->time);

	printf("%u %lld 0x%x", hit->channel, hit->time, hit->type);
	//last_hit[channel] = hit->time;

	// If the channel number modulo 10 is 8 or 9, indicating that the hit contains ADC data, the ADC data is extracted from the bin member of the TDCHit structure (hit->bin) and printed
	if (adc_data)
		printf(" - ADC Data : %d", (int)(hit->bin));

	printf("\n");
}


// call xhptdc8_read_hits until there is some data or max count of trials
// This function is responsible for continuously polling the xHPTDC8 board to check for hits.
int poll_for_hits(TDCHit* hit_buffer, size_t events_per_read) {
	int trys_to_read_hits = 0;
	while (trys_to_read_hits < MAX_TRYS_TO_READ_HITS) {
		
		// Poll for hits by calling xhptdc8_read_hits
		// Returns the numbers of hits read. Saves what's read in the hit buffer
		unsigned long hit_count = xhptdc8_read_hits(hit_buffer, events_per_read);

		// If hits are available, return the hit count
		if (hit_count)
			return hit_count;

		// If no hits are available, wait for 1 millisecond before trying again
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
		// Increment the trial count
		trys_to_read_hits++;
	}
	if (trys_to_read_hits == MAX_TRYS_TO_READ_HITS)
		printf("Not enough data, check trigger source and device configuration\n");
	return trys_to_read_hits;
}


// fetch hits from the board by polling
// 'read_hits_wrapper' serves as a wrapper around the poll_for_hits function 
void read_hits_wrapper(int events_per_read, std::chrono::duration<double> read_duration) {
	int total_event_count = events_per_read;

	TDCHit* hit_buffer = new TDCHit[events_per_read];

	int total_events = 0;
    auto overall_start_time = std::chrono::steady_clock::now();

	if (total_event_count != -1){ 
		while (total_events < total_event_count and std::chrono::steady_clock::now() - overall_start_time < read_duration ) {
			//start_time = std::chrono::steady_clock::now();
			unsigned long hit_count = poll_for_hits(hit_buffer, events_per_read);

			for (unsigned int i = 0; i < hit_count; i++)
			{
				TDCHit* hit = &(hit_buffer[i]);
				print_hit(hit);
				
				if (++total_events % static_cast<int>(period_of_trigger) == 0)
					printf("Count: %d - events: %d\n", total_events, hit_count);
			}
		}
	} 
	delete[] hit_buffer;
}

// Note: 
// poll_for_hits focuses on low-level polling for hits, while read_hits_wrapper orchestrates 
// the process of hit retrieval and provides a higher-level interface for fetching hits in bulk 
// from the xHPTDC8 board. 

int main(int argc, char* argv[]) {
	// printf("Cronologic xhptdc8 using driver: %s\n", xhptdc8_get_driver_revision_str());
	int error_code = initialize_xhptdc8(8 * 1024 * 1024);

	int device_count = get_device_count();
	//configure all devices with that manager
	exit_on_fail(configure_xhptdc8(device_count), "Could not configure.");
	// print_device_information();

	//start measurement
	exit_on_fail(xhptdc8_start_capture(), "Could not start capturing.");

	//collect measured data for: events_per_read
	read_hits_wrapper(1e9, std::chrono::duration<double>(2));

	//stop measurement
	exit_on_fail(xhptdc8_stop_capture(), "Could not stop capturing.");

	//close manager
	exit_on_fail(xhptdc8_close(), "Could not close devices-manager.");

	return 0;
}

