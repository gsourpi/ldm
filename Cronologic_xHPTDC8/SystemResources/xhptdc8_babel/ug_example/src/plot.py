# branched off wordtoHist

from matplotlib import pyplot as plt
from matplotlib.ticker import EngFormatter
import numpy as np

OVF_PERIOD = 33554432
HIST_TIME = 100e-6
TDC_RES = 25e-12

hist_size = HIST_TIME/TDC_RES
# Create an empty histogram
histogram = [0] * int(hist_size)


# This way we imitate having the data already in memory
with open("lib/x64/output1.txt", "r") as file:
    # Initialize lists to store the channel numbers and timestamps
    channel = []
    timestamps = []

    # Read each line in the file
    for line in file:
        # Split each line into two parts based on whitespace
        parts = line.split()

        # Extract the number and timestamp
        num = int(parts[0])
        timestamp = int(parts[1])

        # Append to the appropriate list
        channel.append(num)
        timestamps.append(timestamp)
        
filtered_timestamps = []
trigger_timestamp = 0 
trigger_found = False 

# Initialize an empty list to store filtered timestamps
filtered_timestamps = []

# Loop through the channel and timestamps lists
for index, chann in enumerate(channel):
    if not trigger_found:
        if chann == 0:  # if we have a trigger signal
            trigger_found = True
            trigger_timestamp = timestamps[index]
            if timestamps[index] - trigger_timestamp > 0:
                filtered_timestamps.append(timestamps[index] - trigger_timestamp)
    else:
        if chann == 0: 
            trigger_timestamp = timestamps[index]
        else:   
            if timestamps[index] - trigger_timestamp > 0:
                filtered_timestamps.append(timestamps[index] - trigger_timestamp)

# for index, chann in enumerate(channel): 
#         if chann == 0: # if we have a trigger signal 
#             trigger_timestamp = timestamps[index]
#         else:
#             if timestamps[index] - trigger_timestamp > 0: 
#                 filtered_timestamps.append(timestamps[index] - trigger_timestamp)
                
                
# Find unique elements and their counts
unique_elements, counts = np.unique(filtered_timestamps, return_counts=True)

# # Print unique elements and their counts
# for element, count in zip(unique_elements, counts):
#     print("Number:", element, "Count:", count)

# Create a bar plot
plt.scatter(unique_elements, counts)

# Set labels and title
plt.xlabel('Time in picoseconds')
plt.ylabel('Count')
plt.title('Histogram of Counts')

# Set x-axis tick labels
plt.xlim(unique_elements.min()*0.5 , unique_elements.max()*1.2)
# Show plot
plt.show()
# plt.hist(filtered_timestamps)
# plt.show()

# """ plt.plot(histogram)
# plt.xticks(ticks=plt.xticks()[0][1:], labels=TDC_RES * np.array(plt.xticks()[0][1:], dtype=np.float64))
# plt.show()
# """

# histogram_data = filtered_timestamps
# xdata=list( range(0,len(histogram) ) )
# xdata = np.array(xdata) * TDC_RES
# fig, ax = plt.subplots()
# formatter = EngFormatter(unit='s')
# ax.xaxis.set_major_formatter(formatter)

# ax.plot(xdata,histogram)

# plt.savefig('plot.jpg')
# plt.show()



print("done")