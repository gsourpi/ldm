#!/bin/bash
cd /home/gsourpi/Desktop/git/ldm/Cronologic_xHPTDC8/Data_acquisition

DIR_PATH = "/home/gsourpi/Desktop/git/ldm/Cronologic_xHPTDC8"

TIMESTAMP=$(date +"%d%m%Y_%H%M%S")
OUTPUT_FILE_PATH = "$DIR_PATH/Experiment_Data/output_$TIMESTAMP.txt"

./xhptdc8_ugex > "$OUTPUT_FILE_PATH"

#python data_processing.py --input "$OUTPUT_FILE_PATH" --output "crono_output_$TIMESTAMP.pkl" --trigger_ch 1

