import sys

# Estimate memory usage per integer
integer_size = sys.getsizeof(0)  # Size of an integer object in bytes

# Assume total available memory in bytes (example: 4 GB = 4 * 1024 * 1024 * 1024 bytes)
available_memory = 8 * 1024 * 1024# Adjust this based on your system

# Calculate maximum number of integers
max_integers = available_memory // integer_size

print(f"Maximum number of integers that can be handled: {max_integers}")
