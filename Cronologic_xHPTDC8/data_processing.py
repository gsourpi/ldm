from matplotlib.ticker import EngFormatter
from matplotlib import pyplot as plt 
import numpy as np
from pathlib import Path

from Utilities.TDC import TDCGroupHits
from Utilities.dataStorage import writeLists

#preallocateCounts = lil_matrix((1,size), dtype=np.int32)
preallocateCounts = np.zeros(int(1e9), dtype=int)

file_path = "/home/gsourpi/Desktop/git/ldm/Cronologic_xHPTDC8/Data_acquisition/crono_1_min_v11.txt"

hits_proccessed = 0 

with open(file_path, "r") as file:
   # Initialize lists to store the channel numbers and timestamps
   channel = []
   timestamps = []

   # Read each line in the file
   for line in file:
      parts = line.split()
      
      num = int(parts[0])
      timestamp = int(parts[1])
      
      channel.append(num)
      timestamps.append(timestamp)

trigger_timestamp = 0 
trigger_found = False 

for index, chann in enumerate(channel):
   if not trigger_found:
         if chann == 1:  # if we have a trigger signal
            trigger_found = True
            trigger_timestamp = timestamps[index]
            hits_proccessed += 1
   else:
         if chann == 1: 
            # Update the trigger timestamp 
            trigger_timestamp = timestamps[index]
            hits_proccessed+=1
         else:   
            preallocateCounts[timestamps[index] - trigger_timestamp] += 1
            hits_proccessed+=1

print(f"Total number of hits read: {hits_proccessed}")

# Initial plot of points 
non_zero_indices = np.nonzero(preallocateCounts)[0]
non_zero_counts = preallocateCounts[non_zero_indices]
plt.scatter(np.array(non_zero_indices), np.array(non_zero_counts))
plt.show(block = False)

# Set appropriate window parameters 
binWindowSize = 500 # picoseconds
binWindowStep = binWindowSize

# Group the bins into appropriate hits applying Gaussian Fit to the data 
hits_lists = TDCGroupHits(preallocateCounts, binWindowSize, binWindowStep=binWindowStep)

# Write lists to pkl file
writeLists(path ='crono_output_v11.pkl', list1 = preallocateCounts, list2 = hits_lists)
