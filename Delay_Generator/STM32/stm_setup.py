import serial
import time
import sys 

def send_uart_commands(port, baudrate, msg):
    try:	
            sys.stdout.flush()	
            ser.write(msg.encode('utf-8'))
            sys.stdout.write(f"\r[PC -> STM32] Set delay to: {msg.strip()} ps")
            time.sleep(1)

    except KeyboardInterrupt:
        print("Interrupted by user.")
        ser.close()
        print("Serial port closed.")

if __name__ == "__main__":
   
    SERIAL_PORT = "/dev/ttyUSB2" 
    BAUD_RATE = 115200
    
    # Delay range definitions (in picoseconds)
    DELAY_RANGE_START = 1000
    DELAY_RANGE_STOP  = 25000
    DELAY_RANGE_STEP  = 1000
    
    # Specify how often and for how long to resend the same message
    msg_counts = float('inf')
    time_of_same_msg = 2 # seconds 
    
    ser = serial.Serial(SERIAL_PORT, BAUD_RATE, timeout=2)
    time.sleep(1) 
    
    messages_to_send = [
    f"{i}\r" 
    for i in range(
        DELAY_RANGE_START, 
        DELAY_RANGE_STOP, 
        DELAY_RANGE_STEP
    )
]
    for message in messages_to_send: 
    	start_time = time.time()
    	count = 0 
    	while (count < msg_counts) and (time.time()-start_time < time_of_same_msg): 
    		send_uart_commands(SERIAL_PORT, BAUD_RATE, message)
    		count += 1
   
    ser.close()
    print("\nSerial port closed.")
