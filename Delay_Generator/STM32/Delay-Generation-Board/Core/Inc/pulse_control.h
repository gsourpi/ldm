/*
 * pulse_control.h
 *
 *  Created on: Nov 11, 2024
 *      Author: gsourpi
 */

#ifndef INC_PULSE_CONTROL_H_
#define INC_PULSE_CONTROL_H_

#include <stdint.h>
#include <math.h>
#include "stm32f3xx_hal.h"  // Include HAL for SPI functions
#include "pulse_control.h"
#include "menu.h"
#include "main.h"


// SPI Chip Select (CS) Pin
#define CS_PIN GPIO_PIN_6
#define CS_PORT GPIOB

// Gain Control Register for the LMH6401 amplifier
#define REG_GAIN_CONTROL 0x02  // Adjust according to actual register address

// Base and Maximum Delay in picoseconds (ps) for Delay Chip
#define REF_DELAY_PS 3200     // 3.2 ns in ps
#define MAX_DELAY_PS 14800     // 14.8 ns in ps (max delay for delay chip)

// Delay Steps in picoseconds for each bit position (for 10-bit delay word)
static const int delay_steps[10] = {10, 15, 35, 70, 145, 290, 575, 1150, 2300, 4610};

/* Function Prototypes */

// SPI Register Access for LMH6401 Variable Gain Amplifier
uint8_t readRegister(uint8_t regAddress);
void set_amplitude(uint8_t amplitude);

// Word Calculation Functions for Delay Chips
uint16_t calculateDelayWord(uint32_t programmable_delay);
uint32_t calculatePwWord(uint32_t pulse_width_ps);

// 64-bit SPI Word Builder for Cascading Delays and Pulse Width
uint64_t build_spi_delay_pw_word(uint32_t pulse_width_ps, uint32_t total_delay_ps);

// Function to Transmit 64-bit Word for Combined Delay and Pulse Width
void set_pw_and_delay(uint32_t pulse_width_ps, uint32_t total_delay_ps);

extern uint32_t pulse_width_ps;
extern uint32_t total_delay_ps;
extern uint32_t accumulated_delay_ps;
extern float amplitude;

extern volatile uint16_t PW;       // 10-bit PW register
extern volatile uint16_t Del1;     // 11-bit delay value
extern volatile uint16_t Del0;     // 11-bit delay value
extern volatile uint16_t DelRef;   // 11-bit static calibration value
extern volatile uint16_t data_array[4];  // Data array for SPI transmission

#endif /* INC_PULSE_CONTROL_H_ */
