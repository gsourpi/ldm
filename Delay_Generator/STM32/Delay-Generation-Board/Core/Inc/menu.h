/*
 * menu.h
 *
 *  Created on: Nov 11, 2024
 *      Author: gsourpi
 */

#ifndef INC_MENU_H_
#define INC_MENU_H_

#include <stdint.h>
#include <stdbool.h>
#include "pulse_control.h"

#define ITEMS_PER_PAGE 10    // Number of items displayed at once
#define ITEMS_PER_COLUMN 3   // Number of items per column

typedef enum {
    None,
    PulseWidth,
    Delay,
    Amplitude
} ParameterType;

// Define your MenuItem structure to match the new format
typedef struct MenuItem {
    char *text;                 // Display text for the menu item
    struct Menu *submenu;       // Pointer to a submenu (or NULL if no submenu)
    ParameterType paramType;    // Type of parameter (PulseWidth, Delay, Amplitude, etc.)
    int value;                  // Value associated with the menu item
} MenuItem;

// Define the Menu structure
typedef struct Menu {
    char *name;            // Name of the menu
    MenuItem *items;       // Array of menu items
    int numItems;          // Number of items in the menu
} Menu;

// Externally accessible variables
extern Menu initialMenu;           // The initial menu
extern uint8_t currentSelection;    // Tracks the current menu selection
extern Menu *currentMenu;           // Points to the current menu
extern volatile uint32_t counter;   // Current counter value from TIM2
extern volatile uint32_t prev_counter; // Previous counter value for comparison
extern volatile int32_t rotation;   // Tracks the direction of rotation
extern int total_items;             // Total items in the current menu
extern int pageStartIndex;          // Start index for pagination

// Function prototypes for menu operations
void displayRegisterValue(uint32_t regValue);
void drawMenu(uint8_t selected, Menu *menu);
void navigateMenu(volatile int32_t *rotation, volatile bool *buttonPressed);

#endif /* INC_MENU_H_ */
