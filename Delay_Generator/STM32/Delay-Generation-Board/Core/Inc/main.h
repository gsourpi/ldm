/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "menu.h"
#include "pulse_control.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
typedef struct {
    char identifier;
    int value;
} ParsedData;

void parse_rx_data(ParsedData *results, int *count);
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);

extern SPI_HandleTypeDef hspi1;
extern UART_HandleTypeDef huart2;

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define AMP_DEL_nCS_Pin GPIO_PIN_1
#define AMP_DEL_nCS_GPIO_Port GPIOF
#define DEL_nCS_Pin GPIO_PIN_4
#define DEL_nCS_GPIO_Port GPIOA
#define INT_TRIG_Pin GPIO_PIN_0
#define INT_TRIG_GPIO_Port GPIOB
#define AMP_PLS_nCS_Pin GPIO_PIN_9
#define AMP_PLS_nCS_GPIO_Port GPIOA
#define AMP_PD_Pin GPIO_PIN_10
#define AMP_PD_GPIO_Port GPIOA
#define CASCADE_SEL_Pin GPIO_PIN_11
#define CASCADE_SEL_GPIO_Port GPIOA
#define SSD_RST_Pin GPIO_PIN_12
#define SSD_RST_GPIO_Port GPIOA

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
