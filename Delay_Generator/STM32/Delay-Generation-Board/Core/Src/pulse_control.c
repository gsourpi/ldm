/*
 * pulse_control.c
 *
 *  Created on: Nov 11, 2024
 *      Author: gsourpi
 */

// For SPI interface - LMH6401 Variable Gain Amplifier
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include "stm32f3xx_hal.h"
#include "stm32f3xx_hal_gpio.h"
#include "pulse_control.h"
#include <string.h>



uint32_t pulse_width_ps = UINT32_MAX;
uint32_t total_delay_ps = UINT32_MAX;
float amplitude = NAN;
uint32_t accumulated_delay_ps = UINT32_MAX;


uint8_t readRegister(uint8_t regAddress) {
    //uint8_t command = (1 << 7) | regAddress; // '1' for read, followed by 7-bit register address
    uint8_t receivedData = 0;
    //uint8_t dummyByte = 0x00; // Dummy byte to clock the response

    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
//    HAL_SPI_Transmit(&hspi2, &command, 1, HAL_MAX_DELAY);
//    HAL_SPI_TransmitReceive(&hspi2, &dummyByte, &receivedData, 1, HAL_MAX_DELAY); // Clock out received data
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_SET);

    return receivedData;
}


void set_amplitude(uint8_t amplitude) {
	// Based on pg.25 datasheet: https://www.ti.com/lit/ds/symlink/lmh6401.pdf?ts=1731403880328&ref_url=https%253A%252F%252Fnl.mouser.com%252F



//	float input_amplitude = 1.0; // Assume a 1.0V input amplitude
//    float required_gain = amplitude / input_amplitude;
	uint8_t gain_register_value = amplitude;
//
//    // Convert linear gain to dB
//    int gain_dB = (int)(20 * log10f(required_gain));

    if (amplitude > 32) {
    	gain_register_value = 32;
    }

    uint8_t command = (0 << 7) | REG_GAIN_CONTROL; // '0' for write
    uint8_t txData[2] = {gain_register_value, command};

    HAL_GPIO_WritePin(AMP_DEL_nCS_GPIO_Port, AMP_DEL_nCS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, (uint8_t*)&txData, 1, HAL_MAX_DELAY);
    HAL_GPIO_WritePin(AMP_DEL_nCS_GPIO_Port, AMP_DEL_nCS_Pin, GPIO_PIN_SET);

   HAL_GPIO_WritePin(AMP_PLS_nCS_GPIO_Port, AMP_PLS_nCS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, (uint8_t*)&txData, 1, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(AMP_PLS_nCS_GPIO_Port, AMP_PLS_nCS_Pin, GPIO_PIN_SET);

/*
    uint8_t command1 = (1 << 7) | 3; // '0' for write
    uint8_t txData1[2] = {0, command1};

    HAL_GPIO_WritePin(AMP_DEL_nCS_GPIO_Port, AMP_DEL_nCS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, (uint8_t*)&txData1, 1, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(AMP_DEL_nCS_GPIO_Port, AMP_DEL_nCS_Pin, GPIO_PIN_SET);
*/


	// Enable the Internal Trigger
	HAL_GPIO_WritePin(INT_TRIG_GPIO_Port, INT_TRIG_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(INT_TRIG_GPIO_Port, INT_TRIG_Pin, GPIO_PIN_RESET);

}


// For SPI interface
void set_pw_and_delay(uint32_t pulse_width_ps, uint32_t total_delay_ps){

    uint16_t PW = pulse_width_ps/5; // Default (Max Pulse) 3FF
    uint32_t PW_t = __RBIT(PW);

    PW = PW_t >> 22;
	if(total_delay_ps > 10000){
		Del1 = 1 << 10;
		total_delay_ps -= 10000;
	}
	else{
		Del1 = 0;
	}
	Del0 = total_delay_ps / 10;

	// Clearer to pre-slice the values
	uint16_t DelRefA = (DelRef & 0x7C0) >> 6;
	uint16_t DelRefB = (DelRef & 0x38) >> 3;
	uint16_t DelRefC = (DelRef & 0x7) >> 0;

	uint16_t Del1A = (Del1 & 0x400) >> 10;
	uint16_t Del1B = (Del1 & 0x3F8) >> 3;
	uint16_t Del1C = (Del1 & 0x7) >> 0;

	uint16_t Del0A = (Del0 & 0x780) >> 7;
	uint16_t Del0B = (Del0 & 0x7F) >> 0;

	uint16_t PWH = (PW & 0x3C0) >> 6;
	uint16_t PWL = (PW & 0x3F) >> 0;

	// Assemble the SPI message
	data_array[0] = 0 | (DelRefA << 4) | (DelRefB << 0);
	data_array[1] = 0 | (DelRefC << 13) | (Del1A << 12) | (Del1B << 4) | (Del1C << 0);
	data_array[2] = 0 | (Del0A << 12) | (Del0B << 4) | (PWH << 0);
	data_array[3] = 0 | (PWL << 10);

	// Send the message through SPI
	HAL_GPIO_WritePin(DEL_nCS_GPIO_Port, DEL_nCS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, (uint8_t*)&data_array, 4, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(DEL_nCS_GPIO_Port, DEL_nCS_Pin, GPIO_PIN_SET);

//	// Enable the Internal Trigger
//	HAL_GPIO_WritePin(INT_TRIG_GPIO_Port, INT_TRIG_Pin, GPIO_PIN_SET);
//	HAL_GPIO_WritePin(INT_TRIG_GPIO_Port, INT_TRIG_Pin, GPIO_PIN_RESET);

	// Send UART message for verification
	char buffer[80];
	const char *message = "Word sent to delay chips.\0";
	for (char *dest = buffer; (*dest = *message) != '\0'; dest++, message++);
	HAL_UART_Transmit(&huart2, (uint8_t*)buffer, strlen(buffer), HAL_MAX_DELAY);
}

