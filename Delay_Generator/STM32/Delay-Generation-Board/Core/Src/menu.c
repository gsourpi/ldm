// menu.c

#include "main.h"
#include "ssd1306.h"
#include "ssd1306_tests.h"
#include "ssd1306_fonts.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>


// Pulse Width Menu Items
MenuItem pwidthMenuItems[] = {
    { "0", NULL, PulseWidth, 0 },
    { "1", NULL, PulseWidth, 1 },
    { "2", NULL, PulseWidth, 2 },
    { "3", NULL, PulseWidth, 3 },
    { "4", NULL, PulseWidth, 4 },
    { "5", NULL, PulseWidth, 5 }
};

Menu pwidthMenu = {
    .name = "Pulse Width",
    .items = pwidthMenuItems,
    .numItems = 6
};

// Delay Pico Next Menu Items
MenuItem delaypicoNextMenuItems[] = {
    { "500 ps", NULL, Delay, 500 },
    { "600 ps", NULL, Delay, 600 },
    { "700 ps", NULL, Delay, 700 },
    { "800 ps", NULL, Delay, 800 },
    { "900 ps", NULL, Delay, 900 },
    { "1000 ps", NULL, Delay, 1000 }
};

Menu delaypicoNextMenu = {
    .name = "Delay Pico Next",
    .items = delaypicoNextMenuItems,
    .numItems = 6
};

// Delay Pico Menu Items
MenuItem delaypicoMenuItems[] = {
    { "0 ps", NULL, Delay, 0 },
    { "100 ps", NULL, Delay, 100 },
    { "200 ps", NULL, Delay, 200 },
    { "300 ps", NULL, Delay, 300 },
    { "400 ps", NULL, Delay, 400 },
    { "next", &delaypicoNextMenu, None, 0 }
};

Menu delaypicoMenu = {
    .name = "Delay Pico",
    .items = delaypicoMenuItems,
    .numItems = 6
};

// Delay Sub Menu Items 0-5 ns
MenuItem delaySubMenuItems_0_5[] = {
    { "0 ns", &delaypicoMenu, Delay, 0 },
    { "1 ns", &delaypicoMenu, Delay, 1000 },
    { "2 ns", &delaypicoMenu, Delay, 2000 },
    { "3 ns", &delaypicoMenu, Delay, 3000 },
    { "4 ns", &delaypicoMenu, Delay, 4000 },
    { "5 ns", &delaypicoMenu, Delay, 5000 }
};

Menu delaySubMenu_0_5 = {
    .name = "Delay 0-5 ns",
    .items = delaySubMenuItems_0_5,
    .numItems = 6
};

// Delay Sub Menu Items 5-10 ns
MenuItem delaySubMenuItems_5_10[] = {
    { "5 ns", &delaypicoMenu, Delay, 5000 },
    { "6 ns", &delaypicoMenu, Delay, 6000 },
    { "7 ns", &delaypicoMenu, Delay, 7000 },
    { "8 ns", &delaypicoMenu, Delay, 8000 },
    { "9 ns", &delaypicoMenu, Delay, 9000 },
    { "10 ns", &delaypicoMenu, Delay, 10000 }
};

Menu delaySubMenu_5_10 = {
    .name = "Delay 5-10 ns",
    .items = delaySubMenuItems_5_10,
    .numItems = 6
};

// Delay Sub Menu Items 10-15 ns
MenuItem delaySubMenuItems_10_15[] = {
    { "10 ns", &delaypicoMenu, Delay, 10000 },
    { "11 ns", &delaypicoMenu, Delay, 11000 },
    { "12 ns", &delaypicoMenu, Delay, 12000 },
    { "13 ns", &delaypicoMenu, Delay, 13000 },
    { "14 ns", &delaypicoMenu, Delay, 14000 },
    { "15 ns", &delaypicoMenu, Delay, 15000 }
};

Menu delaySubMenu_10_15 = {
    .name = "Delay 10-15 ns",
    .items = delaySubMenuItems_10_15,
    .numItems = 6
};

// Delay Sub Menu Items 15-20 ns
MenuItem delaySubMenuItems_15_20[] = {
    { "15 ns", &delaypicoMenu, Delay, 15000 },
    { "16 ns", &delaypicoMenu, Delay, 16000 },
    { "17 ns", &delaypicoMenu, Delay, 17000 },
    { "18 ns", &delaypicoMenu, Delay, 18000 },
    { "19 ns", &delaypicoMenu, Delay, 19000 },
    { "20 ns", &delaypicoMenu, Delay, 20000 }
};

Menu delaySubMenu_15_20 = {
    .name = "Delay 15-20 ns",
    .items = delaySubMenuItems_15_20,
    .numItems = 6
};

// Delay Sub Menu Items 20-25 ns
MenuItem delaySubMenuItems_20_25[] = {
    { "20 ns", &delaypicoMenu, Delay, 20000 },
    { "21 ns", &delaypicoMenu, Delay, 21000 },
    { "22 ns", &delaypicoMenu, Delay, 22000 },
    { "23 ns", &delaypicoMenu, Delay, 23000 },
    { "24 ns", &delaypicoMenu, Delay, 24000 },
    { "25 ns", &delaypicoMenu, Delay, 25000 }
};

Menu delaySubMenu_20_25 = {
    .name = "Delay 20-25 ns",
    .items = delaySubMenuItems_20_25,
    .numItems = 6
};

// Delay Menu Items
MenuItem delayMenuItems[] = {
    { "0-5 ns", &delaySubMenu_0_5, None, 0 },
    { "5-10 ns", &delaySubMenu_5_10, None, 0 },
    { "10-15 ns", &delaySubMenu_10_15, None, 0 },
    { "15-20 ns", &delaySubMenu_15_20, None, 0 },
    { "20-25 ns", &delaySubMenu_20_25, None, 0 }
};

Menu delayMenu = {
    .name = "Delay",
    .items = delayMenuItems,
    .numItems = 5
};

// Amplitude Menu Items
MenuItem amplitudeMenuItems[] = {
    { "1", NULL, Amplitude, 1 },
    { "2", NULL, Amplitude, 2 }
};

Menu amplitudeMenu = {
    .name = "Amplitude",
    .items = amplitudeMenuItems,
    .numItems = 2
};

// Initial menu with sub-menus
MenuItem initialMenuItems[] = {
    { "Pulse Width", &pwidthMenu, None, 0 },
    { "Delay", &delayMenu, None, 0 },
    { "Amplitude", &amplitudeMenu, None, 0 }
};

Menu initialMenu = {
    .name = "Main Menu",
    .items = initialMenuItems,
    .numItems = 3
};

void drawMenu(uint8_t selected, Menu *menu)
{
    ssd1306_Fill(Black);

    uint8_t lineHeight = 10;         // Adjust based on your font size
    uint8_t leftX = 0;               // X position for the left column
    uint8_t rightX = 64;             // X position for the right column

    for (uint8_t i = 0; i < ITEMS_PER_COLUMN; i++) {
        uint8_t leftIndex = pageStartIndex + i;
        uint8_t rightIndex = pageStartIndex + i + ITEMS_PER_COLUMN;

        uint8_t yPos = i * lineHeight;

        if (leftIndex < menu->numItems) {
            ssd1306_SetCursor(leftX, yPos);
            if (leftIndex == selected) {
                ssd1306_WriteString("->", Font_7x10, White);
            } else {
                ssd1306_WriteString("  ", Font_7x10, White);
            }
            ssd1306_WriteString(menu->items[leftIndex].text, Font_6x8, White);
        }

        if (rightIndex < menu->numItems) {
            ssd1306_SetCursor(rightX, yPos);
            if (rightIndex == selected) {
                ssd1306_WriteString("->", Font_7x10, White);
            } else {
                ssd1306_WriteString("  ", Font_7x10, White);
            }
            ssd1306_WriteString(menu->items[rightIndex].text, Font_6x8, White);
        }
    }

    ssd1306_UpdateScreen();
}

void navigateMenu(volatile int32_t *rotation, volatile bool *buttonPressed)
{
    // Handle clockwise rotation (increment currentSelection)
    if (*rotation > 0 && currentSelection < currentMenu->numItems - 1) {
        currentSelection++;
        drawMenu(currentSelection, currentMenu);
    }
    // Handle counterclockwise rotation (decrement currentSelection)
    else if (*rotation < 0 && currentSelection > 0) {
        currentSelection--;
        drawMenu(currentSelection, currentMenu);
    }
    // Handle button press
    else if (*buttonPressed) {
            MenuItem *selectedItem = &currentMenu->items[currentSelection];
            if (selectedItem->submenu != NULL) {
                currentMenu = selectedItem->submenu;
                currentSelection = 0;
                drawMenu(currentSelection, currentMenu);
                if (selectedItem->paramType == Delay && strstr(selectedItem->text, "ns") && strchr(selectedItem->text, '-') == NULL) {
                	accumulated_delay_ps = selectedItem->value;
                }
            } else {
                switch (selectedItem->paramType) {
                    case PulseWidth:
                    	pulse_width_ps = selectedItem->value;
                        break;
                    case Delay:
                        accumulated_delay_ps += selectedItem->value;
                        set_pw_and_delay(10, accumulated_delay_ps);
                        accumulated_delay_ps = 0;
                        break;
                    case Amplitude:
                        amplitude = (float)selectedItem->value;
                        break;
                    default:
                        break;
                }

                if (pulse_width_ps != UINT32_MAX && total_delay_ps != UINT32_MAX) {
                    set_pw_and_delay(pulse_width_ps, total_delay_ps);
                    pulse_width_ps = UINT32_MAX;
                    total_delay_ps = UINT32_MAX;
                }
                if (amplitude != isnan(amplitude)) {
                    set_amplitude(amplitude);
                    amplitude = NAN;
                }

                // Return to initial menu
                currentMenu = &initialMenu;
                currentSelection = 0;
                drawMenu(currentSelection, currentMenu);
            }
    }
    *rotation = 0;
    *buttonPressed = false;
}

// Display a single value (e.g., for showing increments)
void displayRegisterValue(uint32_t regValue)
	{
		 char buffer[30];
		 int32_t signedValue = (int32_t)regValue;
		 snprintf(buffer, sizeof(buffer), "%" PRId32, signedValue);
		 ssd1306_Fill(Black);  // Clear the OLED screen
		 ssd1306_SetCursor(35, 10);  // Set cursor position
		 ssd1306_WriteString(buffer, Font_7x10, White);  // Display the value on OLED
		 ssd1306_UpdateScreen();  // Refresh the screen
	 }

