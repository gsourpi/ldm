import pandas as pd

file_path = 'bom.csv'
df = pd.read_csv(file_path)
df_filtered = df.dropna(subset=['Op Voltage (V)', 'Qty', 'Current (mA)']).copy()
df_filtered['Qty'] = pd.to_numeric(df_filtered['Qty'], errors='coerce')
df_filtered['Current (mA)'] = pd.to_numeric(df_filtered['Current (mA)'], errors='coerce')
df_filtered = df_filtered.dropna(subset=['Current (mA)', 'Qty'])
df_filtered['Total Current (mA)'] = df_filtered['Qty'] * df_filtered['Current (mA)']
total_current = df_filtered['Total Current (mA)'].sum()
df_filtered[['Op Voltage Min (V)', 'Op Voltage Max (V)']] = df_filtered['Op Voltage (V)'].str.split(',', expand=True).astype(float)

print(df_filtered[['Reference', 'Qty', 'Current (mA)', 'Total Current (mA)', 'Op Voltage Min (V)', 'Op Voltage Max (V)']])
print(f"\nTotal Current: {total_current} (mA) or {total_current * 1e-3} (A).")
print(f"Maximum min operational voltage for components: {df_filtered['Op Voltage Min (V)'].max()} (V).")
print(f"Minimum max operational voltage for components: {df_filtered['Op Voltage Max (V)'].min()} (V).")

