import subprocess
import os 

def run_experiment(delay, acq_time, output_file):
    """
    Launch two processes in parallel:
    1. stm_delay_controller.py with --delay and --duration arguments.
    2. histmode.py with a matching acquisition_time_in_seconds (acq_time) 
       and an output filename (output_file).

    Both scripts run for the same duration (acq_time)

    We wait for both to finish before returning.
    """
    # python stm_set_delay.py --port /dev/ttyUSB0 --baudrate 115200  --delay 10000  --duration 60
    cmd1 = [
        "python",
        "stm_set_delay.py",
        "--port", "/dev/ttyUSB0",
        "--baudrate", "115200",
        "--delay", str(delay),
        "--duration", str(acq_time) 
    ]

    # python tdc_daq_sw/histomode.py --tacq 6 --outputfile histograms/output_file
    cmd2 = [
    "python",
    "tdc_daq_sw/histomode.py",
    "--tacq", str(acq_time),
    "--outputfile", output_file
    ]

    print(f"\nSet delay={delay}, duration={acq_time}")
    print(f"Starting histmode with duration={acq_time}, output_file={output_file}")

    # Start both processes in parallel and wait for them to finish
    p1 = subprocess.Popen(cmd1)
    p2 = subprocess.Popen(cmd2)

    p1.wait()
    p2.wait()

    print("\nBoth scripts finished.\n")

def main():

    # Example: scan_from_6n_to_1n_10sec_integration
    # Or simply 'testing' when used for testing 
    scan_folder_path = "Data/fine_scan_850ps_to_5ns_100ps_step_#4"
    os.makedirs(scan_folder_path, exist_ok=True)

    j = 1
    while 1: 
        # Set delay between pump and probe pulses
        delays =  [i for i in range(850, 1100, 10)] + [i for i in range(1100, 6000, 100)]
        # 74 steps total 

        # Set acquisition time (in seconds)
        acq_time = 60 * 10 # half an hour 
        
        for delay in delays:
            output_file = os.path.join(scan_folder_path, f"hist_output_delay_{delay}ps_integration_{acq_time}sec_{j}.txt")
            run_experiment(delay, acq_time, output_file)
        
        j += 1
        print("All experiments finished.")

    
if __name__ == "__main__":
    main()
