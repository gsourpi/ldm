import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from scipy.signal import find_peaks, peak_widths
import argparse
from scipy.optimize import curve_fit
import re 

def plot_histograms(time_ps, pump_hist, probe_hist, detector_hist):
    """Plots Reference, Delay, and Detector histograms in the same figure using subplots."""

    fig, axes = plt.subplots(3, 1, figsize=(10, 12))  # 3 rows, 1 column

    axes[0].plot(time_ps, pump_hist, label='Reference Histogram', linestyle='-', marker='o')
    axes[0].set_xlabel("Time (ps)")
    axes[0].set_ylabel("Counts")
    axes[0].set_title("Reference - Pump Histogram")
    axes[0].legend()
    axes[0].grid()

    axes[1].plot(time_ps, probe_hist, label='Delay Histogram', linestyle='-', marker='o', color='r')
    axes[1].set_xlabel("Time (ps)")
    axes[1].set_ylabel("Counts")
    axes[1].set_title("Delay - Probe Histogram")
    axes[1].legend()
    axes[1].grid()

    axes[2].plot(time_ps, detector_hist, label='Detector Histogram', linestyle='-', marker='o', color='g')
    axes[2].set_xlabel("Time (ps)")
    axes[2].set_ylabel("Counts")
    axes[2].set_title("Detector Histogram")
    axes[2].legend()
    axes[2].grid()

    plt.tight_layout()
    plt.show()


def sent_to_measured_pulse_ratio(hist_name, pump_hist, probe_hist, detector_hist, delay_bins, output_file="detection_efficiency.txt"):
    """Computes and saves detection efficiency; Ratio of the pulses that we trigger to the pulses we are actually measuring."""
    total_pulses_sent = sum(pump_hist) + sum(probe_hist)
    total_pulses_measured = sum(detector_hist)
    missed_pulses = total_pulses_sent - total_pulses_measured

    if total_pulses_sent == 0:
        print("Error: No pulses sent, cannot compute detection efficiency.")
        return None

    sent_to_measured_pulse_ratio = total_pulses_measured / total_pulses_sent

    file_exists = os.path.isfile(output_file)
    is_empty = not file_exists or os.path.getsize(output_file) == 0

    with open(output_file, "a") as f:
        if is_empty: 
            f.write("Detection_Efficiency Delay(bins) Delay (ps) \n")
        f.write(f"{sent_to_measured_pulse_ratio:.6f} {delay_bins} {delay_bins*5}\n")

    #print(f"Computed detection efficiency saved to {output_file}")
    return sent_to_measured_pulse_ratio


def gaussian(x, a, mu, sigma):
        """Gaussian function."""
        return a * np.exp(-((x - mu) ** 2) / (2 * sigma ** 2))

def fit_gaussian(data, plot=True):

    data = np.array(data)
    x = np.arange(len(data))
    
    a_guess = np.max(data)
    mu_guess = np.sum(x * data) / np.sum(data)  # Weighted mean
    sigma_guess = np.sqrt(np.sum(data * (x - mu_guess) ** 2) / np.sum(data))  # Weighted std dev

    p0 = [a_guess, mu_guess, sigma_guess]

    popt, pcov = curve_fit(gaussian, x, data, p0=p0)
    return popt, pcov


def compute_delay_bins(pump_hist, probe_hist):
    pump_peaks_idx, _ = find_peaks(pump_hist, height=0.07*max(pump_hist))
    probe_peaks_idx, _ = find_peaks(probe_hist, height=0.07*max(probe_hist))

    pump_params, _ = fit_gaussian(pump_hist[0: (pump_peaks_idx[1]- pump_peaks_idx[0])//2])
    probe_params, _ = fit_gaussian(probe_hist[0: (probe_peaks_idx[1]- probe_peaks_idx[0])//2])

    delay_bins = probe_params[1] - pump_params[1]
    if delay_bins < 0: 
        return None
    return delay_bins


def get_peak_loc_widths_and_ranges(detector_hist, delay_bins): 
    # Usage: 
    # peaks_idx, peak_width, start_idxs, end_idxs = get_peak_loc_widths_and_ranges(detector_hist, delay_bins)

    peaks_idx, _ = find_peaks(detector_hist, height=0.001*max(detector_hist), distance=delay_bins/2)
    if len(peaks_idx) == 0:
        print(f"Error: No peaks detected. At {hist_name}")
    # elif len(peaks_idx) == 3:
    #     # Select search region for probe peak (expected to be the same height or lower than deadtime) 
    #     start_idx = 
    #     end_idx   = 
    #     peaks_idx_of_probe, _ = find_peaks(detector_hist, height=0.001*max(detector_hist), distance=delay_bins/2)

    results_half = peak_widths(detector_hist, peaks_idx, rel_height=0.5)
    peak_width = results_half[0] 
    start_idxs = np.round(peaks_idx-2*peak_width).astype(int)
    end_idxs = np.round(peaks_idx+2*peak_width).astype(int)

    return peaks_idx, peak_width, start_idxs, end_idxs

def get_integrated_counts_of_peaks(detector_hist, delay_bins): 
    integrated_counts_of_peaks =[]
    peaks_idx, peak_width, start_idxs, end_idxs = get_peak_loc_widths_and_ranges(detector_hist, delay_bins)
    for s,e in zip(start_idxs,end_idxs):
        integrated_counts_of_peaks.append(np.sum(detector_hist[s:e]))
    return integrated_counts_of_peaks, peaks_idx, peak_width, start_idxs, end_idxs


def pulse_ratios_with_integrated_counts(histogram_name,detector_hist, delay_bins, output_file="pulse_ratios_with_integrated_counts.txt"):
    """Computes and saves availability (S) and delay."""
    try: 
        (integrated_counts_of_peaks, 
         peaks_idx, 
         peak_width, 
         start_idxs, 
         end_idxs) = get_integrated_counts_of_peaks(detector_hist, delay_bins)
        
        pump_counts_M  = integrated_counts_of_peaks[0]
        probe_counts_H = integrated_counts_of_peaks[1]   # num of detected counts at the second (probe) peak

        ref_pump_counts  = integrated_counts_of_peaks[2]
        ref_probe_counts = integrated_counts_of_peaks[3]

        norm_pump_counts = pump_counts_M/ref_pump_counts
        norm_probe_counts = probe_counts_H/ref_probe_counts

        t_delay        = integrated_counts_of_peaks[1] - integrated_counts_of_peaks[0]  # time difference between the two peaks in bins
    except Exception as e: 
        print(f"Problem at {histogram_name}, {e}")

    if len(peaks_idx) < 2:
        print(f"Error: Not enough peaks found at {histogram_name}")
        return None

    file_exists = os.path.isfile(output_file)
    is_empty = not file_exists or os.path.getsize(output_file) == 0

    with open(output_file, "a") as f:
        if is_empty:
            f.write("pump_ratio probe_ratio delay_bins histogram_name pump_height probe_height ref_pump_height ref_probe_height\n")
        f.write(f"{norm_pump_counts:.6f} {norm_probe_counts} {delay_bins} {histogram_name} {integrated_counts_of_peaks[0]} {integrated_counts_of_peaks[1]} {integrated_counts_of_peaks[2]} {integrated_counts_of_peaks[3]}\n")
    return 


def pulse_ratios_with_peak_counts(histogram_name,detector_hist, delay_bins, output_file="pulse_ratios_with_peak_counts.txt"):
    
    peaks_idx, _ = find_peaks(detector_hist, height=0.001*max(detector_hist), distance=delay_bins/2)

    if len(peaks_idx) < 2:
        print(f"Error: Not enough peaks found at {histogram_name}")
        return None

    pump_counts_M  = peaks_idx[0]
    probe_counts_H = peaks_idx[1] 

    ref_pump_counts  = peaks_idx[2]
    ref_probe_counts = peaks_idx[3]

    norm_pump_counts = pump_counts_M/ref_pump_counts
    norm_probe_counts = probe_counts_H/ref_probe_counts

    t_delay        = peaks_idx[1] - peaks_idx[0]  # time difference between the two peaks in bins

    file_exists = os.path.isfile(output_file)
    is_empty = not file_exists or os.path.getsize(output_file) == 0

    with open(output_file, "a") as f:
        if is_empty:
            f.write("pump_ratio probe_ratio delay_bins histogram_name pump_height probe_height ref_pump_height ref_probe_height\n")
        f.write(f"{norm_pump_counts:.6f} {norm_probe_counts} {delay_bins} {histogram_name} {peaks_idx[0]} {peaks_idx[1]} {peaks_idx[2]} {peaks_idx[3]}\n")
    return 


def one_peak_count_testing(num_of_peak, hist_name, detector_hist, pump_hist, time_ps, delay_bins, output_file="probe_testing.txt"):
    
    peaks_idx, peak_width, start_idxs, end_idxs = get_peak_loc_widths_and_ranges(detector_hist, delay_bins)
    
    if num_of_peak < 1 : 
        num_of_peak = 0 

    start_idx = start_idxs[num_of_peak-1]
    end_idx   = end_idxs[num_of_peak-1]

    integrated_counts_of_peak = sum(detector_hist[start_idx:end_idx])

    peak_idx = start_idx + (end_idx - start_idx)/2
    counts_from_single_peak = detector_hist[int(round(peak_idx))]
    
    file_exists = os.path.isfile(output_file)
    is_empty = not file_exists or os.path.getsize(output_file) == 0

    with open(output_file, "a") as f:
        if is_empty: 
            f.write("integrated_counts_of_peak counts_from_single_peak delay_bins hist_name \n")
        f.write(f"{integrated_counts_of_peak:.6f} {counts_from_single_peak:.6f} {delay_bins} {hist_name}\n")

    return 

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process histogram files and compute various metrics.")
    parser.add_argument("--histdir", default="../Data/fine_scan_850ps_to_5ns_100ps_step_#1", help="The directory path containing histogram files")
    parser.add_argument("--resultsdir", default="results/", help="The directory path containing the results of metrics calculations")
    parser.add_argument("--plot", action="store_true", help="Optionally plot the histograms")
    args = parser.parse_args()

    results_folder = args.resultsdir
    if not os.path.exists(results_folder):
        os.makedirs(results_folder)

    histograms_dir = args.histdir
    histogram_files = os.listdir(histograms_dir)

    subfolder_name = os.path.basename(histograms_dir)  # e.g. "scan_from_6n_to_1n_10sec_integration"
    subfolder_path = os.path.join(args.resultsdir, subfolder_name)

    # Create the subfolder if it doesn't already exist
    os.makedirs(subfolder_path, exist_ok=True)

    for histogram_name in histogram_files:
        try:
            filename = os.path.join(histograms_dir, histogram_name)
            hist_name = re.search(r'hist_output_delay_\d+ps_integration_\d+sec_\d+', filename).group(0)

            # Load data
            data = pd.read_csv(filename, sep='\\s+', header=None)
            non_zero_columns = (data != 0).any()
            data = data.loc[:, non_zero_columns]
            time_ps = np.arange(len(data)) 

            # Extract histograms
            histograms = {col: data[col].tolist() for col in data.columns}
            pump_hist = histograms[list(histograms.keys())[0]]
            probe_hist = histograms[list(histograms.keys())[1]]
            detector_hist = histograms[list(histograms.keys())[2]]

            if args.plot:
                plot_histograms(time_ps, pump_hist, probe_hist, detector_hist)
            
            delay_bins = compute_delay_bins(pump_hist, probe_hist)

            if delay_bins is not None: 
                sent_to_measured_pulse_ratio(
                    hist_name, pump_hist, probe_hist, detector_hist, delay_bins,
                    output_file=os.path.join(subfolder_path, "sent_to_measured_pulse_ratio.txt")
                )

                one_peak_count_testing(
                    1, hist_name, detector_hist, pump_hist, time_ps, delay_bins,
                    output_file=os.path.join(subfolder_path, "one_peak_count_testing.txt")
                )

                pulse_ratios_with_peak_counts(
                    hist_name, detector_hist, delay_bins,
                    os.path.join(subfolder_path, "pulse_ratios_with_peak_counts.txt")
                )

                pulse_ratios_with_integrated_counts(
                    hist_name, detector_hist, delay_bins,
                    os.path.join(subfolder_path, "pulse_ratios_with_integrated_counts.txt")
                )
        except Exception as e:
            print(f"Error {e} at {hist_name}")

    # python compute_metrics.py --histdir ../histograms --plot
