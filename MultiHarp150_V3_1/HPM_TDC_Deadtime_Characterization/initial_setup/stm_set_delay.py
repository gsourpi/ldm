import serial
import time
import sys
import argparse

"""
STM32 Delay Controller via UART

This script communicates with an STM32 microcontroller over UART to send delay values in picoseconds (ps). 
It allows for dynamic configuration via command-line arguments, enabling users to set the desirable
delay, repetition counts, and message durations.

Features:
- Sends delay values over UART to STM32.
- Configurable delay.
- Allows message repetition or time-based transmission.
- Supports different serial ports and baud rates.

Example Usage:
--------------
Custom parameters:
    python stm_set_delay.py --port /dev/ttyUSB0 --baudrate 115200  --delay 10000  --duration 60
Arguments:
----------
--port      : Serial port (e.g., /dev/ttyUSB2 or COM3) (Required)
--baudrate  : Baud rate (default: 115200)
--delay     : Delay to set in ps
--repeat    : Number of times to resend each message (default: infinite)
--duration  : Duration (seconds) to send each message (default: 2)
"""


def send_uart_commands(ser, msg):
    """Send a UART message to the STM32."""
    try:
        sys.stdout.flush()
        ser.write(msg.encode('utf-8'))
        sys.stdout.write(f"\r[PC -> STM32] Set delay to: {msg.strip()} ps")
        time.sleep(1)

    except KeyboardInterrupt:
        print("\nInterrupted by user.")
        ser.close()
        print("Serial port closed.")

def main():
    # Set up argument parsing
    parser = argparse.ArgumentParser(description="Send delay commands to STM32 over UART.")
    parser.add_argument("--port", type=str, required=True, help="Serial port (e.g., /dev/ttyUSB2 or COM3)")
    parser.add_argument("--baudrate", type=int, default=115200, help="Baud rate (default: 115200)")
    parser.add_argument("--delay", type=int, default=1000, help="Delay range in ps (default: 10000)")
    parser.add_argument("--repeat", type=int, default=float('inf'), help="Number of times to resend each message (default: infinite)")
    parser.add_argument("--duration", type=int, default=2, help="Duration (seconds) to send each message (default: 2)")

    args = parser.parse_args()

    try:
        ser = serial.Serial(args.port, args.baudrate, timeout=2)
        time.sleep(1)

        message = f"{args.delay}\r"

        start_time = time.time()
        count = 0
        while (count < args.repeat) and (time.time() - start_time < args.duration):
            send_uart_commands(ser, message)
            count += 1

        ser.close()

    except serial.SerialException as e:
        print(f"Error opening serial port: {e}")

if __name__ == "__main__":
    main()