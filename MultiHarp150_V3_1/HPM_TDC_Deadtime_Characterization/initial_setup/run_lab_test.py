import subprocess
import os 

def run_experiment(delay, acq_time, output_file):
    """
    Launch two processes in parallel:
    1. stm_delay_controller.py with --delay and --duration arguments.
    2. histmode.py with a matching acquisition_time_in_seconds (acq_time) 
       and an output filename (output_file).

    Both scripts run for the same duration (acq_time)

    We wait for both to finish before returning.
    """
    # python stm_set_delay.py --port /dev/ttyUSB0 --baudrate 115200  --delay 10000  --duration 60
    cmd1 = [
        "python",
        "stm_set_delay.py",
        "--port", "/dev/ttyUSB0",
        "--baudrate", "115200",
        "--delay", str(delay),
        "--duration", str(acq_time) 
    ]

    # python tdc_daq_sw/histomode.py --tacq 6 --outputfile histograms/output_file
    cmd2 = [
    "python",
    "tdc_daq_sw/histomode.py",
    "--tacq", str(acq_time),
    "--outputfile", output_file
    ]

    print(f"\nSet delay={delay}, duration={acq_time}")
    print(f"Starting histmode with duration={acq_time}, output_file={output_file}")

    # Start both processes in parallel and wait for them to finish
    p1 = subprocess.Popen(cmd1)
    p2 = subprocess.Popen(cmd2)

    p1.wait()
    p2.wait()

    print("\nBoth scripts finished.\n")

def main():

    os.makedirs('histograms', exist_ok=True)

    for i in range(1,9,1):
        delays = [i for i in range(500, 6000, 250)] # in ps

        # Define how long each run lasts (in seconds)
        acq_time = int(60*10)

        for delay in delays:
            output_file = f"histograms/hist_output_delay_{delay}ps_{i}.txt"
            run_experiment(delay, acq_time, output_file)

    print("All experiments finished.")
    
if __name__ == "__main__":
    main()
