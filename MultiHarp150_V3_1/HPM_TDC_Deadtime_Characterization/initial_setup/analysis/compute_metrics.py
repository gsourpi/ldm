import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from scipy.signal import find_peaks, peak_widths
import argparse
from scipy.optimize import curve_fit

def plot_histograms(time_ps, ref_hist, delay_hist, detector_hist):
    """Plots Reference, Delay, and Detector histograms in the same figure using subplots."""

    fig, axes = plt.subplots(3, 1, figsize=(10, 12))  # 3 rows, 1 column

    axes[0].plot(time_ps, ref_hist, label='Reference Histogram', linestyle='-', marker='o')
    axes[0].set_xlabel("Time (ps)")
    axes[0].set_ylabel("Counts")
    axes[0].set_title("Reference - Pump Histogram")
    axes[0].legend()
    axes[0].grid()

    axes[1].plot(time_ps, delay_hist, label='Delay Histogram', linestyle='-', marker='o', color='r')
    axes[1].set_xlabel("Time (ps)")
    axes[1].set_ylabel("Counts")
    axes[1].set_title("Delay - Probe Histogram")
    axes[1].legend()
    axes[1].grid()

    axes[2].plot(time_ps, detector_hist, label='Detector Histogram', linestyle='-', marker='o', color='g')
    axes[2].set_xlabel("Time (ps)")
    axes[2].set_ylabel("Counts")
    axes[2].set_title("Detector Histogram")
    axes[2].legend()
    axes[2].grid()

    plt.tight_layout()
    plt.show()


def compute_detection_efficiency(hist_name, ref_hist, delay_hist, detector_hist, delay_bins, output_file="detection_efficiency.txt"):
    """Computes and saves detection efficiency; Ratio of the pulses that we trigger to the pulses we are actually measuring."""
    total_pulses_sent = sum(ref_hist) + sum(delay_hist)
    total_pulses_measured = sum(detector_hist)
    missed_pulses = total_pulses_sent - total_pulses_measured

    if total_pulses_sent == 0:
        print("Error: No pulses sent, cannot compute detection efficiency.")
        return None

    detection_efficiency = total_pulses_measured / total_pulses_sent

    file_exists = os.path.isfile(output_file)
    is_empty = not file_exists or os.path.getsize(output_file) == 0

    with open(output_file, "a") as f:
        if is_empty: 
            f.write("Detection_Efficiency Delay(bins) Delay (ps) \n")
        f.write(f"{detection_efficiency:.6f} {delay_bins} {delay_bins*5}\n")

    #print(f"Computed detection efficiency saved to {output_file}")
    return detection_efficiency


def isolate_first_peak(hist_name, detector_hist, time_ps, delay_bins, plot=False):
    """Isolates and optionally plots the first detected peak."""
    peaks_idx, _ = find_peaks(detector_hist, height=0.0001*max(detector_hist), distance=delay_bins/2)

    if len(peaks_idx) == 0:
        print(f"Error: No peaks detected. At {hist_name}")
        return None, None


    results_half = peak_widths(detector_hist, peaks_idx, rel_height=0.5)
    peak_width = results_half[0][0] # of first peak - pump

    start_idx = int(np.round(peaks_idx[0] - 2 * peak_width))
    end_idx = int(np.round(peaks_idx[0] + 2 * peak_width))

    start_idx = max(start_idx, 0)
    end_idx = min(end_idx, len(detector_hist) - 1)

    if plot:
        plt.figure(figsize=(10, 6))
        plt.plot(time_ps[start_idx:end_idx], detector_hist[start_idx:end_idx], label='Isolated First Peak', linestyle='-', marker='o')
        plt.axvline(x=time_ps[peaks_idx[0]], color='red', linestyle='--', label="Detected Peak Center")
        plt.xlabel("Time (ps)")
        plt.ylabel("Counts")
        plt.title("Isolated First (Pump) Peak")
        plt.legend()
        plt.grid()
        plt.show()

    return start_idx, end_idx


def probe_testing(hist_name, detector_hist, ref_hist, time_ps, delay_bins, output_file="probe_testing.txt"):
    """Computes and saves the probability of detecting at least one photon per pulse. Ratio of counts in the first probe pulse to the number of laser triggers"""
    
    #NUM_LASER_TRIG = LASER_FREQ * ACQ_TIME  # Total number of laser triggers
    start_idx, end_idx = isolate_first_peak(hist_name, detector_hist, time_ps, delay_bins, plot=False)

    if start_idx is None or end_idx is None:
        print(f"Error: Could not isolate first peak at {hist_name}")
        return None

    counts_from_isol_peak = sum(detector_hist[start_idx:end_idx])
    peak_idx = start_idx + (end_idx - start_idx)/2
    counts_from_mean = detector_hist[int(round(peak_idx))]
    
    file_exists = os.path.isfile(output_file)
    is_empty = not file_exists or os.path.getsize(output_file) == 0

    with open(output_file, "a") as f:
        if is_empty: 
            f.write("counts_from_isol_peak counts_from_mean delay_bins hist_name \n")
        f.write(f"{counts_from_isol_peak:.6f} {counts_from_mean:.6f} {delay_bins} {hist_name}\n")

    return 

def gaussian(x, a, mu, sigma):
        """Gaussian function."""
        return a * np.exp(-((x - mu) ** 2) / (2 * sigma ** 2))

def fit_gaussian(data, plot=True):

    data = np.array(data)
    x =time_ps
    
    a_guess = np.max(data)
    mu_guess = np.sum(x * data) / np.sum(data)  # Weighted mean
    sigma_guess = np.sqrt(np.sum(data * (x - mu_guess) ** 2) / np.sum(data))  # Weighted std dev

    p0 = [a_guess, mu_guess, sigma_guess]

    popt, pcov = curve_fit(gaussian, x, data, p0=p0)
    return popt, pcov

def compute_delay_bins(ref_hist, delay_hist):
    pump_params, _ = fit_gaussian(ref_hist)
    probe_params , _ = fit_gaussian(delay_hist)

    delay_bins = probe_params[1] - pump_params[1]
    if delay_bins < 0: 
        return None
    return delay_bins


def compute_availability(histogram_name,detector_hist, delay_bins, output_file="availability_delay.txt"):
    """Computes and saves availability (S) and delay."""
    try: 
        peaks_idx, _ = find_peaks(detector_hist, height=0.001*max(detector_hist), distance=delay_bins/2)
    except: 
        print(f"Problem at {histogram_name}")

    if len(peaks_idx) < 2:
        print(f"Error: Not enough peaks found at {histogram_name}")
        return None

    pump_counts_M = detector_hist[peaks_idx[0]]
    probe_counts_H = detector_hist[peaks_idx[1]]
    t_delay = peaks_idx[1] - peaks_idx[0]

    tot_pulses = sum(detector_hist)
    pump_m = pump_counts_M / tot_pulses
    probe_h = probe_counts_H / tot_pulses

    availability_s = probe_h/pump_m

    file_exists = os.path.isfile(output_file)
    is_empty = not file_exists or os.path.getsize(output_file) == 0

    with open(output_file, "a") as f:
        if is_empty:
            f.write("Availability(S) Delay(bins) Delay (ps) Delay_of_ind (bins)\n")
        f.write(f"{availability_s:.6f} {t_delay} {t_delay*5} {delay_bins}\n")

    return availability_s, t_delay, t_delay*5


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process histogram files and compute various metrics.")
    parser.add_argument("--histdir", default="../histograms", help="The directory path containing histogram files")
    parser.add_argument("--resultsdir", default="analysis/results/", help="The directory path containing the results of metrics calculations")
    parser.add_argument("--plot", action="store_true", help="Optionally plot the histograms")
    args = parser.parse_args()

    results_folder = args.resultsdir
    if not os.path.exists(results_folder):
        os.makedirs(results_folder)

    histograms_dir = args.histdir
    histogram_files = os.listdir(histograms_dir)
    for histogram_name in histogram_files:
        try: 
            filename = os.path.join(histograms_dir, histogram_name)
            hist_name = os.path.splitext(histogram_name)[0]

            # Load data
            data = pd.read_csv(filename, sep='\\s+', header=None)
            non_zero_columns = (data != 0).any()
            data = data.loc[:, non_zero_columns]
            time_ps = np.arange(len(data)) 

            # Extract histograms
            histograms = {col: data[col].tolist() for col in data.columns}
            ref_hist = histograms[list(histograms.keys())[0]]
            delay_hist = histograms[list(histograms.keys())[1]]
            detector_hist = histograms[list(histograms.keys())[2]]

            if args.plot:
                plot_histograms(time_ps, ref_hist, delay_hist, detector_hist)
            
            delay_bins = compute_delay_bins(ref_hist, delay_hist)

            if delay_bins is not None: 
                #compute_availability(histogram_name, detector_hist, delay_bins, os.path.join(args.resultsdir, "availability_delay.txt"))
                #compute_detection_efficiency(hist_name, ref_hist, delay_hist, detector_hist, delay_bins, os.path.join(args.resultsdir, "detection_efficiency.txt"))
                probe_testing(hist_name, detector_hist, ref_hist, time_ps, delay_bins, os.path.join(args.resultsdir, "probe_testing.txt"))
        except Exception as e:
            print(f"Error {e} at {hist_name}")
                

# python compute_metrics.py --histdir ../histograms --plot
