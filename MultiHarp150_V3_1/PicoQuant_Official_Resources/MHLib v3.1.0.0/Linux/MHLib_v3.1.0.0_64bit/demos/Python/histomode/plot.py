import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

filename = "histomode.out"

data = pd.read_csv(filename, delim_whitespace=True, header=None)
non_zero_columns = (data != 0).any()
data = data.loc[:, non_zero_columns]

time_ps = np.arange(len(data)) * 5
colors = plt.get_cmap("tab10")

plt.figure(figsize=(10, 6))
for i, col in enumerate(data.columns):
    plt.plot(time_ps, data[col], label=f'Channel {col+1}', color=colors(i % 10))

plt.xlabel("Time (ps)")
plt.ylabel("Count")
plt.title("Filtered Histogram Data Plot (Non-Zero Columns Only)")
plt.legend()
plt.grid()

non_zero_bins = np.where(data.any(axis=1))[0]
if len(non_zero_bins) > 0:
    left_limit = max(0, non_zero_bins[0] - (non_zero_bins[-1] - non_zero_bins[0]) // 2)
    right_limit = min(len(time_ps) - 1, non_zero_bins[-1] + (non_zero_bins[-1] - non_zero_bins[0]) // 2)
    plt.xlim(time_ps[left_limit], time_ps[right_limit])

plt.show()

fig, axes = plt.subplots(len(data.columns), 1, figsize=(10, 3 * len(data.columns)), sharex=True)
if len(data.columns) == 1:
    axes = [axes]

for i, (ax, col) in enumerate(zip(axes, data.columns)):
    ax.plot(time_ps, data[col], label=f'Channel {col+1}', color=colors(i % 10))
    ax.set_ylabel("Count")
    ax.set_title(f"Histogram for Channel {col+1}")
    ax.legend()
    ax.grid()
    
    non_zero_bins = np.where(data[col] > 0)[0]
    if len(non_zero_bins) > 0:
        left_limit = max(0, non_zero_bins[0] - (non_zero_bins[-1] - non_zero_bins[0]) // 2)
        right_limit = min(len(time_ps) - 1, non_zero_bins[-1] + (non_zero_bins[-1] - non_zero_bins[0]) // 2)
        ax.set_xlim(time_ps[left_limit], time_ps[right_limit])

plt.xlabel("Time (ps)")
plt.tight_layout()
plt.show()
