import numpy as np
import pandas as pd

filename = "histomode.out"
data = pd.read_csv(filename, sep='\s+', header=None)
non_zero_columns = (data != 0).any()
data = data.loc[:, non_zero_columns]
time_ps = np.arange(len(data)) * 5

histograms = {col: data[col].tolist() for col in data.columns}

ref_hist      = histograms[list(histogram_lists.keys())[0]]
delay_hist    = histograms[list(histogram_lists.keys())[1]]
detector_hist = histograms[list(histogram_lists.keys())[2]]




