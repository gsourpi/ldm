The external FPGA interface (EFI) is a novel feature exclusive to the new
MultiHarp 160. It permits retrieving TTTR mode data at substantially higher 
bandwidth than via USB. This enables custom data processing in real-time,
way beyond the capabilities of a PC in terms of speed and latency. 
The zip archive here provides the required gateware, software, and 
documentation. Please unpack it to a folder of your choice and start by
studying the documentation.
Note that using the EFI requires the regular MultiHarp programming library
MHLib v3.0.0.0 or higher to be installed saparately.
Also note that if you start new work in this direction it is strongly 
recommended that you check the PicoQuant website for the most recent versions
of EFI and MHLib.
