FluoPlot

     3D-Visualization Tool
     for Time-Resolved
     Fluorescence Data

First Steps to FluoPlot
     Vers. 1.6

a) Be sure you have installed the latest driver package for your computer's
   graphics equipment provided by its manufacturer from the internet. Although
   most graphic cards will be supported by Microsoft's generic drivers, it is
   almost always necessary to enrich the supported features to the up-to-date
   level, since some of the generic drivers do only support subsets of the
   actual capabilities!

   FluoPlot makes extensive use of OpenGL features which first came with ver-
   sion 1.5. If your graphics device drivers don't support OpenGL or come
   with a version prior to V1.5, you might observe deficiencies in the visual
   presentation of your data. Which version of OpenGL is actually installed
   on your computer you may find out with GLInfo2.EXE

   If read the following error message
      --------------------------------------------------------------------
      FluoPlot
      --------------------------------------------------------------------
      Characters are too large or too many. Unable to create font texture.
      --------------------------------------------------------------------
   then your graphics device provides only insufficient texture support or
   (much more likely) its drivers are not up-to-date. FluoPlot needs parts of
   the texture structures to render the font for the labeling of the plots.


b) Even though you could simply start the FluoPlot.EXE without installing, you
   should at least invoke FluoPlot.REG by means of a simple double click to
   support the program with proper starting conditions. These are e.g. pre-
   stored default positions, a bunch of color schemes etc.

   This step is also a means to reset stored options to the defaults, if you
   should ever spoil your settings by accident or mistake.

Now, having taken care of the aforementioned steps, everything is ready to go.



Known problems:

-  You installed FluoPlot on your network but couldn't read pages of the
   FluoPlot Online Help, even though the content, index and search options
   seem to work?

   In response to increasing security problems involving MS HTML Help files
   (.CHM extension), Microsoft recently introduced Security Update 896358
   (see "http://support.microsoft.com/kb/896054"). The security update pre-
   vents the CHM files from opening unless those files reside locally on the
   user's hard drive. After the update is installed, users who access the
   help file from a server or network location will not be able to open it
   and will receive the messages

                        "Action canceled"               or
                  "The page cannot be displayed."

   This security update affects all CHM files, not just those distributed
   by PicoQuant GmbH.

   Work-Around 1: Deinstall FluoPlot and re-install it  *locally*  on your computer
                  - this solves the problem.

   Work-Around 2: Refer to Microsoft Knowledge Base ID 896054
                  "http://support.microsoft.com/kb/896054".
                  We recommend that you use Method 1: Modify the ItssRestrictions
                  registry entry to enable a specific Web application.

   Work-Around 3: Use HHReg.EXE
                  HHReg is a utility to register HTML Help files on an individual
                  Windows computer to make them available for viewing. HHReg removes
                  the upper mentioned limitation by explicitly registering HTML Help
                  CHM files in the Windows registry, making them available for viewing
                  even when opened on network drives.

                  Note:
                  * There was in fact a strong reason for Microsoft to restrict the
                    access to HTML Help files, since they could encapsulate active
                    code with possibly malicious purpose.
                  * If you register a helpfile, you accept the risk of lesser security!

                  The conclusion is:
                  !!!   Do only register HTML Help files of trusted origin   !!!

                  You could download the HHReg-Tool  from here:
                     "http://www.ec-software.com/products_hhreg.html"

                  !!!   Notice   !!!
                  With any change of your user credentials in the windows system
                  it is required to register your trusted help files again. This
                  especially comes to effect each time you change your password!


-  Look for other known problems at the online help pages.


Enjoy your work with FluoPlot.
