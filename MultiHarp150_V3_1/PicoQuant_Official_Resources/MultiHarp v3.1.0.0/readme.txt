This is the PicoQuant MultiHarp software pack.
It may be distributed on CD/DVD or as a zip file via download.
In case of a zip file, please unpack it to a temporary hard disk location. 

The folder "installation" contains the setup files.
You can run setup.exe directly from this folder on the CD or hard disk.
The setup program also installs the device driver for the MultiHarp. 

The MultiHarp software is supported for Windows 8.1, 10 and 11.
Windows 7 should still work but is no longer actively supported.  
Dependent on your version of Windows you may need to log on as administrator
in order to perform the software setup. It is important to perform the setup 
as administator because otherwise the device driver installation will fail.
Only if you intend to use the software without hardware, e.g. as a file 
viewer, you may run setup without administrator rights.

In your chosen installation folder Setup will create a subfolder FILEDEMO 
which contains demo source code for accessing the MultiHarp data files 
from various programming languages.

To uninstall the software you may need to log on as administrator 
(dependent on your version of Windows). Backup your measurement data.
From the start menu select:  PicoQuant - MultiHarp v.x.x  >  uninstall.
Alternatively you can use the Control Panel Wizard 'Programs and Features'


Disclaimer

PicoQuant GmbH disclaims all warranties with regard to this software 
including all implied warranties of merchantability and fitness. 
In no case shall PicoQuant GmbH be liable for any direct, indirect or 
consequential damages or any material or immaterial damages whatsoever 
resulting from loss of data, time or profits; arising from use, inability 
to use, or performance of this software and associated documentation. 


License and Copyright Notice

With the MultiHarp hardware product you have purchased a license to use 
the MultiHarp software. You have not purchased other rights to the software. 
The software is protected by copyright and intellectual property laws. 
You may not distribute the software to third parties or reverse engineer, 
decompile or disassemble the software or part thereof. You may use and 
modify demo code to create your own software. Original or modified demo 
code may be re-distributed, provided that the original disclaimer and 
copyright notes are not removed from it. Copyright of the manual and 
on-line documentation belongs to PicoQuant GmbH. No parts of it may be 
reproduced, translated or transferred to third parties without written 
permission of PicoQuant GmbH. 


Acknowledgements

The MultiHarp hardware in its current version uses the White Rabbit 
PTP core v. 4.0
(https://www.ohwr.org/projects/wr-cores/wiki/wrpc-release-v40) licensed 
under the CERN Open Hardware Licence v1.1 and its embedded WRPC software 
(https://ohwr.org/projects/wrpc-sw/wikis/home) licensed under GPL 
Version 2, June 1991. The WRPC software was minimally modified and in 
order to meet the licensing terms the modified WRPC source code is 
provided as part of the MultiHarp software distribution media.
When the MultiHarp software is used under Linux it uses Libusb to access 
the MultiHarp USB devices. Libusb is licensed under the LGPL which allows 
a fairly free use even in commercial projects. For details and precise
terms please see http://libusb.info. In order to meet the license 
requirements a copy of the LGPL as appliccable to Libusb is provided 
as part of the MultiHarp software distribution media. The LGPL does not 
apply to the MultiHarp software as a whole.


Trademark Disclaimer

Products and corporate names appearing in the product manuals or in the 
online documentation may or may not be registered trademarks or copyrights 
of their respective owners. They are used only for identification or 
explanation and to the owner�s benefit, without intent to infringe.


Contact and Support

PicoQuant GmbH
Rudower Chaussee 29
12489 Berlin, Germany
Phone +49 30 1208820-0
Fax   +49 30 1208820-90
email info@picoquant.com
web   www.picoquant.com

 