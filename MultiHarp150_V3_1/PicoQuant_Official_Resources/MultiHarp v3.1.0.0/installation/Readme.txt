MultiHarp 150/160 TCSPC Software version 3.1.0.0
PicoQuant GmbH - March 2022


Introduction

The MultiHarp 150/160 are TCSPC systems with USB 3.0 interface. 
The systems require a 686 class PC with suitable USB host controller,
4 GB of memory, two or more CPU cores and at least 2 GHz CPU clock. 
The MultiHarp software is supported on Windows 8.1, 10 and 11 
including the x64 versions. 


What's new in this version 

- Supports event filtering in the time tagging modes 
  (requires suitable firmware) 
- Fixes some small issues in code and documentation
- The file format remains compatible
- Now officially supporting Windows 11


What was new in version 3.0

- Supports the new scalable hardware models MultiHarp 160 with up to 
  64 channels, 5 ps resolution and a high speed external FPGA interface 
- Suppors a programmable input hysteresis (requires suitable firmware) 
- Fixes a critical bug of version 2.0 where EEPROM data may be lost 
  when using White Rabbit 
- Some minor bugfixes 


What was new in version 2.0

- Supporting the new high resolution models MultiHarp 150  4P/8P/16P 
- Providing a new device driver supporting "secure boot" with Windows 10 
- Updated bibliography with reference paper on the MultiHarp 150 
- Some minor bugfixes 


What was new in version 1.1

- Supporting the 16-channel model 
- Supporting a programmable dead-time (requires suitable firmware)
- Supporting operation under Linux with Wine 
- Some minor bugfixes 


Disclaimer

PicoQuant GmbH disclaims all warranties with regard to this software 
including all implied warranties of merchantability and fitness. 
In no case shall PicoQuant GmbH be liable for any direct, indirect or 
consequential damages or any material or immaterial damages whatsoever 
resulting from loss of data, time or profits; arising from use, inability 
to use, or performance of this software and associated documentation. 


License and Copyright Notice

With the MultiHarp hardware product you have purchased a license to use 
the MultiHarp software. You have not purchased other rights to the software. 
The software is protected by copyright and intellectual property laws. 
You may not distribute the software to third parties or reverse engineer, 
decompile or disassemble the software or part thereof. You may use and 
modify demo code to create your own software. Original or modified demo 
code may be re-distributed, provided that the original disclaimer and 
copyright notes are not removed from it. Copyright of the manual and 
on-line documentation belongs to PicoQuant GmbH. No parts of it may be 
reproduced, translated or transferred to third parties without written 
permission of PicoQuant GmbH. 


Acknowledgements

The MultiHarp hardware in its current version uses the White Rabbit 
PTP core v. 4.0
(https://www.ohwr.org/projects/wr-cores/wiki/wrpc-release-v40) licensed 
under the CERN Open Hardware Licence v1.1 and its embedded WRPC software 
(https://ohwr.org/projects/wrpc-sw/wikis/home) licensed under GPL 
Version 2, June 1991. The WRPC software was minimally modified and in 
order to meet the licensing terms the modified WRPC source code is 
provided as part of the MultiHarp software distribution media.
When the MultiHarp software is used under Linux it uses Libusb to access 
the MultiHarp USB devices. Libusb is licensed under the LGPL which allows 
a fairly free use even in commercial projects. For details and precise
terms please see http://libusb.info. In order to meet the license 
requirements a copy of the LGPL as appliccable to Libusb is provided 
as part of the MultiHarp software distribution media. The LGPL does not 
apply to the MultiHarp software as a whole.


Trademark Disclaimer

Products and corporate names appearing in the product manuals or in the 
online documentation may or may not be registered trademarks or copyrights 
of their respective owners. They are used only for identification or 
explanation and to the owner�s benefit, without intent to infringe.


Installation 

The MultiHarp software can be distributed on CD/DVD or via download.
If you received the package via download it is packed in a zip-file. 
Unzip that file and place the distribution setup files in a temporary 
disk folder. 

Dependent on your version of Windows you may need to log on as administrator
in order to perform the software setup. It is important to perform the setup 
as administator because otherwise the device driver installation will fail.
Only if you intend to use the software without hardware, e.g. as a file 
viewer, you may run setup without administrator rights.

The setup program will install the MultiHarp software including driver, 
manual, online-help, sample data and programming demos for data access. 
The setup program will copy the device driver but it will not immediately 
load it.  This is done by the regular Windows Plug&Play device management 
when you connect the device. Dependent on the version of Windows you may be 
prompted again at this stage in oder to confirm the driver installation.

To uninstall the MultiHarp software you may need to log on as administrator 
(dependent on your version of Windows). Backup your measurement data.
From the start menu select:  PicoQuant - MultiHarp v.x.x  >  uninstall.
Alternatively you can use the Control Panel Wizard 'Add/Remove Programs'
(in some Windows versions this is called 'Software' or 'Apps&Features')


Contact and Support

PicoQuant GmbH
Rudower Chaussee 29
12489 Berlin, Germany
Phone +49 30 1208820-0
Fax   +49 30 1208820-90
email info@picoquant.com
www http://www.picoquant.com
