The MultiHarp software can also be used under Linux (x86 platform only). 
This requires that Wine is installed (see https://www.winehq.org)
as well as Libusb 1.0 (see https://libusb.info/).
You can then run the regular software setup as explained in the MultiHarp
manual. 
In order to set the access permissions to the device, use the udev 
rules file provided here. For details please see the MultiHarp manual 
section 8.4. "Using the Software under Linux".