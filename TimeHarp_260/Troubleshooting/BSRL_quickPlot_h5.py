import h5py 
import numpy as np
from matplotlib import pyplot as plt 
import struct 
import argparse
import struct
from matplotlib import pyplot as plt
from matplotlib.ticker import EngFormatter
import numpy as np
import mpld3
import datetime as dt

TDC_RES = 25e-12
#date = dt.datetime(2024, 07, 07, 14, 30, 45)

def main(hist_file, save):
   with h5py.File(hist_file, 'r') as hf:
      available_keys = list(hf.keys())
      
      print("Enter the date and time in the format 'DD_MM_YYYY_hh_mm_ss'. For example: '07_07_2024_050001'.")
      while True:
         chosen_key = input("Enter the wanted key: ")
         if chosen_key in available_keys:
               break
         else:
               print("No data available for this date and/or time. Please enter a valid info.")

      binary_data = hf[chosen_key][:]
      binary_data = binary_data.tobytes()
      
   num_bins = len(binary_data) // 4
   histogram = list(struct.unpack(f'{num_bins}I', binary_data))
   
   xdata=list( range(0,len(histogram) ) )
   xdata = np.array(xdata) * TDC_RES
   fig, ax = plt.subplots()
   formatter = EngFormatter(unit='s')
   ax.xaxis.set_major_formatter(formatter)
   
   #ax.set_yscale('log') # Set logarithmic scale. 
   ax.set_xlabel('Time (s)')
   ax.set_ylabel('Count')
   ax.set_title(f'Plot of: {chosen_key} key.')

   ax.plot(xdata,histogram)
   if save: 
      plt.savefig(f'{chosen_key}_hist.png')
      print(f"Plot of {chosen_key} saved successfully.")
   plt.show()
   

if __name__ == "__main__":
   parser = argparse.ArgumentParser(description="Process and save data from an HDF5 file.")
   parser.add_argument('--hist_file', type=str, required=True, help='Path to the histogram file (expected .h5 file)')
   parser.add_argument('--save', action='store_true', default=False, help='Flag to save the extracted plot.')

   args = parser.parse_args()
   main(args.hist_file, args.save)