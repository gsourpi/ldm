# Plots the data from the quick acquisition of BSRL.
# 
# Miguel

import argparse
import struct
from matplotlib import pyplot as plt
from matplotlib.ticker import EngFormatter
import numpy as np

TDC_RES = 25e-12

def main(hist_file):
    with open(hist_file, 'rb' ) as file:
        binaryData = file.read()

    # Number of bins
    num_bins = len(binaryData) // 4
    histogram = list(struct.unpack(f'{num_bins}I', binaryData))

    xdata=list( range(0,len(histogram) ) )
    xdata = np.array(xdata) * TDC_RES
    fig, ax = plt.subplots()
    formatter = EngFormatter(unit='s')
    ax.xaxis.set_major_formatter(formatter)
    ax.set_yscale('log')

    ax.plot(xdata,histogram)
    plt.show()



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process files.')
    parser.add_argument('--hist_file', help='Histogram file (32-bit ints)')
    args = parser.parse_args()
    main(args.hist_file)