import struct

with open('/home/gsourpi/Desktop/LDM/tttrmode3.out', 'rb') as inputfile:
    binary_data = inputfile.read()

# '<' means that the msb is on the rightmost side 
# 'I' stands for unsigned integer of 4 bytes = 32 bits
unpacked_data = struct.unpack('<' + 'I' * (len(binary_data) // 4), binary_data)

with open('/home/gsourpi/Desktop/LDM/mod2.txt', 'w') as output_file:
    for uint in unpacked_data:
        output_file.write(str(uint) + '\n')
        


