from matplotlib import pyplot as plt
import numpy as np
import timeit
from line_profiler import LineProfiler

# Not fully implemented: markers might be the last word in buffer as well as regular marker activity;
# Also, this does not care about which channel the hit came from

OVF_PERIOD = 33554432
numberOverflows = 0 

def T2Decode(word):

    global overflows

    specialBitMask = 0x80000000
    channelMask = 0x7E000000
    timeTagMask = 0x1FFFFFF
    
    # Check if special bit is set
    specialBit = word & specialBitMask
    if (specialBit != 0):
        # Check if word is overflow, print number of overflows
        if( (word & channelMask) >> 25 == 63 ):
            type = "overflow"
            data = word & timeTagMask
            numberOverflows =+ 1

        # Check if word is sync event, print timetag of sync
        elif( (word & channelMask) >> 25 == 0 ):
            type  = "sync"
            data = word & timeTagMask
            
        # Otherwise, word is a marker with a timetag
        else:
            type = "marker"
            data = (word & channelMask) >> 25

    # If special bit is not set, word is a regular event
    else:
        type = "regular"
        # For our use, we have only one channel, so we don't care about which channel the hit came from
        channel = (word & channelMask) >> 25
        data = word & timeTagMask

    return[type, data]


print(T2Decode(16318547))

TDCData = []
import ast
import re
with open("/home/gsourpi/Desktop/LDM/tttrmode4.out", "r") as file:
    contents = file.read()
    
    string_lists = contents
    
    # Find all occurrences of ']' except the last one
    replaced_string = re.sub(r'\]', '', string_lists, count=len(re.findall(r'\[', string_lists)) - 1)

    # Convert each remaining list string to a list of integers using ast.literal_eval()
    result = []
    for list_string in replaced_string.split():
        list_values = [int(x) for x in re.findall(r'\d+', list_string)]
        result.extend(list_values)

# # Decode text output into a list
# import ast 
# contents = ast.literal_eval(contents)

# import re
# # Find the substring between the first '[' and the last ']'
# match = re.search(r'\[(.*?)\]', contents)

# # Extract the substring containing the list
# if match:
#     list_string = match.group(1)

#     # Split the substring by commas
#     list_items = list_string.split(',')

#     # Remove any empty strings or whitespace
#     list_items = [item.strip() for item in list_items if item.strip()]

#     # Convert the list items to integers
#     extracted_list = [int(item) for item in list_items]


for word in result: 
    decodedWord = T2Decode(int(word)) 
    TDCData.append(decodedWord)
    
# Split list into turns (individual lists with sync as the starting item)
TDCSeries = np.array(TDCData)
idx = np.where(TDCSeries == "sync" )[0]

subarrays = np.split(TDCSeries, idx)
TDCSeries = [subarray.tolist() for subarray in subarrays if len(subarray) > 0]

TDCTurnHits = [[]]
# Use the sync time to normalize arrival time in each turn
for turn in TDCSeries:
    hitsOffsetCorrected = []
    tOffset = turn[0][1]
    for hit in turn[1:]:
        hitsOffsetCorrected.append( int(hit[1]) - int(tOffset) )
    # Clear negative elements (end marker)
        hitsOffsetCorrected = [item for item in hitsOffsetCorrected if item >= 0]
    TDCTurnHits.append(hitsOffsetCorrected)

counts = {}

def updatePlot(bins,counts): 
    # Clear the previous plot 
    plt.clf()
    
    # Combine the lists into a list of tuples
    combined = list(zip(bins, counts))

    # Sort the combined list based on the first element of each tuple (bins)
    sortedCombined = sorted(combined, key=lambda x: x[0])

    # Unzip the sorted list of tuples back into separate lists
    bins, counts = zip(*sortedCombined)

    plt.bar(np.arange(len(bins)), counts, width=0.8, color = 'maroon') 
    plt.xticks(np.arange(len(bins)), bins)
    plt.xlabel('Bins')
    plt.ylabel('Counts')
    plt.title('Histogram of Bins')
    plt.grid(True)
    plt.xticks(rotation=90)  
    plt.tight_layout()  
    plt.draw()
    plt.show()

    
import random

for row in TDCTurnHits:
    # for num in row: 
    # # fisrtNUM = row[0]
    # if row: 
    #     #num = random.choice(row)
    #     nums = random.sample(row,2)
    #     for num in nums: 
        for num in row:
            if num in counts:
                counts[num] += 1
                # # For each Hit update the plot 
                # bins = list(counts.keys())
                # countsValues = list(counts.values())
                # # Update plot
                # updatePlot(bins, countsValues)
            else:
                counts[num]  = 1
                # # For each Hit update the plot 
                # bins = list(counts.keys())
                # countsValues = list(counts.values())
                # # Update plot
                # updatePlot(bins, countsValues)
            

# For each Hit update the plot 
bins = list(counts.keys())
countsValues = list(counts.values())
# Update plot
updatePlot(bins, countsValues)

# groupedBins= []
# groupedcountValues = [0]*10
# for i, num in enumerate(bins):
#     # Check if the number falls within ±5 of any group
#     found_group = False
#     for j, group in enumerate(groupedBins):
#         if np.any(np.abs(np.array(group) - num) <= 9):
#             groupedcountValues[j] += countsValues[i]
#             found_group = True
#             break
#     # If the number doesn't fit within any existing group, create a new group
#     if not found_group:
#         groupedBins.append(num)
#         groupedcountValues[len(groupedBins) -1] += countsValues[i]
        

""" x = []
y = []
for idx, lst in enumerate( (syncs, hits, markers) ):
    for time in lst:
        x.append(time)
        y.append(idx)

plt.ylim((-5, 5))
plt.yticks([0,1,2], [ 'SYN', 'HIT', 'MRKR'])
plt.scatter(x, y)
plt.show() """
