from matplotlib import pyplot as plt
import numpy as np 
def updatePlot(counts): 
    
    # Filter bins and counts to include only bins with counts > 0
    # non_zero_indices = np.where(counts > 0)[0]
    # filteredBins = bins[non_zero_indices]
    # filteredCounts = counts[non_zero_indices]
    
    # Clear the previous plot 
    plt.clf()
    
    # We have predefined the bins in an ascending order, so no sorted() is needed
    #plt.hist(counts, 1000)
    plt.bar( np.arange(int(5 * 1e6)), counts, width=0.8, color = 'maroon')
    
    # plt.xticks(np.arange(len(bins)), bins)
    plt.xlabel('Bins')
    plt.ylabel('Counts')
    plt.title('Histogram of Bins')
    plt.grid(True)
    plt.xticks(rotation=90)  
    plt.tight_layout()  
    plt.draw()
    plt.pause(0.5)
    
def updateRect1(counts):
    # Clear previous plot
    plt.clf()

    # Create rectangles for non-zero counts
    rects = [plt.Rectangle((i, 0), 1.0, count) for i, count in enumerate(counts) if count != 0]

    # Set labels and aspect ratio
    plt.xlabel('Bins')
    plt.ylabel('Counts')
    
    # Adjust the limits of the axes to include all rectangles
    ax = plt.gca()
    ax.set_xlim(np.arange(len(counts) + 1).min(), np.arange(len(counts) + 1).max())  # +1 for including the last bin
    ax.set_ylim(np.arange(max(counts) + 1).min(), np.arange(max(counts) + 1).max())

    if not rects:
        print("poylo")
        return  # If there are no rectangles to draw, return without showing the plot

    # Add rectangles to the current axes
    for rect in rects:
        ax.add_patch(rect)
        plt.draw()
        plt.pause(0.5)
        
def updateRect(counts, figNumber):
    # Generate the plot 
    plt.figure(figNumber)
    
    # Normalize counts
    #normalized_counts = [count / max(counts) for count in counts if count != 0]

    # Create rectangles for non-zero counts
    
    # plt.Rectangle(anchor_point, width, height)
    # The count index number is the bin numbers.
    # i = list(np.where(counts)[0])
    # i_max = i.max()
    
    #rects = [plt.Rectangle((i // i_max , 0), 1.0, normalized_count) for i, normalized_count in enumerate(normalized_counts) if normalized_count != 0]
    
    indexes = np.where(counts)[0]
    i_max = indexes.max()
    nonzeroCountsList = list(indexes)
    len_counts = len(nonzeroCountsList)
    max1 = max(counts)
    print(max1, len_counts)
    rects = [plt.Rectangle((i // i_max , 0), 1.0, counts[i]//max(counts)) for i in nonzeroCountsList]
    if not rects:
        return  # If there are no rectangles to draw, return without showing the plot

    # Add rectangles to the current axes
    ax = plt.gca()
    for rect in rects:
        ax.add_patch(rect)

    # Set labels and aspect ratio
    plt.xlabel(f'Normalised Bins at {i_max}')
    plt.ylabel(f'Normalized Counts at {max(counts)}')
    
    plt.grid('True')

    # # Use a logarithmic scale for the y-axis
    # ax.set_yscale('log')
    # ax.set_xscale('log')

    # Adjust the limits of the x-axis to include all bins
    plt.xlim(0, 1)
    
    
        
def Plot(counts, avgNumHits):
        
    # Define average number of hits so that each window is one burst
    avgNumHits = avgNumHits
    indexes = np.where(counts)[0]

    # Create a figure and axis
    fig, ax = plt.subplots()
    plt.subplots_adjust(bottom=0.25)  # Adjust bottom margin to make room for the slider

    # Plot the data - Create bar plot
    bars = ax.bar(range(len(indexes)), counts)

    # Create slider for x-axis scrolling
    ax_slider = plt.axes([0.1, 0.1, 0.8, 0.05], facecolor='lightgoldenrodyellow')
    x_slider = Slider(ax_slider, 'Time Bins', 0, len(counts) - avgNumHits, valinit=0)

    # Function to update plot when slider value changes
    def update(val):
        xmin = int(x_slider.val)
        xmax = xmin + avgNumHits
        
        # Update x-axis limits
        ax.set_xlim(xmin, xmax)
        
        # Extract data within the specified window
        window_counts = counts[xmin:xmax+1]
        
        # Update y-axis limits based on data within the window
        ax.set_ylim(0, max(window_counts) * 1.1)  # Set y-axis limits slightly above maximum count
        
        fig.canvas.draw_idle()

    # Register the update function with the slider
    x_slider.on_changed(update)

    plt.xlabel('Bins - Time Difference')
    plt.ylabel('Counts')
    plt.title('Interactive Mode')
    plt.grid(True)
    plt.show()