from matplotlib import pyplot as plt
import numpy as np

# Not fully implemented: markers might be the last word in buffer as well as regular marker activity;
# Also, this does not care about which channel the hit came from

OVF_PERIOD = 33554432

# These lines are for making a plot
overflows = 0
syncs = []
hits = []
markers = []

def T2Decode(word):

    global overflows

    specialBitMask = 0x80000000
    channelMask = 0x7E000000
    timeTagMask = 0x1FFFFFF
    
    # Check if special bit is set
    specialBit = word & specialBitMask
    if (specialBit != 0):
        # Check if word is overflow, print number of overflows
        if( (word & channelMask) >> 25 == 63 ):
            type = "overflow"
            data = word & timeTagMask

            # These lines are for making a plot
            overflow = word & timeTagMask
            print("Overflow %s" % overflow)
            overflows += 1


        # Check if word is sync event, print timetag of sync
        elif( (word & channelMask) >> 25 == 0 ):
            type  = "sync"
            timeTag = word & timeTagMask
            
            # These lines are for making a plot
            syncs.append(timeTag + OVF_PERIOD*overflows)
            print("Sync %s" % timeTag)

        # Otherwise, word is a marker with a timetag
        else:
            type = "marker"
            data = (word & channelMask) >> 25
            
            # These lines are for making a plot
            timeTag = word & timeTagMask
            markers.append(timeTag + OVF_PERIOD*overflows)
            print("Marker: %s, %s" % (data, timeTag))


    # If special bit is not set, word is a regular event
    else:
        type = "regular"
        # For our use, we have only one channel, so we don't care about which channel the hit came from
        channel = (word & channelMask) >> 25
        timeTag = word & timeTagMask

        # These lines are for making a plot
        timeTag = word & timeTagMask
        hits.append(timeTag + OVF_PERIOD*overflows)
        print("Channel %s: %s" % (channel, timeTag))
        #overflow = 0
    #return[type, data]


TDCData = []
syncEvent = -1

file = open("/home/gsourpi/Desktop/LDM/Outputs/initial_channels_T2_mode.txt")
# Decode text output into a list
for line in file:
    TDCData.append( T2Decode(int(line)) )

x = []
y = []
for idx, lst in enumerate( (syncs, hits, markers) ):
    for time in lst:
        x.append(time)
        y.append(idx)

plt.ylim((-5, 5))
plt.yticks([0,1,2], [ 'SYN', 'HIT', 'MRKR'])
plt.scatter(x, y)
plt.show()
