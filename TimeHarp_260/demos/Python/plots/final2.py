import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np

def plotHist(counts):
    # Define average number of hits so that each window is one burst
    avgNumHits = 8

    # Get Data - Bins and Counts 
    indexes = np.where(counts)[0]
    #counts =[i for i in counts if i in indexes]
    counts = np.array(counts)
    # Create a figure and axis
    fig, ax = plt.subplots()
    plt.subplots_adjust(bottom=0.25)  # Adjust bottom margin to make room for the slider

    # Plot the data - Create bar plot
    bars = ax.bar(indexes, counts[indexes])

    # Create slider for x-axis scrolling
    ax_slider = plt.axes([0.1, 0.1, 0.8, 0.05], facecolor='lightgoldenrodyellow')
    x_slider = Slider(ax_slider, 'Time Bins', 0, len(counts) - avgNumHits, valinit=0)

    # Function to update plot when slider value changes
    def update(val):
        xmin = int(x_slider.val)
        xmax = xmin + avgNumHits
        
        # Update x-axis limits
        ax.set_xlim(xmin, xmax)
        
        # Extract data within the specified window
        window_counts = counts[xmin:xmax+1]
        
        # Update y-axis limits based on data within the window
        ax.set_ylim(0, max(window_counts) * 1.1)  # Set y-axis limits slightly above maximum count
        
        fig.canvas.draw_idle()

    # Register the update function with the slider
    x_slider.on_changed(update)

    plt.xlabel('Bins - Time Difference')
    plt.ylabel('Counts')
    plt.title('Interactive Mode')
    plt.grid(True)
    plt.show()
