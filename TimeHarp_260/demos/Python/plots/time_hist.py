import matplotlib.pyplot as plt
from matplotlib.widgets import Button, TextBox
import numpy as np

window_start, window_end = 0, 0
numButtonPressed = -1
numBurstsForEveryHit = []
sync_index = []
sync = False

# def T2Decode(word):

#     global overflows
#     global sync
#     sync = False

#     specialBitMask = 0x80000000
#     channelMask = 0x7E000000
#     timeTagMask = 0x1FFFFFF
#     data = 0
    
#     # Check if special bit is set
#     specialBit = word & specialBitMask
#     if (specialBit != 0):

#         # Check if word is sync event, print timetag of sync
#         if( (word & channelMask) >> 25 == 0 ):
#             type  = "sync"
#             data = word & timeTagMask
#             sync = True
            
#     # If special bit is not set, word is a regular event
#     else:
#         type = "regular"
#         # For our use, we have only one channel, so we don't care about which channel the hit came from
#         data = word & timeTagMask

#     return data, sync


# def read_list_from_file(file_path):
#     with open(file_path, 'r') as f:
#         # Read the contents of the file and split by newline
#         lines = f.readlines()
#         # Remove newline characters from each line and return the list
#         return [int(line.strip()) for line in lines]

# # File path of the text file
# file_path = '/home/gsourpi/Desktop/LDM/Outputs/initial_channels_T2_mode.txt'

# TDCData = []
# sync_index = []
# preallocateCounts = [0] * int(1 * 1e6)

# # Read the list from the file
# words = read_list_from_file(file_path)
# for word in words: 
#     decodedWord, sync = T2Decode(word)
#     if sync == False: 
#         TDCData.append(decodedWord)
#     else: 
#         sync_index.append(len(TDCData))
        

def plotHist(counts, syncTimetags, numButtonPressed):
    
    # Get Data - Bins and Counts 
    # sync_index.insert(0,0) # so that each window will be: [0,sync_1], [sync_1,sync_2] etc.
    
    # Indexes where we have non-zero counts
    indexes = np.where(counts)[0]
    nonzeroCountsList = list(indexes)
    
    for i in nonzeroCountsList:
        print(i)
        print(counts[i])

    rects = [plt.Rectangle((i, 0), 1, counts[i]) for i in nonzeroCountsList]

    if not rects:
        print("No rectangles to plot.") 
        
    # Create a figure and axis
    fig, ax = plt.subplots()
    plt.subplots_adjust(bottom=0.25)  # Adjust bottom margin to make room for the buttons
    
    # Plot the data - Add rectangles to the current axes
    for rect in rects:
        ax.add_patch(rect)

    # Define navigation button positions and labels
    left_button_ax = plt.axes([0.1, 0.05, 0.1, 0.05])
    right_button_ax = plt.axes([0.3, 0.05, 0.1, 0.05])
    text_box_ax = plt.axes([0.5, 0.05, 0.2, 0.05])

    left_button = Button(left_button_ax, '<- Previous')
    right_button = Button(right_button_ax, 'Next ->')
    text_box = TextBox(text_box_ax, 'Bin Range:', initial="0-1")

    # Function to update plot when the buttons are clicked
    def update_window(direction):
        
        global window_start, window_end, numButtonPressed
        if direction == 'next':
            print("Next Button was pressed.")
            numButtonPressed += 1
        elif direction == 'previous':
            print("Previous Button was pressed.")
            numButtonPressed -= 1
            
        if numButtonPressed >= len(sync_index) - 1:
            numButtonPressed = len(sync_index) - 2  # Adjust to stay within bounds
        elif numButtonPressed < 0:
            numButtonPressed = 0  # Ensure it doesn't go below 0
        
        
        # # Ensure numButtonPressed stays within bounds
        # numButtonPressed = max(0, min(numButtonPressed, len(sync_index) - 1))        
        window_start = int(sync_index[numButtonPressed])
        window_end   = int(sync_index[numButtonPressed + 1])
        print(f"Bin start is: {window_start} and end is: {window_end}")

        
        # Update x-axis limits
        ax.set_xlim(window_start, window_end)
        # Extract data within the specified window
        window_counts = counts[window_start:window_end]
        
        # Remove zeros from the window counts list since they are not plotted 
        # Won't slow the run time as the window has few counts
        window_counts = [count for count in window_counts if count != 0]
        print(f"Count start is: {min(window_counts)} and end is: {max(window_counts)}")

        # Update y-axis limits based on data within the window
        ax.set_ylim(min(window_counts)*0.7, max(window_counts) * 1.1)  
        
        ax.grid(True, linestyle='--', linewidth=0.5, color='gray') 
        fig.canvas.draw_idle()
        
    def update_window_from_text(text):
        global window_start, window_end
        start, end = map(int, text.split('-'))
        window_start = start
        window_end = end
        ax.set_xlim(window_start, window_end)
        window_counts = counts[window_start:window_end]
        window_counts = [count for count in window_counts if count != 0]
        ax.set_ylim(min(window_counts)*0.7, max(window_counts) * 1.1)  
        ax.grid(True, linestyle='--', linewidth=0.5, color='gray') 
        fig.canvas.draw_idle()
    

    # Register the update function with the buttons
    left_button.on_clicked(lambda event: update_window('previous'))
    right_button.on_clicked(lambda event: update_window('next'))
    text_box.on_submit(update_window_from_text)
    
    plt.xlabel('Bins - Time Difference')
    plt.title('Interactive Mode')
    plt.grid(True)
    plt.show()
    

    
# plotHist(TDCData, sync_index)
# print(TDCData)
# print(sync_index)
