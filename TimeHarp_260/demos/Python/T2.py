import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


time_tag_mask = 0x1FFFFFF
channel_mask  = 0x7E000000
special_mask  = 0x80000000
time_resolution    = 25

events = {
    "sync": [],
    "timetag": [], 
    "overflow":[]
}
TDCEvents = pd.DataFrame(events)

with open('/home/gsourpi/Desktop/LDM/Outputs/initial_channels_T2_mode.txt', 'r') as file:
    
    for data in file:
        
        #Import the TDC data
        TDCData = int(data.strip())
        
        # Bit masks
        time_tag = TDCData & time_tag_mask
        channel  = 1 >> TDCData >> 25
        special_bit = TDCData >> 31
        
        if special_bit == 1: 
            if channel == 0: 
                print("Synchronization event detected...")
                TDCEvents.loc[len(TDCEvents)] = [time_tag, 0, 0]   
                
            elif channel == 63: 
                    print("Oveflow!")
                    print(f"Total number of overflows is: {time_tag}")
                    TDCEvents.loc[len(TDCEvents)] = [0, 0, time_tag]
        else: 
            # If there is at least one sync pulse start recording the measurements
            if (TDCEvents['sync'] != 0).any(): 
                print("Regular event recorded...")
                sync_timetag = TDCEvents[TDCEvents['sync'] != 0].index.max()
                print(sync_timetag)
                TDCEvents.loc[len(TDCEvents)] = [0, time_tag - sync_timetag, 0]

print(TDCEvents.head(20))
print(np.count_nonzero(TDCEvents.values[TDCEvents['overflow'].values != 0]))
print(TDCEvents.describe())


print("Histogram Generation...")

total_time_ps = int(1e3) # total measurement time
bin_size_ps = 25
num_bins = total_time_ps // bin_size_ps

plt.hist(np.array(TDCEvents[:]['timetag']).flatten(), bins = num_bins, range=(0, total_time_ps))  
plt.xlabel('Time (ps)')
plt.ylabel('Counts')
plt.grid(True)
plt.title(f'Histogram of Counts for {total_time_ps} msec Measurement')
plt.xticks(range(0, total_time_ps + bin_size_ps, bin_size_ps))
plt.show()