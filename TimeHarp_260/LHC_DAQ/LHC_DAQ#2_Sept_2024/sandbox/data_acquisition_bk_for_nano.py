import ctypes as ct
from ctypes import byref
import os
import sys
import time
import struct
from matplotlib import pyplot as plt
import numpy as np
import struct
import h5py

import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np

import argparse

if sys.version_info[0] < 3:
    print("[Warning] Python 2 is not fully supported. It might work, but "
            "use Python 3 if you encounter errors.\n")
    raw_input("press RETURN to continue"); print
    input = raw_input

LIBRARY_PATH = "/afs/cern.ch/user/g/gsourpi/ldm/TimeHarp_260/"

# From th260defin.h
LIB_VERSION = "3.2"
MAXDEVNUM = 4
MODE_T2 = 2
MODE_T3 = 3
MAXLENCODE = 5
MAXINPCHAN = 2
TTREADMAX = 131072
FLAG_OVERFLOW = 0x0001
FLAG_FIFOFULL = 0x0002

# Variables to store information read from DLLs
buffer = (ct.c_uint * TTREADMAX)()
dev = []
libVersion = ct.create_string_buffer(b"", 8)
hwSerial = ct.create_string_buffer(b"", 8)
hwPartno = ct.create_string_buffer(b"", 8)
hwVersion = ct.create_string_buffer(b"", 16)
hwModel = ct.create_string_buffer(b"", 16)
errorString = ct.create_string_buffer(b"", 40)
numChannels = ct.c_int()
resolution = ct.c_double()
syncRate = ct.c_int()
countRate = ct.c_int()
flags = ct.c_int()
nRecords = ct.c_int()
ctcstatus = ct.c_int()
warnings = ct.c_int()
warningstext = ct.create_string_buffer(b"", 16384)

# Initialization for decoding of the TimeHarp TDC 
# Bit masks initialization 
specialBitMask = 0x80000000
channelMask = 0x7E000000
syncMask = 0xFE000000
syncEventMasked = 0x80000000
overflowEventMasked = 0xFE000000
timeTagMask = 0x1FFFFFF

# Parameter initialization  
overflowInTurn = 0 
syncTimetag = 0 
OVF_PERIOD = 33554432

# Histogram array initialization
DATA_FREQ  = 11245 # Hz (LHC Clock)
HIST_TIME = 1 / DATA_FREQ
TDC_RES = 250e-12
hist_size = HIST_TIME/TDC_RES

histogram = [0] * int(hist_size)

if os.name == "nt":
    th260lib = ct.WinDLL("th260lib64.dll")
else:
    th260lib = ct.CDLL(LIBRARY_PATH + "library/th260lib.so")

def closeDevices():
    for i in range(0, MAXDEVNUM):
        th260lib.TH260_CloseDevice(ct.c_int(i))
    #exit(0) # uncomment this to stop the script when closeDevices is called

def stoptttr():
    tryfunc(th260lib.TH260_StopMeas(ct.c_int(dev[0])), "StopMeas")
    closeDevices()

def tryfunc(retcode, funcName, measRunning=False):
    if retcode < 0:
        th260lib.TH260_GetErrorString(errorString, ct.c_int(retcode))
        print("TH260_%s error %d (%s). Aborted." % (funcName, retcode,\
                errorString.value.decode("utf-8")))
        if measRunning:
            stoptttr()
        else:
            closeDevices()
            
def main(output_file, group_key,  acquisition_time): 
    
    # Measurement parameters
    mode = MODE_T2 # set T2 or T3 here, observe suitable Syncdivider and Range!
    binning = 0 # meaningful only in T3 mode
    offset = 0 # in psec.Meaningful only in T3 mode
    syncDivider = 1 

    ### For TimeHarp 260 N
    syncTriggerEdge = 0 # you can change this
    syncTriggerLevel = -100 # you can change this
    inputTriggerEdge = 0 # you can change this
    inputTriggerLevel = -25 # you can change this
    
    tacq = int(acquisition_time) # Measurement time in millisec

    # Get library version and available TimeHarp devices
    th260lib.TH260_GetLibraryVersion(libVersion)
    print("Library version is %s" % libVersion.value.decode("utf-8"))
    if libVersion.value.decode("utf-8") != LIB_VERSION:
        print("Warning: The application was built for version %s" % LIB_VERSION)

    print("\nSearching for TimeHarp devices...")
    print("Devidx     Status")

    for i in range(0, MAXDEVNUM):
        retcode = th260lib.TH260_OpenDevice(ct.c_int(i), hwSerial)
        if retcode == 0:
            print("  %1d        S/N %s" % (i, hwSerial.value.decode("utf-8")))
            dev.append(i)
        else:
            if retcode == -1: # TH260_ERROR_DEVICE_OPEN_FAIL
                print("  %1d        no device" % i)
            else:
                th260lib.TH260_GetErrorString(errorString, ct.c_int(retcode))
                print("  %1d        %s" % (i, errorString.value.decode("utf8")))

    # Use the first TimeHarp device found 
    if len(dev) < 1:
        print("No device available.")
        closeDevices()
    print("Using device #%1d" % dev[0])
    print("\nInitializing the device...")

    # Using internal clock
    devidx = ct.c_int(dev[0])
    mode = ct.c_int(mode)
    
    # Initialize the device 
    tryfunc(th260lib.TH260_Initialize(devidx.value, mode.value), "Initialize")

    # Get Hardware Info: model, part number and version
    tryfunc(th260lib.TH260_GetHardwareInfo(dev[0], hwModel, hwPartno, hwVersion),\
            "GetHardwareInfo")
    print("Found Model %s Part no %s Version %s" % (hwModel.value.decode("utf-8"),\
        hwPartno.value.decode("utf-8"), hwVersion.value.decode("utf-8")))

    # Get number of input channels
    tryfunc(th260lib.TH260_GetNumOfInputChannels(ct.c_int(dev[0]), byref(numChannels)),\
            "GetNumOfInputChannels")
    print("Device has %i input channels." % numChannels.value)
    print("Mode              : %d" % mode.value)
    
    # Configure the input channels: sync divider, CFD, binning, resolution etc. 
    if hwModel.value.decode("utf-8") == "TimeHarp 260 P":
        print("SyncCFDZeroCross  : %d" % syncCFDZeroCross)
        print("SyncCFDLevel      : %d" % syncCFDLevel)
        print("InputCFDZeroCross : %d" % inputCFDZeroCross)
        print("InputCFDLevel     : %d" % inputCFDLevel)
    elif hwModel.value.decode("utf-8") == "TimeHarp 260 N":
        print("SyncTriggerEdge   : %d" % syncTriggerEdge)
        print("SyncTriggerLevel  : %d" % syncTriggerLevel)
        print("InputTriggerEdge  : %d" % inputTriggerEdge)
        print("InputTriggerLevel : %d" % inputTriggerLevel)
    else:
        print("Unknown hardware model %s. Aborted." % hwModel.value.decode("utf-8"))
        closeDevices()    
   
    tryfunc(th260lib.TH260_SetSyncDiv(ct.c_int(dev[0]), ct.c_int(syncDivider)),
	            "SetSyncDiv")

    if hwModel.value.decode("utf-8") == "TimeHarp 260 P":
        tryfunc(
            th260lib.TH260_SetSyncCFD(ct.c_int(dev[0]), ct.c_int(syncCFDLevel),\
                                        ct.c_int(syncCFDZeroCross)),\
            "SetSyncCFD"
        )
        # we use the same input settings for all channels, you can change this
        for i in range(0, numChannels.value):
            tryfunc(
                th260lib.TH260_SetInputCFD(ct.c_int(dev[0]), ct.c_int(i),\
                                        ct.c_int(inputCFDLevel),\
                                        ct.c_int(inputCFDZeroCross)),\
                "SetInputCFD"
            )
    if hwModel.value.decode("utf-8") == "TimeHarp 260 N":
        tryfunc(
             th260lib.TH260_SetSyncEdgeTrg(ct.c_int(dev[0]), ct.c_int(syncTriggerLevel),\
                                          ct.c_int(syncTriggerEdge)),\
             "SetSyncEdgeTrg"
            )
           # we use the same input settings for all channels, you can change this
        for i in range(0, numChannels.value):
                retcode = th260lib.TH260_SetInputEdgeTrg(ct.c_int(dev[0]), ct.c_int(i),\
                                                 ct.c_int(inputTriggerLevel),\
                                                 ct.c_int(inputTriggerEdge))
                if retcode < 0:
                     print("TH260_SetInputCFD error %d. Aborted." % retcode)
                     closeDevices()

    tryfunc(th260lib.TH260_SetSyncChannelOffset(ct.c_int(dev[0]), ct.c_int(0)),\
            "SetSyncChannelOffset")


    for i in range(0, numChannels.value):
        tryfunc(
            th260lib.TH260_SetInputChannelOffset(ct.c_int(dev[0]), ct.c_int(i),\
                                                ct.c_int(0)),\
            "SetInputChannelOffset"
        )

    tryfunc(th260lib.TH260_SetBinning(ct.c_int(dev[0]), ct.c_int(binning)), "SetBinning")
    tryfunc(th260lib.TH260_SetOffset(ct.c_int(dev[0]), ct.c_int(offset)), "SetOffset")
    tryfunc(th260lib.TH260_GetResolution(ct.c_int(dev[0]), byref(resolution)),\
            "GetResolution")
    print("Resolution is %1.1lfps" % resolution.value)

    print("\nMeasuring input rates...")

    # After Init or SetSyncDiv allow 150 ms for valid count rate readings
    time.sleep(0.15)

    tryfunc(th260lib.TH260_GetSyncRate(ct.c_int(dev[0]), byref(syncRate)),
            "GetSyncRate")
    print("\nSyncrate=%1d/s" % syncRate.value)

    for i in range(0, numChannels.value):
        tryfunc(
            th260lib.TH260_GetCountRate(ct.c_int(dev[0]), ct.c_int(i), byref(countRate)),\
            "GetCountRate"
        )
        print("Countrate[%1d]=%1d/s" % (i, countRate.value))

    # After getting the count rates, check for warnings
    tryfunc(th260lib.TH260_GetWarnings(ct.c_int(dev[0]), byref(warnings)), "GetWarnings")
    if warnings.value != 0:
        th260lib.TH260_GetWarningsText(ct.c_int(dev[0]), warningstext, warnings)
        print("\n\n%s" % warningstext.value.decode("utf-8"))

    print("Starting data collection...")       

    progress = 0
    sys.stdout.write("\nProgress:%12u" % progress)
    sys.stdout.flush()

    tryfunc(th260lib.TH260_StartMeas(ct.c_int(dev[0]), ct.c_int(tacq)), "StartMeas")

    # Parameter initialization for word decoding 
    latestSync = -1
    overflowInTurn = 0 
    numOfCounts_per_trigger, numOfCounts_per_second, last_time_update = 0, 0, 0 
    hit_rate_per_trigger , hit_rate_per_second = [], [] 
    
    # Initialize time 
    last_time_update = time.time()
    while True:
        tryfunc(th260lib.TH260_GetFlags(ct.c_int(dev[0]), byref(flags)), "GetFlags")
        
        if flags.value & FLAG_FIFOFULL > 0:
            print("\nFiFo Overrun!")
            stoptttr()
        
        tryfunc(
            # nRecords returns the number of TTTR records received 
            # byref(buffer) is a pointer to the buffer where the TTTR data will be stored 
            th260lib.TH260_ReadFiFo(ct.c_int(dev[0]), byref(buffer), TTREADMAX,byref(nRecords)),"ReadFiFo", measRunning=True
        )

        if nRecords.value > 0:
            for word in buffer[0:nRecords.value]: 
                # Check for sync pulse:
                if( (word & syncMask) == syncEventMasked ):  
                    latestSync = (word & timeTagMask)
                    overflowInTurn  = 0 
                    # Update hit rate per trigger 
                    hit_rate_per_trigger.append(numOfCounts_per_trigger)
                    numOfCounts_per_trigger = 0   
                
                # Check for overflow
                elif( (word & syncMask) == overflowEventMasked ):
                    # Add OVF_PERIOD to time counts until next sync
                    overflowInTurn += 1

                # If special bit is clear (0), then it's a hit
                elif (word & specialBitMask == 0) and latestSync > 0: 
                    
                    timebin = ( (word & timeTagMask) + overflowInTurn*OVF_PERIOD) - latestSync 
                    
                    # Add hit to the full histogram 
                    if timebin > 0 and timebin < hist_size: 
                        histogram[timebin] += 1 
                    
                    numOfCounts_per_trigger += 1 
                    numOfCounts_per_second  += 1
                 
                # Update hit rate if applicable
                current_time = time.time()    
                if current_time - last_time_update >=1: 
	                hit_rate_per_second.append(numOfCounts_per_second)
	                numOfCounts_per_second = 0  
	                last_time_update = current_time
    
            progress += nRecords.value
            sys.stdout.write("\rProgress:%12u" % progress)
            sys.stdout.flush()
        else:

            tryfunc(th260lib.TH260_CTCStatus(ct.c_int(dev[0]), byref(ctcstatus)),\
                    "CTCStatus")
            if ctcstatus.value > 0: 
                stoptttr()
                break

    closeDevices()
    
    # Specific window of histogram we want
    start_index = 10
    end_index   = 20 
    segmented_histogram = histogram[start_index:end_index]

    data_lists = {
    'full_histogram': histogram,
    'segmented_histogram': segmented_histogram,
    'hit_rate_per_second': hit_rate_per_second,
    'hit_rate_per_trigger': hit_rate_per_trigger
    }
    
    start = time.time()
    with h5py.File(output_file, 'w') as file:
        datetime_group = file.create_group(group_key)
            
        # Store the sub-lists under the timestamp group
        for name, data_list in data_lists.items():
            # Pack into binary format 
            binary_sub_list = struct.pack(f'{len(data_list)}I', *data_list)
            
            # binary_sub_list_array points to the same memory buffer as the data from binary_sub_list
            binary_sub_list_array = np.frombuffer(binary_sub_list, dtype=np.uint32)
            
            datetime_group.create_dataset(name, data=binary_sub_list_array)

        print(f"\nTime to save binary array data in h5 file: {time.time()-start} secs.") 
    print(f"Saving binary data as an array (no compression) in .h5: {os.path.getsize(output_file)} bytes.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process files.')
    parser.add_argument('--output_file', help='Filename of .h5 to save the data: Full aged histogram, segmented histogram, per trigger and per second count rate.')
    parser.add_argument('--group_key', help='Timestamp of the beginning of the acquisition. Group name for stored data in .h5 file.')
    parser.add_argument('--acquisition_time', type=float, help='Measurement acquisition time in milliseconds.')
    args = parser.parse_args()
    main(args.output_file, args.group_key, args.acquisition_time)
