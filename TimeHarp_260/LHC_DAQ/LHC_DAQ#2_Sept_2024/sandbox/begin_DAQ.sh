#!/bin/bash 

# Capture the datetime from the FESA Server
#python start_fesa_subscription.py & 
#sleep 15

# Prompt user for overwrite option
read -p "Do you want to overwrite previous datetime in crontab? (yes/no) - Select no for the first time. " do_overwrite 

while true; do
    # Capture the datetime from the FESA Server
    datetime=$(cat daq_start_timetag.txt)

    echo "The datetime obtained is: $datetime"
    read -p "Is this OK to proceed? (yes/no): " answer
    
    if [ "$answer" = "yes" ]; then      
        if [ "$do_overwrite" = "no" ]; then
            # Insert datetime at the beginning of the crontab
            sudo sed -i "1i\\$datetime" /etc/crontab
            echo "Datetime inserted into /etc/crontab successfully. Do -cat /etc/crontab- for verification. "
        else
            # Overwrite  
           sudo sed -i '1,4d' /etc/crontab
           sudo sed -i "1i\\$datetime" /etc/crontab
           echo "Datetime inserted into /etc/crontab successfully. Do -cat /etc/crontab- for verification."
        fi 
        break 
    else
        echo "Rerunning to obtain a new datetime..."
    fi
done
