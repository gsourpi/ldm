#!/bin/bash

# Define the directory and logfile
DIRECTORY="/afs/cern.ch/user/g/gsourpi/ldm/TimeHarp_260/LHC_DAQ/LHC_DAQ#2_Sept_2024"
LOGFILE="${DIRECTORY}/loglog.log"

# Command you want to execute
echo "!Running on shell: $BASH $(date)"
