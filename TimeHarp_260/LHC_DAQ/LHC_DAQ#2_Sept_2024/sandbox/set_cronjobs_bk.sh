# Source the variables from the text file
source daq_start_timetag.txt

# Print the variables to verify
echo "HOUR: $HOUR"
echo "MINUTE: $MINUTE"
echo "SECOND: $SECOND"

# Now you can use these variables in your script
DIRECTORY="/afs/cern.ch/user/g/gsourpi/ldm/TimeHarp_260/LHC_DAQ/LHC_DAQ#2_Sept_2024"

# Generate crontab commands
{
  # Command to initialize the device 1 minute before the first data acquisition
  INIT_MINUTE=$((MINUTE - 1))
  echo "$INIT_MINUTE $HOUR * * * root $DIRECTORY/initialize_device.sh >> $DIRECTORY/log.log 2>&1"

  # Command to begin data acquisition which will continue forever with 5 minute intervals
  echo "$MINUTE $HOUR * * * root $DIRECTORY/data_acquisition.sh \"sleep $SECOND\""

  # Command to copy and compress the temporary 5 minute histogram to the general file of BSRL Data
  COPY_MINUTES=$(seq -s "," $((MINUTE + 6)) 6 54)
  echo "$COPY_MINUTES $HOUR * * * root $DIRECTORY/copy_key.py --source_file $DIRECTORY/BSRL_5_min_histogram.h5 --target_file $DIRECTORY/BSRL_data.h5 >> $DIRECTORY/log.log 2>&1"

  # Command to create a backup of the data and send it to the newbsrl user
  echo "0 1 * * * root cp $DIRECTORY/BSRL_data.h5 $DIRECTORY/BSRL_data_bak.h5 && scp -r $DIRECTORY/BSRL_data.h5 newbsrl@pcbe15348:/BSRL/"
} | sudo tee -a /etc/crontab > /dev/null

echo "Update crontab file successfully."

