import pyjapc
from datetime import * 
import queue
import os 

# Initialize a pyjapc instance
fesa = pyjapc.PyJapc()

# Initialize the value of the updated parameter 
latest_param = -1

# Initialize a FIFO queue 
output_queue = queue.Queue()

def subscribed(param, val):
    global latest_param
    latest_param = param
    next_daq = datetime.now() + timedelta(seconds=300.0 - val)
    # Uncomment for debugging 
    #output_queue.put(f"Histogram age (from 0 to 300) in seconds: {val}")
    #output_queue.put(f"Next data acquisiton in: {300.0 - val} seconds")
    #output_queue.put(f"Next data acquisiton at: {next_daq}")
    #output_queue.put("At the /etc/crontab : ")
    output_queue.put(f"\nHOUR={next_daq.strftime('%H')} \nMINUTE={next_daq.strftime('%M')} \nSECOND={next_daq.strftime('%S')} \n")

    os.system("clear")
    while not output_queue.empty():
            print(output_queue.get())

fesa.rbacLogin('gsourpi')
fesa.setSelector("")
var = "LHC.BSRL.5R4.B1/Acquisition#acqGHistogramAge"
param = fesa.getParam(var)

fesa.subscribeParam(var, subscribed)
fesa.startSubscriptions()

try: 
    while True: 
                pass 
except KeyboardInterrupt: 
    fesa.stopSubscriptions()
