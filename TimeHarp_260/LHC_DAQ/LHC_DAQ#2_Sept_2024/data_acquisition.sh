#!/bin/bash

# Define a log file path
DIRECTORY=/afs/cern.ch/user/g/gsourpi/ldm/TimeHarp_260/LHC_DAQ/LHC_DAQ#2_Sept_2024
#LOG_FILE=$DIRECTORY/data_acquisition.log
OUTPUT_FILE=$DIRECTORY/BSRL_5_min_hist.h5

# Redirect all output (stdout and stderr) to the log file
# exec &>> $LOG_FILE

echo "Script started at $(date)" 
echo ""
cd $DIRECTORY
while true; do 
    echo "Data collection starting..."
    start_of_data_acq=$(date +"%Y-%m-%d %H:%M:%S")
    acquire_data="python $DIRECTORY/data_acquisition.py --output_file \"$OUTPUT_FILE\" --dataset_key \"$start_of_data_acq\" --acquisition_time 300"
    
    # Run the 5 minute acquisition command; if it fails, reinitialize the device and retry acquisition 
    eval $acquire_data || { bash $DIRECTORY/initialize_device.sh && eval $acquire_data; }
    echo ""
    echo "Data collection finished."
done 
