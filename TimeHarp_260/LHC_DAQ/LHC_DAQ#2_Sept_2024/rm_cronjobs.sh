#!/bin/bash

# Print the current contents of /etc/crontab
echo "Here is the content of /etc/crontab:"
sudo cat /etc/crontab

# Ask the user for confirmation to remove the last 5 lines
read -p "Do you want to remove the last 5 lines of /etc/crontab? (y/n): " choice

# If user confirms, proceed with removing the last 5 lines
if [[ "$choice" == "y" || "$choice" == "Y" ]]; then
    echo "Removing the last 4 lines..."

    # Backup the original file
    sudo cp /etc/crontab /etc/crontab.bak
    echo "Backup created as /etc/crontab.bak"

    # Remove the last 5 lines and save to a temporary file
    sudo head -n -4 /etc/crontab > /tmp/crontab_tmp

    # Replace the original file with the new one
    sudo mv /tmp/crontab_tmp /etc/crontab

    # Set correct ownership and permissions for /etc/crontab
    sudo chown root:root /etc/crontab
    sudo chmod 644 /etc/crontab

    # Print the updated file to verify the changes
    echo "Here is the updated /etc/crontab:"
    sudo tail /etc/crontab

else
    echo "Operation canceled. No changes made."
fi
