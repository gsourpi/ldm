import ctypes as ct
from ctypes import byref
import os
import sys
import time
import struct
from matplotlib import pyplot as plt
import numpy as np
import struct
import h5py

import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np

import argparse
import threading
import queue 
import datetime

if sys.version_info[0] < 3:
    print("[Warning] Python 2 is not fully supported. It might work, but "
            "use Python 3 if you encounter errors.\n")
    raw_input("press RETURN to continue"); print
    input = raw_input

LIBRARY_PATH = "/afs/cern.ch/user/g/gsourpi/ldm/TimeHarp_260/"

# From th260defin.h
LIB_VERSION = "3.2"
MAXDEVNUM = 4
MODE_T2 = 2
MODE_T3 = 3
MAXLENCODE = 5
MAXINPCHAN = 2
TTREADMAX = 131072
FLAG_OVERFLOW = 0x0001
FLAG_FIFOFULL = 0x0002

# Variables to store information read from DLLs
buffer = (ct.c_uint * TTREADMAX)()
dev = []
libVersion = ct.create_string_buffer(b"", 8)
hwSerial = ct.create_string_buffer(b"", 8)
hwPartno = ct.create_string_buffer(b"", 8)
hwVersion = ct.create_string_buffer(b"", 16)
hwModel = ct.create_string_buffer(b"", 16)
errorString = ct.create_string_buffer(b"", 40)
numChannels = ct.c_int()
resolution = ct.c_double()
syncRate = ct.c_int()
countRate = ct.c_int()
flags = ct.c_int()
nRecords = ct.c_int()
ctcstatus = ct.c_int()
warnings = ct.c_int()
warningstext = ct.create_string_buffer(b"", 16384)

# Initialization for decoding of the TimeHarp TDC 
# Bit masks initialization 
specialBitMask = 0x80000000
channelMask = 0x7E000000
syncMask = 0xFE000000
syncEventMasked = 0x80000000
overflowEventMasked = 0xFE000000
timeTagMask = 0x1FFFFFF

# Parameter initialization  
overflowInTurn = 0 
syncTimetag = 0 
OVF_PERIOD = 33554432

# Histogram array initialization
DATA_FREQ  = 11245 # Hz (LHC Clock)
HIST_TIME = 1 / DATA_FREQ
TDC_RES = 250e-12
hist_size = HIST_TIME/TDC_RES

if os.name == "nt":
    th260lib = ct.WinDLL("th260lib64.dll")
else:
    th260lib = ct.CDLL(LIBRARY_PATH + "library/th260lib.so")

def closeDevices():
    for i in range(0, MAXDEVNUM):
        th260lib.TH260_CloseDevice(ct.c_int(i))
    #exit(0) # uncomment this to stop the script when closeDevices is called

def stoptttr():
    tryfunc(th260lib.TH260_StopMeas(ct.c_int(dev[0])), "StopMeas")
    closeDevices()

def tryfunc(retcode, funcName, measRunning=False):
    if retcode < 0:
        th260lib.TH260_GetErrorString(errorString, ct.c_int(retcode))
        print("TH260_%s error %d (%s). Aborted." % (funcName, retcode,\
                errorString.value.decode("utf-8")))
        if measRunning:
            stoptttr()
        else:
            closeDevices()

end_of_acq_time = False
count_of_5min_acq = 0  #count of 5 min histograms that have been saved so far 
new_hist  = np.zeros(int(hist_size), dtype=int)
prev_hist = np.zeros(int(hist_size), dtype=int)
latestSync = -1
overflowInTurn = 0 

def main(output_file, acq_time):
    global count_of_5min_acq, end_of_acq_time, new_hist, prev_hist, latestSync, overflowInTurn 
    start_of_script = time.time() # from 0 up to acquisition_time 
    # tacq = int(acquisition_time*24*60*60*1e3) # Measurement time in milliseconds - acquisition_time in days 
    tacq = int(acq_time*60*1e3) # Measurement time in milliseconds - acquisition_time in minutes 
    
    # Measurement parameters
    mode = MODE_T2 # set T2 or T3 here, observe suitable Syncdivider and Range!
    binning = 0 # meaningful only in T3 mode
    offset = 0 # in psec.Meaningful only in T3 mode
    syncDivider = 1 

    # For TimeHarp 260 P
    syncCFDZeroCross = -5 # in mV
    syncCFDLevel = -5 # in mV
    inputCFDZeroCross = -5 # in mV
    inputCFDLevel = -5 # in mV

    ### For TimeHarp 260 N
    syncTriggerEdge = 0 # in mV
    syncTriggerLevel = -100 #  in mV
    inputTriggerEdge = 0 #  in mV
    inputTriggerLevel = -25 #  in mV

    def save_data(histogram ,start_of_daq, output_file):
        global count_of_5min_acq
        count_of_5min_acq +=1 
        start_saving = time.time()
        with h5py.File(output_file, 'a') as file:
            binhist = struct.pack(f'{len(histogram)}I', *histogram)
            binhist_array = np.frombuffer(binhist, dtype=np.uint32)
            file.create_dataset(start_of_daq, data = binhist_array, compression='gzip', compression_opts=4) 
            print(f"\nTime to save and compress binary array data in h5 file: {time.time()-start_saving} secs.") 
        print(f"Saving binary data as an array (with compression) in .h5: {os.path.getsize(output_file)} bytes.")

    # Get library version and available TimeHarp devices
    th260lib.TH260_GetLibraryVersion(libVersion)
    print("Library version is %s" % libVersion.value.decode("utf-8"))
    if libVersion.value.decode("utf-8") != LIB_VERSION:
        print("Warning: The application was built for version %s" % LIB_VERSION)

    print("\nSearching for TimeHarp devices...")
    print("Devidx     Status")

    for i in range(0, MAXDEVNUM):
        retcode = th260lib.TH260_OpenDevice(ct.c_int(i), hwSerial)
        if retcode == 0:
            print("  %1d        S/N %s" % (i, hwSerial.value.decode("utf-8")))
            dev.append(i)
        else:
            if retcode == -1: # TH260_ERROR_DEVICE_OPEN_FAIL
                print("  %1d        no device" % i)
            else:
                th260lib.TH260_GetErrorString(errorString, ct.c_int(retcode))
                print("  %1d        %s" % (i, errorString.value.decode("utf8")))

    # Use the first TimeHarp device found 
    if len(dev) < 1:
        print("No device available.")
        closeDevices()
    print("Using device #%1d" % dev[0])
    print("\nInitializing the device...")

    # Using internal clock
    devidx = ct.c_int(dev[0])
    mode = ct.c_int(mode)
    
    # Initialize the device 
    tryfunc(th260lib.TH260_Initialize(devidx.value, mode.value), "Initialize")

    # Get Hardware Info: model, part number and version
    tryfunc(th260lib.TH260_GetHardwareInfo(dev[0], hwModel, hwPartno, hwVersion),\
            "GetHardwareInfo")
    print("Found Model %s Part no %s Version %s" % (hwModel.value.decode("utf-8"),\
        hwPartno.value.decode("utf-8"), hwVersion.value.decode("utf-8")))

    # Get number of input channels
    tryfunc(th260lib.TH260_GetNumOfInputChannels(ct.c_int(dev[0]), byref(numChannels)),\
            "GetNumOfInputChannels")
    print("Device has %i input channels." % numChannels.value)
    print("Mode              : %d" % mode.value)
    
    # Configure the input channels: sync divider, CFD, binning, resolution etc. 
    if hwModel.value.decode("utf-8") == "TimeHarp 260 P":
        print("SyncCFDZeroCross  : %d" % syncCFDZeroCross)
        print("SyncCFDLevel      : %d" % syncCFDLevel)
        print("InputCFDZeroCross : %d" % inputCFDZeroCross)
        print("InputCFDLevel     : %d" % inputCFDLevel)
    elif hwModel.value.decode("utf-8") == "TimeHarp 260 N":
        print("SyncTriggerEdge   : %d" % syncTriggerEdge)
        print("SyncTriggerLevel  : %d" % syncTriggerLevel)
        print("InputTriggerEdge  : %d" % inputTriggerEdge)
        print("InputTriggerLevel : %d" % inputTriggerLevel)
    else:
        print("Unknown hardware model %s. Aborted." % hwModel.value.decode("utf-8"))
        closeDevices()    

    tryfunc(th260lib.TH260_SetSyncDiv(ct.c_int(dev[0]), ct.c_int(syncDivider)),
                "SetSyncDiv")

    if hwModel.value.decode("utf-8") == "TimeHarp 260 P":
        tryfunc(
            th260lib.TH260_SetSyncCFD(ct.c_int(dev[0]), ct.c_int(syncCFDLevel),\
                                        ct.c_int(syncCFDZeroCross)),\
            "SetSyncCFD"
        )
        # we use the same input settings for all channels, you can change this
        for i in range(0, numChannels.value):
            tryfunc(
                th260lib.TH260_SetInputCFD(ct.c_int(dev[0]), ct.c_int(i),\
                                        ct.c_int(inputCFDLevel),\
                                        ct.c_int(inputCFDZeroCross)),\
                "SetInputCFD"
            )
    if hwModel.value.decode("utf-8") == "TimeHarp 260 N":
        tryfunc(
            th260lib.TH260_SetSyncEdgeTrg(ct.c_int(dev[0]), ct.c_int(syncTriggerLevel),\
                                        ct.c_int(syncTriggerEdge)),\
            "SetSyncEdgeTrg"
            )
        # we use the same input settings for all channels, you can change this
        for i in range(0, numChannels.value):
                retcode = th260lib.TH260_SetInputEdgeTrg(ct.c_int(dev[0]), ct.c_int(i),\
                                                ct.c_int(inputTriggerLevel),\
                                                ct.c_int(inputTriggerEdge))
                if retcode < 0:
                    print("TH260_SetInputCFD error %d. Aborted." % retcode)
                    closeDevices()

    tryfunc(th260lib.TH260_SetSyncChannelOffset(ct.c_int(dev[0]), ct.c_int(0)),\
            "SetSyncChannelOffset")


    for i in range(0, numChannels.value):
        tryfunc(
            th260lib.TH260_SetInputChannelOffset(ct.c_int(dev[0]), ct.c_int(i),\
                                                ct.c_int(0)),\
            "SetInputChannelOffset"
        )

    tryfunc(th260lib.TH260_SetBinning(ct.c_int(dev[0]), ct.c_int(binning)), "SetBinning")
    tryfunc(th260lib.TH260_SetOffset(ct.c_int(dev[0]), ct.c_int(offset)), "SetOffset")
    tryfunc(th260lib.TH260_GetResolution(ct.c_int(dev[0]), byref(resolution)),\
            "GetResolution")
    print("Resolution is %1.1lfps" % resolution.value)

    print("\nMeasuring input rates...")

    #After Init or SetSyncDiv allow 150 ms for valid count rate readings
    time.sleep(0.15)

    tryfunc(th260lib.TH260_GetSyncRate(ct.c_int(dev[0]), byref(syncRate)),
            "GetSyncRate")
    print("\nSyncrate=%1d/s" % syncRate.value)

    for i in range(0, numChannels.value):
        tryfunc(
            th260lib.TH260_GetCountRate(ct.c_int(dev[0]), ct.c_int(i), byref(countRate)),\
            "GetCountRate"
        )
        print("Countrate[%1d]=%1d/s" % (i, countRate.value))

    # After getting the count rates, check for warnings
    tryfunc(th260lib.TH260_GetWarnings(ct.c_int(dev[0]), byref(warnings)), "GetWarnings")
    if warnings.value != 0:
        th260lib.TH260_GetWarningsText(ct.c_int(dev[0]), warningstext, warnings)
        print("\n\n%s" % warningstext.value.decode("utf-8"))
        
    data_queue = queue.Queue()
    data_ready = threading.Event()
    
    
    def data_acq():
        global new_hist, prev_hist, end_of_acq_time, latestSync, overflowInTurn
        print("Starting data collection...") 
        progress = 0
        sys.stdout.write("\nProgress:%12u" % progress)
        sys.stdout.flush()
        tryfunc(th260lib.TH260_StartMeas(ct.c_int(dev[0]), ct.c_int(tacq)), "StartMeas")

        while not end_of_acq_time:
            start_of_daq = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            while time.time() - start_of_script - count_of_5min_acq*float(5*60) <  float(5*60): # Since we want to obtain 5 minute histograms 
                #print('Read buffer - Time in seconds:', time.time() - start_of_script - count_of_5min_acq*float(1*60))
                tryfunc(th260lib.TH260_GetFlags(ct.c_int(dev[0]), byref(flags)), "GetFlags")
                
                if flags.value & FLAG_FIFOFULL > 0:
                    print("\nFiFo Overrun!")
                    stoptttr()
                
                tryfunc(
                    # nRecords returns the number of TTTR records received 
                    # byref(buffer) is a pointer to the buffer where the TTTR data will be stored 
                    th260lib.TH260_ReadFiFo(ct.c_int(dev[0]), byref(buffer), TTREADMAX,byref(nRecords)),"ReadFiFo", measRunning=True
                )

                if nRecords.value > 0:
                    for word in buffer[0:nRecords.value]: 
                        # Check for sync pulse:
                        if( (word & syncMask) == syncEventMasked ):  
                            latestSync = (word & timeTagMask)
                            overflowInTurn  = 0 
                        
                        # Check for overflow
                        elif( (word & syncMask) == overflowEventMasked ):
                            # Add OVF_PERIOD to time counts until next sync
                            overflowInTurn += 1

                        # If special bit is clear (0), then it's a hit
                        elif (word & specialBitMask == 0) and latestSync > 0: 
                            
                            timebin = ( (word & timeTagMask) + overflowInTurn*OVF_PERIOD) - latestSync 
                            
                            # Add hit to the full histogram 
                            if timebin > 0 and timebin < hist_size: 
                                new_hist[timebin] += 1 
                            
                    progress += nRecords.value
                    sys.stdout.write("\rProgress:%12u" % progress)
                    sys.stdout.flush()
                else:
                    tryfunc(th260lib.TH260_CTCStatus(ct.c_int(dev[0]), byref(ctcstatus)),\
                            "CTCStatus")
                    if ctcstatus.value > 0: #  if the acquisition time has expired.
                        stoptttr()
                        end_of_acq_time = True                    
    
            else: 
                prev_hist[:] = new_hist
                data_queue.put([prev_hist, start_of_daq])
                data_ready.set() # Signal the saving thread to begin 
                # Empty the histogram that has been built so far. 
                new_hist.fill(0)
                
        closeDevices()
        acquisition_thread.join()
        data_queue.put(None)  # Signal the saving thread to exit
        saving_thread.join()
        
    def save_data_thread():
        while True: 
            data_ready.wait() # Wait for the signal from the acquisition thread
            data_item = data_queue.get()
            if data_item is None:
                data_queue.task_done()
                break  # Exit the loop, stopping the thread
            hist, start_of_daq = data_item
            save_data(histogram = hist, start_of_daq = start_of_daq, output_file=output_file)
            data_queue.task_done()
            data_ready.clear()  # Reset the event after handling the data
            
    # Main code that runs the threads 
    acquisition_thread = threading.Thread(target=data_acq)
    saving_thread = threading.Thread(target=save_data_thread)

    acquisition_thread.start()
    saving_thread.start()
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process files.', 
                                    epilog="Example of use: python data_acquisition.py --output_file 'test.h5' --acquisition_time 50")
    parser.add_argument('--output_file', help='Filename of .h5 to save the data: Full aged histogram, segmented histogram, per trigger and per second count rate.')
    parser.add_argument('--acquisition_time', type=float, help='Measurement acquisition time in minutes.')
    args = parser.parse_args()
    main(args.output_file, args.acquisition_time)
