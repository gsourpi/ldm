#!/bin/bash

log_dir=/afs/cern.ch/user/g/gsourpi/ldm/TimeHarp_260/LHC_DAQ/LHC_DAQ#2_Sept_2024/logs

rm $log_dir/general.logs
rm $log_dir/data_acq.logs

touch $log_dir/general.logs
touch $log_dir/data_acq.logs

chmod 666 $log_dir/general.logs
chmod 666 $log_dir/data_acq.logs
