# Source the variables from the text file
source ./daq_start_timetag.txt
USER="gsourpi"

# Print the variables to verify
echo "HOUR: $HOUR"
echo "MINUTE: $MINUTE"
echo "SECOND: $SECOND"

# Now you can use these variables in your script
DIRECTORY="/afs/cern.ch/user/g/gsourpi/ldm/TimeHarp_260/LHC_DAQ/LHC_DAQ#2_Sept_2024"

# Generate crontab commands
{
  # Command to initialize the device 1 minute before the first data acquisition
  INIT_MINUTE=$((MINUTE - 1))
  echo "$INIT_MINUTE $HOUR * * * $USER $DIRECTORY/initialize_device.sh >> $DIRECTORY/logs/general.logs 2>&1"

  # Command to begin data acquisition every 5 minutes
  echo "$MINUTE $HOUR * * * $USER $DIRECTORY/data_acquisition.sh \"sleep $SECOND\" >> $DIRECTORY/logs/data_acq.logs 2>&1"

  # Command to copy and compress 5-minute histogram data
  COPY_MINUTES=$(seq -s "," $((MINUTE + 6)) 6 54)
  echo "$COPY_MINUTES $HOUR * * * $USER $DIRECTORY/copy_and_compress_key.py --source_file $DIRECTORY/BSRL_5_min_hist.h5 --target_file $DIRECTORY/BSRL_data.h5 >> $DIRECTORY/logs/general.logs 2>&1"

  # Command to back up data and transfer it
  echo "0 1 * * * $USER (cp $DIRECTORY/BSRL_data.h5 $DIRECTORY/BSRL_data_bak.h5 && scp -r $DIRECTORY/BSRL_data.h5 newbsrl@pcbe15348:/BSRL/) >> $DIRECTORY/logs/general.logs 2>&1"
} | sudo tee -a /etc/crontab > /dev/null

bash $DIRECTORY/logs/empty_logs.sh
echo "Emptied log files successfully."
# sudo crontab my_crontab
echo "Crontab for user gsourpi updated successfully."

