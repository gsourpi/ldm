import pyjapc
from datetime import * 
import queue
import os 

# Initialize a pyjapc instance
fesa = pyjapc.PyJapc()

# Initialize the value of the updated parameter 
latest_param = -1

def subscribed(param, val):
    global latest_param
    latest_param = param
    next_daq = datetime.now() + timedelta(seconds=300.0 - val)
    timetag = f"HOUR={next_daq.strftime('%H')}\nMINUTE={next_daq.strftime('%M')}\nSECOND={next_daq.strftime('%S')}"
    with open("daq_start_timetag.txt", "w") as file:
        file.write(timetag)
        
fesa.rbacLogin('gsourpi')
fesa.setSelector("")
var = "LHC.BSRL.5R4.B1/Acquisition#acqGHistogramAge"
param = fesa.getParam(var)

fesa.subscribeParam(var, subscribed)
fesa.startSubscriptions()

try: 
    while True: 
                pass 
except KeyboardInterrupt: 
    fesa.stopSubscriptions()
