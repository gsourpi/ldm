#!/bin/bash

DIRECTORY="/afs/cern.ch/user/g/gsourpi/ldm/TimeHarp_260/driver"
cd $DIRECTORY

if lsmod | grep -q th260pcie; then
    echo "Kernel module th260pcie is loaded. Continuing..."
else
    echo "Kernel module th260pcie is not loaded. Loading..."
    insmod th260pcie.ko
    
    if [ $? -ne 0 ]; then
    echo "Error: Failed to load kernel module th260pcie."
    exit 1
    else
    echo "Kernel module th260pcie loaded successfully."
    fi
fi

if dmesg | grep -i 'th260pcie' > /dev/null && lspci | grep -i 'Xilinx'; then
    echo " " 
    echo "The hardware device associated with 'Xilinx' and 'th260pcie' has been detected by the system." 
else 
    lspci | grep -i 'Xilinx'
    dmesg | grep -i 'th260pcie'
fi
