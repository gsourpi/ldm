import h5py
import os 
import time

def copy_key(source, target):
    start_time = time.time()

    with h5py.File(source, 'r') as source_h5:
        source_key = list(source_h5.keys())[0] 
        data_to_copy = source_h5[source_key][:]
 
    with h5py.File(target, 'a') as target_h5:
        if source_key in target_h5:
            del target_h5[source_key]
        target_h5.create_dataset(source_key, data=data_to_copy, compression='gzip', compression_opts = 9)

    print(f"Successfully copied dataset '{source_key}' from '{source}' to '{target}'")
    print(f"Went from {os.path.getsize(source)/1e6} Mbytes into {os.path.getsize(target)/1e6} Mbytes with level 9 gzip compression.") 
    print(f"Time to copy and compress the dataset: {time.time() - start_time} secs.")

if __name__ == "__main__":
    source_file = "/BSRL/BSRL_5_min_histogram.h5" 
    target_file = "/BSRL/BSRL_data.h5"
    copy_key(source_file, target_file)




