#!/bin/bash


# Define a log file path
LOG_FILE="/BSRL/ldm/TimeHarp_260/data_acquisition.log"

# Redirect all output (stdout and stderr) to the log file
exec &>> "$LOG_FILE"

# Echo a timestamp to indicate script start
echo "Script started at $(date)"

cd /BSRL/ldm/TimeHarp_260/driver

if lsmod | grep -q th260pcie; then
    echo "Kernel module th260pcie is loaded. Continuing..."
else
    echo "Kernel module th260pcie is not loaded. Loading..."
    insmod th260pcie.ko
    
    if [ $? -ne 0 ]; then
    echo "Error: Failed to load kernel module th260pcie."
    exit 1
    else
    echo "Kernel module th260pcie loaded successfully."
    fi
fi

if dmesg | grep -i 'th260pcie' > /dev/null && lspci | grep -i 'Xilinx'; then
    echo " " 
    echo "The hardware device associated with 'Xilinx' and 'th260pcie' has been detected by the system." 
else 
    lspci | grep -i 'Xilinx'
    dmesg | grep -i 'th260pcie'
fi

echo "Data collection starting..."

cd /BSRL/ldm/TimeHarp_260
timestamp=$(date +'%d_%m_%Y_%H%M%S')
OUTPUT_FILE="/BSRL/BSRL_5_min_histogram.h5"

# Run data_acquisition for 5 minutes or 300000 milliseconds
python data_acquisition.py --output_file "$OUTPUT_FILE" --dataset_key  "${timestamp}" --acquisition_time 300000

echo ""
echo "Data collection finished."