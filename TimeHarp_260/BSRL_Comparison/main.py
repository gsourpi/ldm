from tools.file_handling import FileHandling
from tools.data_loader import DataLoader
from tools.data_visualization import DataVisualization
from online_analysis import OnlineAnalysis, RawTdcData
from compare_bsrl import CompareBSRL

import numpy as np
import logging
import h5py
import pytimber
from pyspark.sql import SparkSession
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
from nxcals.spark_session_builder import get_or_create, Flavor # Using Flavor.YARN_SMALL, all properties pre-set but can be overwritten, running on Yarn 

import sys 
import os
import json
import datetime 
import struct 
from datetime import datetime, timedelta, timezone 
from dataclasses import dataclass, field
from typing import List, Optional, Union
import time 
import importlib
import gc

from datetime import datetime
import pytz
# Define user-specific paths (update as needed per user)
USR_PATH = "/home/gsourpi/Desktop/git/"
TIMBER_DATA_PATH = os.getcwd() + '/timber_data'
TMTP_DATA_PATH = os.getcwd() + '/BSRL_data.h5'

sys.path.append("/home/gsourpi/Desktop/git/bsrl-ucap-analysis/")
try: 
    from bsrl_ucap_analysis.resources.scripts.BsrlScripts import (
        correct_histogram, validate_histogram, get_bunch_properties, Config,
        load_config_file, get_BQM_bucket_info, label_buckets, get_RF_parameters,
        get_bucket_offset, get_bucket_population, match_bunch_patterns, get_q,
        get_ref_params, get_bucket_limits, get_bucket_statistics
    )
    from bsrl_ucap_analysis.resources.scripts.helpfuncs.misc import (
        multiple_integration, multiple_integration_trapezoidal
    )
    from bsrl_ucap_analysis.resources.constants import constants as c
except ModuleNotFoundError as e: 
    print(f"Error: {e}")

constants_path = '/home/gsourpi/Desktop/git/bsrl-ucap-analysis/bsrl_ucap_analysis/resources/constants/constants.py'

def update_DT_TDC(new_value):
    with open(constants_path, 'r') as file:
        lines = file.readlines()
    with open(constants_path, 'w') as file:
        for line in lines:
            # Look for the line defining DT_TDC and replace it with the new value
            if line.startswith("DT_TDC ="):
                file.write(f"DT_TDC = {new_value}\n")
            else:
                file.write(line)

if __name__ == "__main__":
        
    # Configure logging
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    logging.basicConfig(level=logging.ERROR, format='%(asctime)s - %(levelname)s - %(message)s')
    
    # Create an instance of DataLoader
    data_loader = DataLoader()
    
    # Create two instances of TdcData 
    picoquant_data = RawTdcData(device_name = 'picoquant')
    acquiris_data  = RawTdcData(device_name = 'acquiris')
    
    # Connect to the Timber database 
    data_loader.connect_db()

    # Duration in which the picoquant (new) card was running in parallel with acquiris (old) card in LHC. (Local timezone UTC +02:00)
    start_time = "2024-07-05 00:00:00"
    stop_time = "2024-07-12 00:00:00"
    
    # Extract the durations which were significant; concerning the beam during start of RAMP and end of STABLE.
    significant_beam_durations = data_loader.get_significant_timestamps(start_time, stop_time, overwrite = False)
    
    # Load picoquant data and extract the datetime of the DAQs
    hf = data_loader.load_h5_file(os.getcwd() + '/BSRL_data.h5')
    datetimes_with_tdc_data = list(hf.keys())
    
    # Find and save the matching timestamps between the 2 independent DAQs
    picoquant_timestamps = []
    
    logging.info("Prompting user to confirm that the file contains the timestamps from PicoQuant that fall within the periods when the beam meets our standards.")
    user_input = input("Does the 'matching_timestamps' file exist? (y/n): ")
    
    if user_input.lower() == 'y' and FileHandling.is_valid_file('matching_timestamps.json') and not FileHandling.is_empty_file('matching_timestamps.json'): 
        _ , matching_timestamps_data = FileHandling.read_json_file('matching_timestamps.json')
    else: 
        
        # Initialize json file that will hold the matching timestamps 
        FileHandling.save_json_file('matching_timestamps.json', [], gzip_compression=False, append_mode=False)
        total_matches = 0 
        
        # Iterate over each fill's significant duaration (between RAMP and STABLE BEAMS)
        for beam_fill_data in significant_beam_durations:
            unix_ramp_start_time, unix_stable_beam_end_time = beam_fill_data['duration']
            
            # Convert Unix timestamps to datetime objects in UTC timezone 
            utc_ramp_start_time = datetime.fromtimestamp(unix_ramp_start_time, tz=timezone.utc).replace(tzinfo=None, microsecond=0)
            utc_stable_beam_end_time =datetime.fromtimestamp(unix_stable_beam_end_time, tz=timezone.utc).replace(tzinfo=None, microsecond=0)

            # Find the matching timestamps of new device 
            matching_timestamps, match_counter = data_loader.find_matching_timestamps(datetimes_with_tdc_data, utc_ramp_start_time, utc_stable_beam_end_time)
            logging.info(f"{match_counter} matching timestamps found in fill {beam_fill_data['fillNumber']}")
            total_matches += match_counter
            FileHandling.save_json_file('matching_timestamps.json', {"fill_number": beam_fill_data["fillNumber"], "timestamps": matching_timestamps, "count": match_counter}, gzip_compression=False, append_mode=True)

        logging.info(f"A total of {total_matches} matching timestamps found.") 
        _ , matching_timestamps_data = FileHandling.read_json_file('matching_timestamps.json')
    
    picoquant_timestamps = [timestamp for item in matching_timestamps_data if 'timestamps' in item for timestamp in item['timestamps']] # In UTC, not local timezone! 
    fill_numbers = np.concatenate([[fill_data["fill_number"]] * fill_data["count"] for fill_data in matching_timestamps_data]).astype(int)

    if len(picoquant_timestamps) != len(fill_numbers):
        raise ValueError("Error: The number of timestamps does not match the number of fill numbers.")    # print(len(timestamps))

    total_matches = sum(item['count'] for item in matching_timestamps_data if 'count' in item)
    logging.info(f"A total of {total_matches} matching timestamps found.")
        
    # Load or download the corresponding acquiris data from Timber 
    logging.info("Prompting user to decide on data download.")
    user_input = input("Download timber data? (y/n): ")
    
    # If selected yes: Download each 5-minute histogram to a different .pkl file in the 'timber_data' folder.
    if user_input.lower() == 'y':
        logging.info(f"Downloading timber data (as .pkl files) at 'timber_data' folder.")
        os.makedirs(os.path.join(os.getcwd(), 'timber_data' ), exist_ok=True)
        for timestamp in picoquant_timestamps:
            
            # Convert the UTC timestamp string to a datetime object
            timestamp_dt = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
            timestamp_dt = timestamp_dt.replace(tzinfo=timezone.utc)
            # Add 5 minutes in UTC
            timestamp_plus_acq_time_dt = timestamp_dt + timedelta(minutes=5)

            # Convert UTC datetime objects to local timezone
            # Assume local timezone is UTC+1
            local_tz_offset = timedelta(hours=1)
            local_timezone = timezone(local_tz_offset)

            timestamp_local_dt = timestamp_dt.astimezone(local_timezone)
            timestamp_plus_acq_time_local_dt = timestamp_plus_acq_time_dt.astimezone(local_timezone)

            # Convert both UTC datetime objects to strings
            timestamp_utc_str = timestamp_dt.strftime("%Y-%m-%d %H:%M:%S")
            timestamp_plus_acq_time_utc_str = timestamp_plus_acq_time_dt.strftime("%Y-%m-%d %H:%M:%S")

            # Convert both local timezone datetime objects to strings
            timestamp_local_str = timestamp_local_dt.strftime("%Y-%m-%d %H:%M:%S")
            timestamp_plus_acq_time_local_str = timestamp_plus_acq_time_local_dt.strftime("%Y-%m-%d %H:%M:%S")

            if FileHandling.is_valid_file(f'{os.path.dirname(__file__)}/timber_data/BSRL_timber_{timestamp_utc_str}_{timestamp_plus_acq_time_utc_str}.pkl'):
                continue  
            
            hist = data_loader.get_timber_data(
                "BSRL.B1.SYS_A:HIST", 
                timestamp_local_str, 
                timestamp_plus_acq_time_local_str
            )
            
            age_rev = data_loader.get_timber_data(
                "BSRL.B1.SYS_A:HIST_AGE_REV", 
                timestamp_local_str, 
                timestamp_plus_acq_time_local_str
            )
            FileHandling.save_pickle_file(f'{os.path.dirname(__file__)}/timber_data/BSRL_timber_{timestamp_utc_str}_{timestamp_plus_acq_time_utc_str}.pkl', data_to_save=[hist,age_rev], gzip_compression=9, append_mode=False)


    # Prepare for the online analysis    
    timber_data_filepath = os.getcwd() + "/timber_data/"
    if not os.path.exists(timber_data_filepath):
        logging.error(f"Error: The directory '{timber_data_filepath}' does not exist.")            
        sys.exit(1)
    timber_files = os.listdir(timber_data_filepath)
    os.makedirs(os.path.join(os.getcwd(), 'online_analysis_data' ), exist_ok=True)

    ##  Run the online analysis of the PicoQuant TDC ##  
    
    # Update TDC resolution
    update_DT_TDC(25e-12)
    importlib.reload(c)

    for i, timestamp in enumerate(picoquant_timestamps):
        continue 
        try:    
            if fill_numbers[i] >-1:
                utc_timestamp = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
                utc_timestamp = utc_timestamp.replace(tzinfo=timezone.utc)
                utc_plus_2 = timezone(timedelta(hours=2))
                utc_plus_2_time = utc_timestamp.astimezone(utc_plus_2)
                formatted_timestamp = utc_plus_2_time.strftime("%d_%m_%Y_%H%M%S")
                
                picoquant_data.raw_hist = hf[formatted_timestamp][:]

                success_flag, data = FileHandling.read_pickle_file(os.path.join(timber_data_filepath,timber_files[i]))
                if success_flag: 
                    picoquant_data.age_rev = data[1]['BSRL.B1.SYS_A:HIST_AGE_REV'][1][0]
                else: 
                    logging.error(f"Unable to read {timber_files[i]} in {timber_data_filepath}.")
                    sys.exit(1)

                picoquant_data_analysis = OnlineAnalysis(picoquant_data)
                picoquant_data_analysis.run_online_analysis(1, picoquant_data.raw_hist, picoquant_data.age_rev ,baseline_smoothing=3)
                
                picoquant_data_analysis.save_online_analysis(
                    filename=f'{os.path.dirname(__file__)}/online_analysis_data/fill_{fill_numbers[i]}/{picoquant_data.device_name}_device.pkl',
                    success_flag=success_flag, 
                    data=data,
                    timestamp=timestamp.replace(" ", "T"),
                    fill_number=fill_numbers[i],
                    rewrite_data=True,
                    gzip_compression=False
                )
                del data,picoquant_data_analysis
                gc.collect()
        
        except Exception as e:
            logging.error(f"An error occured: {e}")
            with open('online_analysis_data/errored_timestamps.txt', 'a') as file:
                file.write(f"{timestamp} - Error: {e}\n")
            print(f"Logged errored timestamp: {timestamp}, in fill {fill_numbers[i]} ")
        
    # Run the online analysis of the Acquiris TDC
    update_DT_TDC(50e-12)
    importlib.reload(c)

    picoquant_timestamps = [
    datetime.strptime(ts, "%Y-%m-%d %H:%M:%S") for ts in picoquant_timestamps]

    timber_files_sorted = sorted(
        timber_files,
        key=lambda f: datetime.strptime(f.rsplit(".", 1)[0].split("_")[2], "%Y-%m-%dT%H-%M-%S")
    )
    
    acquiris_timestamps = [
        datetime.strptime(f.rsplit(".", 1)[0].split("_")[2], "%Y-%m-%dT%H-%M-%S")
        for f in timber_files_sorted ]

    
    # Get acquiris_fill_numbers list
    acquiris_fill_numbers = []

    for acq_ts in acquiris_timestamps:
        assigned_fill = None
        for i in range(len(picoquant_timestamps) - 1):
            start = picoquant_timestamps[i]
            end = picoquant_timestamps[i + 1]
            if start <= acq_ts < end:
                acquiris_fill_numbers.append(fill_numbers[i])
                assigned_fill = fill_numbers[i]
                break
        
        if acq_ts < picoquant_timestamps[0]:
            acquiris_fill_numbers.append(fill_numbers[0]) 
        
        if assigned_fill is None and acq_ts >= picoquant_timestamps[-1]:
            acquiris_fill_numbers.append(fill_numbers[-1])
    
    if len(acquiris_timestamps) != len(acquiris_fill_numbers):
        raise ValueError("Error: The number of timestamps does not match the number of fill numbers.")   

    for i, acquiris_datafile in enumerate(timber_files_sorted):
        try: 
            if acquiris_fill_numbers[i] == 9877:
                success_flag, data = FileHandling.read_pickle_file(os.path.join(timber_data_filepath, acquiris_datafile))
                hist_dict, age_rev_dict = data 
                acquiris_data.raw_hist = hist_dict['BSRL.B1.SYS_A:HIST'][1][0]
                acquiris_data.age_rev =  age_rev_dict['BSRL.B1.SYS_A:HIST_AGE_REV'][1][0]
                acquiris_data_analysis = OnlineAnalysis(acquiris_data)
                
                acquiris_data_analysis.run_online_analysis(1, acquiris_data.raw_hist, acquiris_data.age_rev ,baseline_smoothing=3)
                
                start_of_acq = data[0]['BSRL.B1.SYS_A:HIST'][0][0]
                utc_datetime_str = datetime.fromtimestamp(start_of_acq, tz=timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
                
                acquiris_data_analysis.save_online_analysis(
                    filename=f'{os.path.dirname(__file__)}/online_analysis_data/fill_{acquiris_fill_numbers[i]}/{acquiris_data.device_name}_device.pkl',
                    success_flag=success_flag, 
                    data=data
                    timestamp=utc_datetime_str.replace(" ", "T"),
                    fill_number=acquiris_fill_numbers[i],
                    rewrite_data=False,
                    gzip_compression=False 
                )
                del data, acquiris_data_analysis
                gc.collect()
            else: 
                print(f"Skipping as we are on fill {fill_numbers[i]}")
            
        except Exception as e:
            logging.error(f"An error occured: {e}")
            with open('online_analysis_data/errored_timestamps.txt', 'a') as file:
                file.write(f"{timestamp} - Error: {e}\n")
            print(f"Logged errored timestamp: {timestamp}")