import numpy as np
import logging
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter

class DataVisualization:
    @staticmethod         
    def bin_data(data, original_resolution, target_resolution):
        bins_per_target_bin = int(target_resolution / original_resolution)
        
        aggregated_data = [
            sum(data[i:i + bins_per_target_bin])
            for i in range(0, len(data), bins_per_target_bin)
        ]
        
        # remove the last bin if it has fewer than bins_per_target_bin counts
        if len(data) % bins_per_target_bin != 0:
            aggregated_data = aggregated_data[:-1]
        
        xdata = np.array(range(len(aggregated_data))) * target_resolution
        
        return aggregated_data, xdata
    
    @staticmethod  
    def plot_histogram(title, histogram_list, bin_data=False, original_resolution=None, target_resolution=None):
        logging.info("Plotting histogram.")
        if original_resolution is None:
            return logging.error('Resolution not set.')
            
        if bin_data: 
            ydata, xdata = DataVisualization.bin_data(histogram_list, original_resolution, target_resolution) 
        else: 
            xdata = list(range(0, len(histogram_list)))
            xdata = np.array(xdata) * original_resolution 
            ydata = histogram_list
            
        _ , ax = plt.subplots()

        # Format the x-axis with scientific notation and time units
        formatter = EngFormatter(unit='s')
        ax.xaxis.set_major_formatter(formatter)

        ax.set_xlabel('Time')
        ax.set_ylabel('Count')
        
        if isinstance(title, str):
            ax.set_title(title)
        else: 
            try:
                ax.set_title(str(title))
            except (ValueError, TypeError):
                ax.set_title('Histogram') 
                
        ax.plot(xdata, ydata)
        plt.show()
        
    @staticmethod
    def plot_histograms(titles, histograms, bin_data, original_resolutions=None, target_resolutions=None):
        logging.info("Plotting histograms.")
        if original_resolutions is None:
            return logging.error('Resolutions not set.')
        
        num_histograms = len(histograms)
        fig, axes = plt.subplots(1, num_histograms, figsize=(5 * num_histograms, 4))

        for i, ax in enumerate(axes):
            histogram_list = histograms[i]
            title = titles[i]
            original_resolution = original_resolutions[i]
            target_resolution = target_resolutions[i]
            bin_data_option = bin_data[i]

            if bin_data_option:
                ydata, xdata = DataVisualization.bin_data(histogram_list, original_resolution, target_resolution)
            else:
                xdata = np.array(range(0, len(histogram_list))) * original_resolution
                ydata = histogram_list

            # Format the x-axis with scientific notation and time units
            formatter = EngFormatter(unit='s')
            ax.xaxis.set_major_formatter(formatter)

            ax.set_xlabel('Time')
            ax.set_ylabel('Count')
            if isinstance(title, str):
                ax.set_title(title)
            else:
                try:
                    ax.set_title(str(title))
                except (ValueError, TypeError):
                    logging.error("Conversion of title to string failed. Setting default title.")
                    ax.set_title('Histogram')

            ax.plot(xdata, ydata)

        plt.tight_layout()
        plt.show()
        