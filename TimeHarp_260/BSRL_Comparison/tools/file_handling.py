import numpy as np
import logging
import h5py
import os 
import json 
import struct
import gzip 
import pandas as pd
from decimal import Decimal
from datetime import datetime, date
import pickle
class FileHandling:
    @staticmethod
    def is_valid_file(filename):
        if os.path.isfile(filename):
            return True 
        else:
            logging.error(f"File not found: {filename}")
            return False
        
    @staticmethod
    def is_empty_file(filename): 
        if os.path.getsize(filename) > 0:
                return False
        else:
            logging.error(f"The file {filename} is empty.")
            return True
        
    @staticmethod
    def is_gzip_file(filename):
        # Check if the file is gzipped by reading the first two bytes
        with open(filename, 'rb') as f:
            return f.read(2) == b'\x1f\x8b'
        
    @staticmethod
    def read_json_file(filename):
        if FileHandling.is_valid_file(filename) and not FileHandling.is_empty_file(filename):
            try:
                if FileHandling.is_gzip_file(filename):
                    with gzip.open(filename, 'rt', encoding='utf-8') as json_file:
                        data = json.load(json_file)
                        logging.info(f"Successfully read gzipped JSON file: {filename}")
                else:
                    with open(filename, 'r') as json_file:
                        data = json.load(json_file)
                        logging.info(f"Successfully read JSON file: {filename}")
                return [True, data]
            except Exception as e:
                logging.error(f"Failed to read JSON file {filename}: {str(e)}")
                return [False, None]
        else: 
            return [False, None]
    
    def save_json_file(filename, data_to_save, gzip_compression=False, append_mode = False):
        try:
            if append_mode: 
                _, prev_data = FileHandling.read_json_file(filename=filename)
                prev_data.append(data_to_save)
                data_to_save = prev_data
            if gzip_compression:
                with gzip.open(filename, 'wt', encoding='utf-8') as json_file:
                    json.dump(data_to_save, json_file, indent=4)
                    logging.info(f"Data were successfully saved in (gzipped): {filename}.")
            else:
                with open(filename, 'w') as json_file:
                    json.dump(data_to_save, json_file, indent=4)
                    logging.info(f"Data were successfully saved in: {filename}.")
        except Exception as e:
            logging.error(f"Failed to save JSON file {filename}: {str(e)}")
                
    @staticmethod
    def load_h5_file(filename):
        if FileHandling.is_valid_file(filename) and not FileHandling.is_empty_file(filename):
            hf = h5py.File(filename, 'r')                
            return hf
        else:
            logging.error(f"File not found: {filename}")
            return None  
        
    @staticmethod
    def read_pickle_file(filename):
        if FileHandling.is_valid_file(filename) and not FileHandling.is_empty_file(filename):
            try:
                if FileHandling.is_gzip_file(filename):
                    with gzip.open(filename, 'rb') as pickle_file:
                        data = pickle.load(pickle_file)
                        logging.info(f"Successfully read gzipped pickle file: {filename}")
                else:
                    with open(filename, 'rb') as pickle_file:
                        data = pickle.load(pickle_file)
                        logging.info(f"Successfully read pickle file: {filename}")
                return [True, data]
            except Exception as e:
                logging.error(f"Failed to read pickle file {filename}: {str(e)}")
                return [False, None]
        else: 
            return [False, None]
    
    @staticmethod    
    def save_pickle_file(filename, data_to_save, gzip_compression=False, append_mode = False):
        try:
            if append_mode:
                _, prev_data = FileHandling.read_pickle_file(filename=filename)
                if prev_data is None:
                    prev_data = {}
                if isinstance(prev_data, dict):
                    prev_data.update(data_to_save)
                elif isinstance(prev_data, list):
                    prev_data.append(data_to_save)
                else:
                    logging.error(f"Data from {filename} is not a list or dict. Unable to append.")
                    return
                data_to_save = prev_data

            # Gzip compression
            if gzip_compression:
                with gzip.open(filename, 'wb') as pickle_file:
                    pickle.dump(data_to_save, pickle_file)
                    logging.info(f"Data were successfully saved and compressed in: {filename}.")
            else:
                with open(filename, 'wb') as pickle_file:
                    pickle.dump(data_to_save, pickle_file)
                    logging.info(f"Data were successfully saved in: {filename}.")
        except Exception as e:
            logging.error(f"Failed to save pickle file {filename}: {str(e)}")
            
    @staticmethod
    def save_to_hdf5(filename, timestamp, TDC_name, fill_info_dict, data_dict):
        with h5py.File(filename, 'a') as file: 
            if timestamp in file:
                datetime_group = file[timestamp]
                logging.info(f"Group '{timestamp}' already exists. Writing data into the existing group.")
            else:
                datetime_group = file.create_group(timestamp)
                logging.info(f"Group '{timestamp}' created.")
                
            if TDC_name in datetime_group:
                tdc_group = datetime_group[TDC_name]
                logging.info(f"Subgroup '{TDC_name}' already exists within group '{timestamp}'. Writing data into the existing subgroup.")
            else:
                tdc_group = datetime_group.create_group(TDC_name)
                logging.info(f"Subgroup '{TDC_name}' created within group '{timestamp}'.")

            if 'fill_info' in datetime_group:
                fill_group = datetime_group["fill_info"]
                logging.info(f"'fill_info' group already exists within '{timestamp}'. Writing data into the existing group.")
            else:
                fill_group = datetime_group.create_group("fill_info")
                logging.info(f"'fill_info' group created within '{timestamp}'.")

            for name, data in fill_info_dict.items(): 
                if name in fill_group:
                    del fill_group[name]  # Delete the existing dataset
                    logging.info(f"Dataset '{name}' already exists. Overwriting it.")
                    
            fill_group.create_dataset('fillNumber', data = fill_info_dict['fillNumber'])
            fill_group.create_dataset('duration', data = fill_info_dict['duration'])
            logging.info(f"Fill related data saved in group 'fill_info' under '{timestamp}'.")
    
            for name, data_list in data_dict.items():
                # Pack into binary format
                if isinstance(data_list, list):
                        binary_sub_list = struct.pack(f'{len(data_list)}I', *data_list)
                else:
                        binary_sub_list = struct.pack('I', data_list)
                # binary_sub_list_array points to the same memory buffer as the data from binary_sub_list
                binary_sub_list_array = np.frombuffer(binary_sub_list, dtype=np.uint32)
                if name in tdc_group:
                    del tdc_group[name]  # Delete the existing dataset
                    logging.info(f"Dataset '{name}' already exists. Overwriting it.")
                    
                tdc_group.create_dataset(name, data=binary_sub_list_array)

            logging.info(f"Results saved as binary data at {filename}")
            
