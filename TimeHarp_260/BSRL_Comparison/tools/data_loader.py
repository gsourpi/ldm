import numpy as np
import logging
import h5py
import pytimber
from pyspark.sql import SparkSession
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
from nxcals.spark_session_builder import get_or_create, Flavor # Using Flavor.YARN_SMALL, all properties pre-set but can be overwritten, running on Yarn 

import os
import datetime 
from datetime import datetime, timedelta, timezone 
from dataclasses import dataclass, field
from typing import List, Optional, Union

from .file_handling import FileHandling

class DataLoader(FileHandling): 
    def __init__(self):
        super().__init__
        self.ldb = None 
        self.raw_hist = None 
        self.age_rev = None 
        self.beam_number = None 
        
    def connect_db(self):
        spark_session = SparkSession.builder.appName('MY_APP').config('spark.sql.parquet.columnarReaderBatchSize', '2').config("spark.driver.memory", "4g").getOrCreate()
        self.ldb = pytimber.LoggingDB(spark_session=spark_session)
        logging.info("-----------------------------------------------------------------------------------------------------")
        logging.info("Successfully connected to Timber Database.")
        return self.ldb

    def find_matching_timestamps(self, available_keys, start_dt, stop_dt):
        matching_timestamps = []
        match_counter = 0 
        
        for key in available_keys: 
            # Convert timestamp from geneva's summer local timezone (UTC +2) to UTC. 
            key_dt = datetime.strptime(key, "%d_%m_%Y_%H%M%S").replace(tzinfo=timezone(timedelta(hours=2))).astimezone(timezone.utc).replace(tzinfo=None, microsecond=0)
            if start_dt <= key_dt <= stop_dt:
                matching_timestamps.append(key_dt.strftime("%Y-%m-%d %H:%M:%S"))
                match_counter += 1
        return matching_timestamps, match_counter
    
    def get_timber_data(self, data_to_retrieve, start_time, stop_time): 
        logging.info("Loading Timber Data...")
        if not self.ldb:
            logging.error("Database connection not established.")
            return None
        try:
            if data_to_retrieve == 'fills': 
                # Find all fill numbers that contained INJPHYS
                data = self.ldb.get_lhc_fills_by_time(start_time, stop_time, beam_modes='INJPHYS')
            else: 
                # Assumes that we are passing Local Timezone timestamps, converts to UTC and saves the corresponding unix. 
                data = self.ldb.get(data_to_retrieve, start_time, stop_time)
            logging.info("Loaded Timber Data.")
            return data
        except Exception as e:
            logging.error(f"Failed to load data: {e}")
            return None
        
    def process_fills(self, fills):
        ramp_start_times, stable_beam_end_times, significant_durations = [], [], []
        
        for fill in fills:
            for mode in fill['beamModes']:
                if mode['mode'] == "RAMP":
                    ramp_start_times.append(mode['startTime'])
                elif mode['mode'] == 'STABLE':
                    stable_beam_end_times.append(mode['endTime'])
                    
            significant_durations.append({
                'fill_number': fill["fillNumber"], 
                'duration': [ramp_start_times[-1], stable_beam_end_times[-1]]
            })
        
        fills.append({'significant_durations': significant_durations})
        logging.info("Significant durations extracted successfully.")
        return fills
    
    def get_significant_timestamps(self, start_time, stop_time, overwrite = False):

        fills_filename = os.path.join(os.getcwd(), f'fills_{start_time}_{stop_time}.json')
        success_flag, fills = FileHandling.read_json_file(fills_filename)
        
        if success_flag: 
            if 'significant_durations' in fills[-1]:
                if overwrite: 
                    logging.info("Deleting existing durations...")
                    del fills[-1]['significant_durations']
                    
                    logging.info("Initiating download of fill data from Timber...")
                    fills = self.get_timber_data('fills', start_time, stop_time)
                    fills = self.process_fills(fills)
                    FileHandling.save_json_file(fills_filename, fills, gzip_compression = False)
                    return fills[-1]['significant_durations']
                    
                else: 
                    logging.info(f"Extracting significant durations from JSON file...")
                    return fills[-1]['significant_durations']
            
            else: 
                logging.info("Significant durations not found. Begin processing...")
                fills = self.process_fills(fills)
                FileHandling.save_json_file(fills_filename, fills, gzip_compression=False)
                return fills[-1]['significant_durations']
            
        else: 
            logging.error("Fills file not found.")
            logging.info("Initiating download of fill time data from Timber...")
            fills = self.get_timber_data('fills', start_time, stop_time)
            fills = self.process_fills(fills)
            FileHandling.save_json_file(fills_filename, fills, gzip_compression = False)
            return fills[-1]['significant_durations']
        
