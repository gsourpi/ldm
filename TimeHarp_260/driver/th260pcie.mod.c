#include <linux/module.h>
#define INCLUDE_VERMAGIC
#include <linux/build-salt.h>
#include <linux/elfnote-lto.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;
BUILD_LTO_INFO;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(".gnu.linkonce.this_module") = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section("__versions") = {
	{ 0xa569808b, "module_layout" },
	{ 0xc1514a3b, "free_irq" },
	{ 0xd7695b93, "device_destroy" },
	{ 0xe50a0773, "cdev_del" },
	{ 0xe0eed738, "_dev_info" },
	{ 0x4a453f53, "iowrite32" },
	{ 0x5b8239ca, "__x86_return_thunk" },
	{ 0xbdfb6dbb, "__fentry__" },
	{ 0x159fc17e, "pci_unregister_driver" },
	{ 0x659e1c5e, "__pci_register_driver" },
	{ 0xd9a5ea54, "__init_waitqueue_head" },
	{ 0x6091b333, "unregister_chrdev_region" },
	{ 0xa22f07f3, "class_destroy" },
	{ 0xe3ec2f2b, "alloc_chrdev_region" },
	{ 0xc52db6bb, "class_create" },
	{ 0x4c9d28b0, "phys_base" },
	{ 0x92540fbf, "finish_wait" },
	{ 0x8ddd8aad, "schedule_timeout" },
	{ 0x8c26d495, "prepare_to_wait_event" },
	{ 0xfe487975, "init_wait_entry" },
	{ 0x88db9f48, "__check_object_size" },
	{ 0xe2c17b5d, "__SCT__might_resched" },
	{ 0x7cd8d75e, "page_offset_base" },
	{ 0xbe9f85eb, "pcpu_hot" },
	{ 0xf49db96, "pci_disable_device" },
	{ 0x9cbfb38f, "device_create" },
	{ 0x92d5838e, "request_threaded_irq" },
	{ 0x142a0e27, "dma_set_coherent_mask" },
	{ 0x85bc4c9f, "dma_set_mask" },
	{ 0x85a8deac, "pci_set_master" },
	{ 0x6a5cb5ee, "__get_free_pages" },
	{ 0x556422b3, "ioremap_cache" },
	{ 0x305005eb, "pci_request_region" },
	{ 0xa2fe031, "pci_enable_device" },
	{ 0x2be5967a, "cdev_add" },
	{ 0x2b8cf2d9, "cdev_init" },
	{ 0xc40d0601, "cdev_alloc" },
	{ 0x4e674b18, "_dev_err" },
	{ 0xd0da656b, "__stack_chk_fail" },
	{ 0x6b10bee1, "_copy_to_user" },
	{ 0x92997ed8, "_printk" },
	{ 0xe748bd1e, "remap_pfn_range" },
	{ 0xd35cce70, "_raw_spin_unlock_irqrestore" },
	{ 0x34db050b, "_raw_spin_lock_irqsave" },
	{ 0xb5b54b34, "_raw_spin_unlock" },
	{ 0xe2964344, "__wake_up" },
	{ 0xa78af5f3, "ioread32" },
	{ 0xba8fbd64, "_raw_spin_lock" },
	{ 0x4302d0eb, "free_pages" },
	{ 0x8b6ef3a9, "pci_release_region" },
	{ 0xedc03953, "iounmap" },
};

MODULE_INFO(depends, "");

MODULE_ALIAS("pci:v000010EEd00001012sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "05027818AA6BEBCA5941D6E");
MODULE_INFO(rhelversion, "9.4");
