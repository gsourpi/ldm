import numpy as np
import random as rand
import matplotlib.pyplot as plt

def TDC_Emulator(sizeOfPreallocCounts,
                startBinOfFirstMean, endBinOfFirstMean,
                countNoise, 
                period,periodNoise, 
                N, std, stdNoise, 
                maxNumberOfPeaks, minNumberOfPeaksInstace, maxNumberOfPeaksInstace, 
                pause):

    preallocate = [0]* sizeOfPreallocCounts
    currentBinOfMean = rand.randint(startBinOfFirstMean, endBinOfFirstMean)
    numberOfPeaks = 0 # In total 

    while numberOfPeaks < maxNumberOfPeaks and currentBinOfMean < sizeOfPreallocCounts-5*std: 
        numberOfPeaksInstance = 0
        while numberOfPeaksInstance < rand.randint(minNumberOfPeaksInstace+1,maxNumberOfPeaksInstace) and currentBinOfMean < sizeOfPreallocCounts-5*std:
            counts = N
            preallocate[currentBinOfMean] += N - rand.randint(1,countNoise)
            currentStd = std + ((-1)**rand.randint(1,2))*stdNoise*rand.random()
            binStep = 1 # From medium of gaussian distribution  
            while counts > 0: 
                counts = np.round(N*np.exp(-(binStep**2/currentStd**2)/2)).astype(int)
                leftCount = counts + ((-1)**rand.randint(1,2))*rand.randint(1,countNoise)
                rightCount = counts + ((-1)**rand.randint(1,2))*rand.randint(1,countNoise)
                if rightCount>0:
                        preallocate[currentBinOfMean+binStep] += rightCount
                if leftCount>0:
                        preallocate[currentBinOfMean-binStep] += leftCount
                binStep += 1
            currentBinOfMean += period + ((-1)**rand.randint(1,2))*rand.randint(1,periodNoise)
            numberOfPeaksInstance += 1
        numberOfPeaks += numberOfPeaksInstance
        currentBinOfMean += 6*std + rand.randint(0,pause)
    return preallocate


# preallocate = TDC_Emulator(sizeOfPreallocCounts=5000, 
#                         startBinOfFirstMean=50, endBinOfFirstMean=100,
#                         countNoise = 20, period = 80 , periodNoise = 2, 
#                         N = 100, std = 10, stdNoise = 0.2, 
#                         maxNumberOfPeaks = 100, minNumberOfPeaksInstace = 5, maxNumberOfPeaksInstace = 20, 
#                         pause = 10)

# plt.bar(np.arange(0,len(preallocate)),preallocate)
# plt.show(block = True)