# branched off wordtoHist

from matplotlib import pyplot as plt
from matplotlib.ticker import EngFormatter
import numpy as np

specialBitMask = 0x80000000
specialBitShift = 31

syncMask = 0xFE000000
syncEventMasked = 0x80000000

overflowMask = 0xFE000000
overflowEventMasked = 0xFE000000

channelMask = 0x7E000000
channelShift = 25
timeTagMask = 0x1FFFFFF

OVF_PERIOD = 33554432
HIST_TIME = 100e-6
TDC_RES = 25e-12

hist_size = HIST_TIME/TDC_RES

# This way we imitate having the data already in memory
data = []
file = open("/home/gsourpi/Desktop/LDM/Data from PicoQuant/1sec-50PM.txt")
for line in file:
    data.append(int(line))

# Create an empty histogram
histogram = [0] * int(hist_size)

firstSyncIndex = -1
# Find first sync
for index, word in enumerate(data):
    if( (word & syncMask) == syncEventMasked ):
        latestSync = (word & timeTagMask)
        firstSyncIndex = index
        #print("First sync found")
        break

# Go through the data, starting after the sync
overflowInTurn = 0
for word in data[firstSyncIndex:]:    

    # Check for sync
    if( (word & syncMask) == syncEventMasked ):
        # Sync found. Replace current sync with new one and reset overflows
        latestSync = (word & timeTagMask)
        overflowInTurn = 0
    
    # Check for overflow
    elif( (word & syncMask) == overflowEventMasked ):
        # Overflow in this turn. Add OVF_PERIOD to time counts until next sync
        overflowInTurn =+ 1
        #print("Overflow")

    # Check for marker
    elif( (word & specialBitMask) != 0 ):
        # Marker event. Ignore for now
        pass
        #print("Marker")
    
    # Check for hit
    else:
        time = ( (word & timeTagMask) + overflowInTurn*OVF_PERIOD) - latestSync
        # Add to histogram if inside the HIST_TIME
        if(time <= hist_size):
            histogram[time] += 1
        else:
            print("Hit outside histogram limits")
        
        
""" plt.plot(histogram)
plt.xticks(ticks=plt.xticks()[0][1:], labels=TDC_RES * np.array(plt.xticks()[0][1:], dtype=np.float64))
plt.show()
"""

xdata=list( range(0,len(histogram) ) )
xdata = np.array(xdata) * TDC_RES
fig, ax = plt.subplots()
formatter = EngFormatter(unit='s')
ax.xaxis.set_major_formatter(formatter)

ax.plot(xdata,histogram)

plt.savefig('plot.jpg')
plt.show()



print("done")