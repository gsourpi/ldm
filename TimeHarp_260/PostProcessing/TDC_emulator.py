import random
import numpy as np
import matplotlib.pyplot as plt

# The bins are preallocated in this emulator 

def generateRandomMeanValues(startingMeanValue, lastMeanValue, meanSeparation, count):
        result = []
        startingMean = random.randint(startingMeanValue,lastMeanValue)  
        result.append(startingMean)
        for _ in range(1, count):
            newMean = result[-1] + meanSeparation
            if newMean <= HIST_TIME:
                result.append(newMean)
            else:
                break
        return result
    
def plotHistograms(counts, bins): 
        for i, count in enumerate(counts): 
            plt.hist(count, bins, alpha=0.5, label=f'Count {i+1}')
    
def create_histogram_data(counts, bins):
        histogram_data = np.zeros(len(bins) - 1, dtype=int) # Preallocate the counts
        for data in counts:
            idx = np.searchsorted(bins, data, side='right') - 1
            for index in idx: 
                try: 
                    histogram_data[index] += 1
                except: 
                    continue
        return histogram_data
    
def TDC_Emulator(meanSeparation, numberOfHists, 
                startingMeanValue, lastMeanValue, 
                startingStdValue,lastStdValue,
                binStart, binEnd, numOfSamplesGen, 
                plotHist=True, plotBar=False): 
    global HIST_TIME
    # Set the bins 
    if binEnd == None: 
        HIST_TIME = numberOfHists * meanSeparation
        binEnd = HIST_TIME
    else:
        pass # Use the one set by the user 
    
    bins = np.linspace(binStart, binEnd, numOfSamplesGen, dtype= int)

    counts = []

    meanValues = generateRandomMeanValues(startingMeanValue, lastMeanValue, meanSeparation, numberOfHists)
    stdValues  = [random.randint(startingStdValue,lastStdValue) for _ in range(numberOfHists)]
    for i, _ in enumerate(meanValues): 
        counts.append([random.gauss(meanValues[i], stdValues[i]) for _ in range(300)])
        
    histogram_counts = create_histogram_data(counts, bins)
    
    if plotHist: 
        plt.figure(1)
        plotHistograms(counts, bins)
        plt.legend(loc='upper right')
        plt.show(block = False)
    
    if plotBar:
        plt.figure(2)
        
        plt.bar(bins[:-1], histogram_counts, width=np.diff(bins), align='edge')
        plt.xlabel('Bins')
        plt.ylabel('Frequency')
        plt.title('Histogram')
        plt.show(block = True)
        
    return histogram_counts, bins, numberOfHists, meanSeparation

#########################################################################################
# Set the window size and the number of histograms/hits 
meanSeparation  = 500 # Window Size
numberOfHists = 35

# Set the the range in which the first mean will belong to
startingMeanValue = 3
lastMeanValue = 10 

# Set the the range in which the stds will belong to
startingStdValue = meanSeparation//12
lastStdValue = meanSeparation//8

# Set the bins 
HIST_TIME = numberOfHists * meanSeparation
print('Histogram Time is: ', HIST_TIME)
numOfSamplesGen = 15500
bins = np.linspace(0, HIST_TIME, numOfSamplesGen, dtype= int)

TDC_Emulator(meanSeparation = meanSeparation, numberOfHists = 35, 
            startingMeanValue = startingMeanValue, lastMeanValue = lastMeanValue, 
            startingStdValue = startingStdValue, lastStdValue = lastStdValue,
            binStart = 0, binEnd = None, numOfSamplesGen = 300, 
            plotHist=True, plotBar=True)


