import time 
import pickle 
import matplotlib
from matplotlib import pyplot as plt 
import numpy as np
from line_profiler import LineProfiler
from matplotlib.ticker import EngFormatter, FuncFormatter
from scipy.optimize import curve_fit
from postProcessingPlots import plotSomeOnSameGraph, plotNumSubPlots, plotAllOnSameGraph
from TDC_emulator_v2 import TDC_Emulator

# Define necessary lists and parameters 
#preallocateCounts = [0] * int(1 * 1e8)

#@profile 
def writeLists(path, list1, list2): 
            print('Started writing to pkl file...')
            with open(path, 'wb') as dD: 
                pickle.dump( [list1,list2], dD)
                print(f'\nDone writing lists to {path} file!')

# Define the Gaussian function
#@profile
def gaussian(x, mu, sigma, A):
    return A * np.exp(-0.5 * ((x - mu) / sigma)**2)

#@profile
def meanFromGaussianFit(bins, counts, binWindowSize): 
    bins = np.array(bins)
    bin_centers = (bins[:-1] + bins[1:]) / 2
    bin_centers = np.append(bin_centers, (bins[-1] + bins[-2]) / 2)

    # Initial guesses for parameters [mu, sigma, A]
    initStd = binWindowSize/2
    p0 = [np.mean(bin_centers), initStd , np.max(counts)//2]
    
    counts.append(0)
    
    # Fit Gaussian to histogram data
    popt = curve_fit(gaussian, bin_centers, counts, p0 = p0)[0]
    
    # # Extract the optimized values for mu, sigma, and A
    # mu_opt, sigma_opt, A_opt = popt

    print("Mean of the fitted distribution:", popt[0])
    
    return int(popt[0])
#@profile
def meanCalculation(binWindow): 
    return np.mean(binWindow)
        
def getBinsOfCountPeaks(binsWithCounts): 
        binsOfCountPeaks = []
        countThreshold = 10 #threshold so that maxima only from 100 counts and above will be considered -> other is considered noise

        for j, bin in enumerate(binsWithCounts):
            is_maxima = preallocateCounts[bin] > preallocateCounts[bin-1] and preallocateCounts[bin] > preallocateCounts[bin+1]
            if is_maxima and preallocateCounts[bin] > countThreshold: 
                binsOfCountPeaks.append([j,bin]) # j in the bins with counts list 

            print("The number of hits are: ", len(binsOfCountPeaks) )
            return binsOfCountPeaks
        
def TDCDecode(words): 
    # Define the bit masks for decoding 
    specialBitMask = 0x80000000
    channelMask = 0x7E000000
    syncMask = 0xFE000000
    syncEventMasked = 0x80000000
    overflowMask = 0xFE000000
    overflowEventMasked = 0xFE000000
    timeTagMask = 0x1FFFFFF

    global syncTimetag
    syncTimetag = 0 
    for word in words: 
            # If special bit is set (1) and channel is 0 
            if (word & specialBitMask) != 0 and ( (word & channelMask) >>25) == 0: 
                # Update synchronization timetag
                syncTimetag = word & timeTagMask 
                
            # Check for overflow
            elif( (word & syncMask) == overflowEventMasked ):
                # Overflow in this turn. Add OVF_PERIOD to time counts until next sync
                overflowInTurn =+ 1
                #print("Overflow")

            # If special bit is clear (0)
            elif (word & specialBitMask == 0) and syncTimetag > 0: 
                # Add regular measurement to counts of the corresponding bin and convert to picoseconds
                preallocateCounts[   ((word & timeTagMask) - syncTimetag) ] += 1

##@profile
def TDCGroupHits(preallocateCounts, binWindowSize, binWindowStep, binWindowSizeNano ):
    global optimalCenterBinFound, binWindow
    optimalCenterBinFound = False

    if preallocateCounts: 
        indexes = np.where(preallocateCounts)[0] # the bins where the counts are non zero 
        binsWithCounts = list(indexes) 
        
        # Make bins into nanoseconds from picoseconds 
        multiplier = 1e-3
        binsWithCountsNano = [bin*25*(multiplier) for bin in binsWithCounts]
        
        num_windows = len(binsWithCounts)//binWindowSize # number of times we will shift the windows to the right 

    for i in range(num_windows):
        if num_windows % 10 == 0: 
            optimalCenterBinFound = False
            
        # Do a gaussian fit until the mean computed manually and the mean from the gaussian are the same 
        if optimalCenterBinFound == False: 
            if i ==0: 
                binWindow = list(range(binsWithCounts[i]-1, binsWithCounts[i] + binWindowSize ))
                binWindowNano = list(range(int(binsWithCountsNano[i])-1, int(binsWithCountsNano[i]) + binWindowSizeNano))
                # Initialize the window step counter
                windowStepCounter=0
            else:
                startOfBinWindow = centerBin - binWindowSize//2 + binWindowStep * windowStepCounter
                endOfBinWindow   = centerBin + binWindowSize//2 + binWindowStep * windowStepCounter

                # print('Bin Window: ', startOfBinWindow, ' until ', endOfBinWindow)
                # print('Prealloc: ', preallocateCounts[startOfBinWindow: endOfBinWindow])

                binWindow = list(range(startOfBinWindow, endOfBinWindow)) 
                
            estimatedCenterBin = meanCalculation(binWindow=binWindow)
            fittedCenterBin = meanFromGaussianFit(bins=binWindow, counts=preallocateCounts[binWindow[0]:binWindow[-1]], binWindowSize= binWindowSize)
            
            print('Window estimated center bin in ns: ' , estimatedCenterBin * 25 * 1e-3 ) 
            print('Fitted Bin Center in ns: ', fittedCenterBin * 25 * 1e-3) 
            
            if abs(estimatedCenterBin - fittedCenterBin) > 23: # or 0.575 ns
                print("The difference is:", abs(estimatedCenterBin - fittedCenterBin))
                centerBin = fittedCenterBin
            else: 
                # Set this as the general bin center and don't do gaussian fit for the mean again 
                centerBin = int(estimatedCenterBin)
                optimalCenterBinFound = True
            
            # Update the new window limits base on the new centered bin
            windowStepCounter = 0 
            if centerBin < binWindowSize//2: 
                startOfBinWindow = 0 
            else: 
                startOfBinWindow = centerBin - binWindowSize//2 + binWindowStep * windowStepCounter
            endOfBinWindow   = centerBin + binWindowSize//2 + binWindowStep * windowStepCounter

        else: 
            startOfBinWindow = centerBin - binWindowSize//2 + binWindowStep * windowStepCounter
            endOfBinWindow   = centerBin + binWindowSize//2 + binWindowStep * windowStepCounter
            
        inside_condition = preallocateCounts[startOfBinWindow: endOfBinWindow]
        # Update window step counter 
        windowStepCounter += 1
        
        if any(inside_condition): # if it is not an empty list 
            hits_lists.append(list(range(startOfBinWindow, endOfBinWindow)))
                
    return hits_lists


### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
preallocateCounts = TDC_Emulator(sizeOfPreallocCounts=6000, 
                        startBinOfFirstMean=50, endBinOfFirstMean=100,
                        countNoise = 20, period = 80 , periodNoise = 2, 
                        N = 100, std = 10, stdNoise = 0.2, 
                        maxNumberOfPeaks = 100, minNumberOfPeaksInstace = 5, maxNumberOfPeaksInstace = 20, 
                        pause = 10)

start_time = time.time()

# Set appropriate window parameters 
binWindowSize = int(80e3 / 25) # nanoseconds to bins which are in picoseconds with 25 bins resolution 
binWindowSizeNano = 80 # nanoseconds 
binWindowStep = binWindowSize

# To make bins (that are in pscecs) into nanoseconds do : bin * 25 * 10^(-3)
hits_lists = []
hits_lists = TDCGroupHits(preallocateCounts, binWindowSize= 80, binWindowStep=80, binWindowSizeNano=80)
end_time = time.time()
print("\nElapsed time for making the list of hits", end_time - start_time, "seconds\n")

###  Write lists to pkl file ###
start_time = time.time()
writeLists(path ='egw1.pkl', list1 = preallocateCounts, list2 = hits_lists)
end_time = time.time()
print("\nElapsed time for writting to pickle file:", end_time - start_time, "seconds\n")

