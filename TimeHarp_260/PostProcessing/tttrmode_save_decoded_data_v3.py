import time 
import pickle 
import matplotlib
from matplotlib import pyplot as plt 
import numpy as np
from line_profiler import LineProfiler
from scipy.optimize import curve_fit


file_path = "/home/gsourpi/Desktop/LDM/Data from PicoQuant/1sec-0PM.txt" 
#file_path = "/home/gsourpi/Desktop/LDM/0.1secs-0PM.txt"
#file_path = '/home/gsourpi/Desktop/LDM/Data from PicoQuant/5secs-0PM.txt'
#file_path = '/home/gsourpi/Desktop/LDM/Data from PicoQuant/10secs-0PM.txt'

# Define the bit masks 
specialBitMask = 0x80000000
channelMask = 0x7E000000
timeTagMask = 0x1FFFFFF

# Preallocate the bins and the counts 
preallocateCounts = [0] * int(1 * 1e8)
syncTimetag = 0 
binsOfCountPeaks = []
countThreshold = 10 #threshold so that maxima only from 100 counts and above will be considered -> other is considered noise
hits_lists =  []



with open(file_path, 'r') as file:
    lines = file.readlines()

words = [int(line.strip()) for line in lines]

#@profile 
def writeLists(path, list1, list2): 
        print('Started writing to pkl file...')
        with open(path, 'wb') as dD: 
            pickle.dump( [list1,list2], dD)
            print(f'\nDone writing lists to {path} file!')

# Define the Gaussian function
def gaussian(x, mu, sigma, A):
   return A * np.exp(-0.5 * ((x - mu) / sigma)**2)

def gaussianFit(bins, preallocateCounts): 
    bins = np.array(bins)
    bin_centers = (bins[:-1] + bins[1:]) / 2

    # Initial guesses for parameters [mu, sigma, A]
    p0 = [np.mean(bin_centers), np.std(bin_centers)/2, np.max(preallocateCounts[bins[0]: bins[-1]])//2]

    # Fit Gaussian to histogram data
    popt = curve_fit(gaussian, bin_centers, preallocateCounts[bins[0]: bins[-1]], p0 = p0)[0]

    # # Extract the optimized values for mu, sigma, and A
    # mu_opt, sigma_opt, A_opt = popt

    print("Mean of the fitted distribution:", popt[0])

    return popt, bin_centers, preallocateCounts

def plotHist(ax, bins, preallocateCounts, plotGaussian = False):
    rects = [plt.Rectangle((i, 0), 1, preallocateCounts[i]) for i in bins]

    if not rects:
        print("No rectangles to plot.") 
        return
        
    for rect in rects:
        ax.add_patch(rect)
        
    ax.set_xlabel('Bins')
    ax.set_ylabel('Counts')

    xlim_min = min(bins) - 1
    xlim_max = max(bins) + 1

    ax.set_xlim(xlim_min, xlim_max)
    ax.set_ylim(0, max(preallocateCounts[bins[0]:bins[len(bins) -1]]) + 5)
    plt.grid(True)


    if plotGaussian:
        popt, bin_centers, preallocateCounts = gaussianFit(bins, preallocateCounts)
        x_range = np.linspace(bin_centers.min() -1 , bin_centers.max() +1 , 100)
        plt.plot(x_range, gaussian(x_range, *popt), color='red', label='Gaussian Fit')
        
        plt.axvline(x=popt[0], color='blue', linestyle='--', label='Mean of fitted Gaussian Distribution')
        #plt.axvline(x=, color='purple', linestyle='--', label='Mean of peaks window difference')

        print("Mean of the fitted distribution:", popt[0])
        
start_time = time.time()

for word in words: 

    # If special bit is set (1) and channel is 0 
    if (word & specialBitMask) != 0 and ( (word & channelMask) >>25) == 0: 
        # Update synchronization timetag
        syncTimetag = word & timeTagMask 
    
    # If special bit is clear (0)
    elif (word & specialBitMask == 0) and syncTimetag > 0: 
        # Add regular measurement to counts of the corresponding bin
        preallocateCounts[   ((word & timeTagMask) - syncTimetag) * 25 ] += 1

end_time = time.time()
print("\nElapsed time for decoding:", end_time - start_time, "seconds\n")

start_time = time.time()
if preallocateCounts: 
    indexes = np.where(preallocateCounts)[0] # the bins where the counts are non zero 
    binsWithCounts = list(indexes)

    for j, bin in enumerate(binsWithCounts):
        is_maxima = preallocateCounts[bin] > preallocateCounts[bin-1] and preallocateCounts[bin] > preallocateCounts[bin+1]
        if is_maxima and preallocateCounts[bin] > countThreshold: 
            binsOfCountPeaks.append([j,bin]) # j in the bins with counts list 

    print("The number of hits are: ", len(binsOfCountPeaks) )

for i, data in enumerate(binsOfCountPeaks): 
    centerBin = data[0]
    binResolution = 25//2 # picoseconds - hardcoded based on our hardware

    # If it is the first peak 
    if i == 0: 
        hits_lists.append(binsWithCounts[0 : centerBin + binResolution ])
        
    # If it is the last peak 
    elif i == (len(binsOfCountPeaks) - 1) :
        hits_lists.append(binsWithCounts[centerBin - binResolution : len(binsWithCounts)-1])
        
    elif binsOfCountPeaks[i+1] > data[0] + 0.75*binResolution or binsOfCountPeaks[i+1] < data[0] + binResolution//2: 
        # if the bin of the next peak is further than 3/4 of the resolution window
        # or the bin of the next peak is inside the window of the current window peak 
        start = binsOfCountPeaks[i-1][0] +  (data[0]- binsOfCountPeaks[i-1][0])//2 + 1
        end = data[0] + (binsOfCountPeaks[i+1][0] - data[0]) //2 + 1
        fittedCenterBin = gaussianFit(bins = binsWithCounts[start:end], preallocateCounts = preallocateCounts)[0]
        hits_lists.append(binsWithCounts[fittedCenterBin - binResolution : fittedCenterBin + binResolution ])
        
        # The gaussian fit will still put the medium where there are data points 
        # If there are a lot of zeros, the medium will remain in the desirable range 
    else:   
        # For each peak set the center of the hits list and the hardcoded window based on resolution
        hits_lists.append(binsWithCounts[centerBin - binResolution : centerBin + binResolution ])
        

end_time = time.time()
print("\nElapsed time for making the list of hits", end_time - start_time, "seconds\n")

start_time = time.time()

#writeLists(path ='decodedData_10secs_0pm.pkl', list1 = preallocateCounts, list2 = hits_lists)

end_time = time.time()
print("\nElapsed time for writting to pickle file:", end_time - start_time, "seconds\n")


input("Press Enter to Plot the Histogram of the first hit.")

with open('decodedData_v2.pkl', 'rb') as f:
    data = pickle.load(f)
    # Saved in a file in the form: [ [Counts], [ [Hit 1], [Hit 2], ...] ]

plt.ion()
fig, ax = plt.subplots()
# for the first to substract the medium and do the alignment 
# for the rest dont compute the window difference 
plotHist(ax, bins = data[1][2], preallocateCounts = preallocateCounts, fitGaussian = True)
plt.grid(True)
plt.show()
plt.pause(1000)


# This approach finds the local maxima in the bins that correspond to non-zero counts.
# Then it creates the lists of the hits based on the bin difference between the neighboring local maxima of the current local one. 

# It also adds the following arguments: 
# if the bin of the next peak is further than 3/4 of the resolution window or the bin of the next peak is inside the window of the current window peak 
#   do a gaussian fit 
# else use the hardcoded window that we know about 