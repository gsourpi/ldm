from matplotlib.ticker import EngFormatter
from matplotlib import pyplot as plt 
import numpy as np
import time 
import csv
from pathlib import Path
from scipy.sparse import lil_matrix
import os, sys
from Utilities.TDC import TDCGroupHits, TDCTimeHarpDecode
from Utilities.dataStorage import writeLists
import h5py
#file_path = (Path(__file__).parent / '../TimeHarp_260/Data').resolve()
#file_path = "/root/cernbox/Data/Data_PicoQuant/1sec-50PM.txt"
file_path = "/home/gsourpi/Desktop/git/ldm/10secs-10PM.txt"
words = []

if file_path.endswith('.txt'): 
   with open(file_path, "r") as file:
      for line in file: 
         words.append(int(line.strip()))
elif file_path.endswith('.csv'): 
   with open(file_path, "r", newline='') as file:
      csv_reader = csv.reader(file)
      for row in csv_reader:
         if row: 
            words.append(row[0])
elif file_path.endswith('.h5'): 
   with h5py.File(file_path, 'r') as hf:
      available_keys = list(hf.keys())
      print(f"Available keys are: ")
      for index, key in enumerate(available_keys): 
         print(f"{index + 1}. {key}")
      while 1: 
         try:
            choice = int(input("Enter the number of the key to open: "))
            if 1 <= choice <= len(available_keys):
               chosen_key = available_keys[choice - 1]
               break
            else:
               print("Invalid choice. Please enter a valid number.")
         except ValueError:
            print("Invalid input. Please enter a number.")
      histogram = hf[chosen_key][:]

# Decode the timetags while subtracting the synchronization ones from the hits
# and count occurrences for histogram bins 
histogram = np.zeros(int(1e9), dtype=int)
histogram = TDCTimeHarpDecode(histogram, words)
# Initial plot for reference  
non_zero_indices = np.nonzero(histogram)[0]
non_zero_counts = histogram[non_zero_indices]
plt.scatter(np.array(non_zero_indices), np.array(non_zero_counts))
plt.show(block = False)

# Set appropriate window size in bins 
binWindowSize = 100
# Group the bins into appropriate hits applying Gaussian Fit to the data 
hits_lists = TDCGroupHits(histogram, binWindowSize, binWindowStep=binWindowSize)

# Write lists to pkl file
writeLists(path ='crono_output1.pkl', list1 = histogram, list2 = hits_lists)
