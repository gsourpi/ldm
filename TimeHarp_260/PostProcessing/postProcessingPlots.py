import pickle
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
import numpy as np

def plotHist(ax, bins, preallocateCounts, plotGaussian=False):
    # Calculate the width of each bar
    bar_width = bins[1] - bins[0]
    
    # Convert bins(picosecs) in nanoseconds
    picoToNanoMultiplier = 25 * 1e-3 

    def custom_formatter(x, pos):
        return '{:.2f}'.format(x * picoToNanoMultiplier) # Multiply x by 25 and then by 10^(-3) for the label
    ax.xaxis.set_major_formatter(custom_formatter)
    
    # Plot the bars
    ax.bar(bins, preallocateCounts, width=bar_width, align='edge') # Add  color='blue' for consistent colouring 

def plotSomeOnSameGraph(data, block = False, startingHit = 0, endingHit = 4):
    plt.ion()
    fig, ax = plt.subplots(figsize=(8, 6))

    # No.1 : Plot each histogram on the same graph
    for i, bins in enumerate(data[1]):

        plotHist(ax, bins=bins, preallocateCounts=data[0][bins[0]:bins[-1] + 1], plotGaussian=False)
        if i == endingHit: 
            break 
    x_min = min(data[1][startingHit]) - 1
    x_max = max(data[1][endingHit]) + 1 

    plt.xlim(x_min, x_max)
    plt.ylim(0, max(data[0]))
    plt.show(block=block)

def plotNumSubPlots(data, block, startingHit = 2, endingHit = 5):
    # No.2 : Create subplots for each histogram
    plt.ion()
    fig, axs = plt.subplots(endingHit-startingHit+1, 1, figsize=(8, 6))

    # Plot each histogram
    for i, ax in enumerate(axs):
        bins = data[1][i + startingHit]  # Get bins for the current histogram
        plotHist(ax, bins=bins, preallocateCounts=data[0][bins[0]:bins[-1] + 1], plotGaussian=False)
        ax.grid(True)

    plt.show(block=block)

def plotAllOnSameGraph(data, block = True, TDC_RES = 25e-12): 
    xdata=list( range(0,len(data[0]) ) )
    xdata = np.array(xdata) * TDC_RES
    fig, ax = plt.subplots()
    formatter = EngFormatter(unit='s')
    ax.xaxis.set_major_formatter(formatter)
    ax.plot(xdata,data[0])
    
    plt.show(block = block)

###############################################################################################################
with open('egw.pkl', 'rb') as f:
    data = pickle.load(f)
    # Saved in a file in the form: [ [Counts], [ [Hit 1], [Hit 2], ...] ]

# print(len(data[1]))
# plotNumSubPlots(data, block = False, startingHit = 10, endingHit = 15)
# plotSomeOnSameGraph(data, block = False, startingHit = 10, endingHit = 15)
# plotAllOnSameGraph(data, block = True, TDC_RES =  2.5e-11)