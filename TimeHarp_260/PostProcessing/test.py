import random
import numpy as np
import matplotlib.pyplot as plt

def tdc_emulator(num_of_hist, bins, mean_separation, starting_mean, plot_hist=False, plot_bar=False): 
    counts = []

    def generate_mean_values(count, separation, start):
        result = [start + i * separation for i in range(count)]
        return result

    mean = generate_mean_values(num_of_hist, mean_separation, starting_mean)
    std = random.uniform(2, 4)  # Use a single standard deviation for all data points

    for mu in mean: 
        counts.append([random.gauss(mu, std) for _ in range(600)])

    def plot_histograms(counts, bins): 
        for i, count in enumerate(counts): 
            plt.hist(count, bins, alpha=0.5, label=f'{i} Count')

    if plot_hist: 
        plt.figure(1)
        plot_histograms(counts, bins)
        plt.legend(loc='upper right')
        plt.show(block=False)
        
    def create_histogram_data(counts, bins):
        histogram_data = np.zeros(len(bins) - 1, dtype=int)
        for data in counts:
            idx = np.searchsorted(bins, data, side='right') - 1
            for index in idx: 
                try: 
                    histogram_data[index] += 1
                except: 
                    continue
        return histogram_data
    
    histogram_counts = create_histogram_data(counts, bins)
    
    if plot_bar:
        plt.figure(2)
        plt.bar(bins[:-1], histogram_counts, width=np.diff(bins), align='edge')
        plt.xlabel('Bins')
        plt.ylabel('Frequency')
        plt.title('Histogram')
        plt.show(block=True)
        
    return histogram_counts, bins

HIST_TIME = 200
mean_separation = 10
starting_mean = random.randint(3, HIST_TIME // 2)

bins = np.linspace(0, HIST_TIME * 2, 100, dtype=int)

tdc_emulator(num_of_hist=15, bins=bins, mean_separation=mean_separation, starting_mean=starting_mean, plot_hist=True, plot_bar=True)
