import time 
import pickle 
import matplotlib
from matplotlib import pyplot as plt 
import numpy as np
from line_profiler import LineProfiler

file_path = "/home/gsourpi/Desktop/LDM/Data from PicoQuant/1sec-0PM.txt" 
# file_path = "/home/gsourpi/Desktop/LDM/0.1secs-0PM.txt"

# Define the bit masks 
specialBitMask = 0x80000000
channelMask = 0x7E000000
timeTagMask = 0x1FFFFFF

# Preallocate the bins and the counts 
preallocateCounts = [0] * int(1 * 1e8)
syncTimetag = 0 
binsOfCountPeaks = []
countThreshold = 10 #threshold so that maxima only from 100 counts and above will be considered -> other is considered noise
hits_lists =  []
global numHitsAfterSync
numHitsAfterSync = 0 


with open(file_path, 'r') as file:
    lines = file.readlines()

words = [int(line.strip()) for line in lines]

#@profile 
def writeLists(path, list1, list2): 
        print('Started writing to pkl file...')
        # Store list in binary format 
        with open(path, 'wb') as dD: 
            pickle.dump( (list1,list2), dD)
            print(f'\nDone writing lists to {path} file!')

def plotHist(ax, bins, preallocateCounts):

    rects = [plt.Rectangle((i - 0.5, 0), 1, preallocateCounts[i]) for i in bins]

    if not rects:
        print("No rectangles to plot.") 
        return
        
    for rect in rects:
        ax.add_patch(rect)
        
    ax.set_xlabel('Bins')
    ax.set_ylabel('Counts')

    xlim_min = min(bins) - 100 
    xlim_max = max(bins) + 100

    ax.set_xlim(xlim_min, xlim_max)
    ax.set_ylim(0, max(preallocateCounts)*1.1)


start_time = time.time()

for word in words: 

    # If special bit is set (1) and channel is 0 
    if (word & specialBitMask) != 0 and ( (word & channelMask) >>25) == 0: 
        
        # Update synchronization timetag
        syncTimetag = word & timeTagMask 
        numHitsAfterSync = 0
    
    # If special bit is clear (0)
    elif (word & specialBitMask == 0) and syncTimetag > 0: 
        current_abs =  ((word & timeTagMask) - syncTimetag)
        # In any case add regular measurement to counts of the corresponding bin
        preallocateCounts[ current_abs  ] += 1
        
if preallocateCounts: 
    indexes = np.where(preallocateCounts)[0] # the bins where the counts are non zero 
    binsWithCounts = list(indexes) #might not be necessary 

for bin in binsWithCounts:
    is_maxima = preallocateCounts[bin] > preallocateCounts[bin-1] and preallocateCounts[bin] > preallocateCounts[bin+1]
    if is_maxima and preallocateCounts[bin] > countThreshold: 
        binsOfCountPeaks.append(bin)

print("The number of hits are: ", len(binsOfCountPeaks) )
print(binsOfCountPeaks)
input()
print('\n')
print(binsWithCounts)
input()

for i, bin in enumerate(binsOfCountPeaks): 
# If it is the first peak 
    if i == 0: 
        hits_lists.append(binsWithCounts[0 : (binsOfCountPeaks[i+1] - binsOfCountPeaks[i]) //2])

    # If it is the last peak 
    elif i == (len(binsOfCountPeaks) - 1) :
        hits_lists.append(binsWithCounts[(binsOfCountPeaks[i] - binsOfCountPeaks[i-1]) //2 : len(binsWithCounts)-1])
        
    else: 
        print(binsOfCountPeaks[i])
        print(binsOfCountPeaks[i+1])
        print(binsOfCountPeaks[i-1])

        hits_lists.append([binsWithCounts[(binsOfCountPeaks[i]- binsOfCountPeaks[i-1])//2 : (binsOfCountPeaks[i+1] - binsOfCountPeaks[i]) //2]])

end_time = time.time()
print("\nElapsed time for decoding:", end_time - start_time, "seconds\n")

start_time = time.time()

writeLists(path ='decodedData.pkl', list1 = preallocateCounts, list2 = hits_lists)

end_time = time.time()
print("\nElapsed time for writting to pickle file:", end_time - start_time, "seconds\n")


input("Press Enter to Plot the Histogram of the first hit.")

with open('decodedData.pkl', 'rb') as f:
    data = pickle.load(f)

plt.ion()
fig, ax = plt.subplots()
plotHist(ax, data[1][0], preallocateCounts)

plt.show()
plt.pause(1000)


# This approach finds the local maxima in the bins that correspond to non-zero counts.
# Then it creates the lists of the hits based on the bin difference between the neighboring local maxima of the current local one. 

# For 1sec measurement time
# Decoding the data while abstracting synchronization timetags takes: 0.46206092834472656 seconds 
# Creating the list of hits takes: 2.196469783782959 seconds 
# = Total of approx. 2.7 secs
# Saving the data to pkl file takes: 0.8296682834625244 seconds. 
# = Total of approx 3.5 secs

# For 5 secs measurement time  
# Decoding the data while abstracting synchronization timetags takes: 2.2072343826293945 seconds 
# Creating the list of hits takes: 2.6029834747314453 seconds 
# = Total of approx. 4.9 secs
# Saving the data to pkl file takes: approx. 0.9 seconds. 
# = Total of approx 6 secs

# For 10 secs measurement time 
# Decoding the data while abstracting synchronization timetags takes: 4.473719120025635 seconds 
# Creating the list of hits takes: 3.629934787750244 seconds 
# = Total of approx. 8.1 secs
# Saving the data to pkl file takes: 1.3140099048614502 seconds. 
# = Total of approx 9.5 secs
