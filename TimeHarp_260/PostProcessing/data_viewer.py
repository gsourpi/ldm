import pickle
import os, sys
from pathlib import Path 
# Absolute path of the directory containing main.py
script_dir = os.path.dirname(os.path.abspath(__file__))

# Add the Utilities directory to the Python path
utilities_path = os.path.abspath(os.path.join(script_dir, 'Utilities'))
sys.path.append(utilities_path)

from Utilities.postProcessingPlots import plotHist, plotSomeOnSameGraph, plotAllOnSameGraph, plotNumSubPlots

file_path = (Path(__file__).parent / '../crono_output1.pkl').resolve()

with open(file_path, 'rb') as f:
    data = pickle.load(f)
    # Saved in a file in the form: [ [Counts], [ [Hit 1], [Hit 2], ...] ]
    
#print(len(data[1]))

#plotNumSubPlots(data, block = True, startingHit = 3, endingHit = 5)
#plotSomeOnSameGraph(data, block = True, startingHit = 1, endingHit = 3)
#plotAllOnSameGraph(data, block = True)