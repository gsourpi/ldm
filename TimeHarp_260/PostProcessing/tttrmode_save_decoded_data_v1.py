import time 
import pickle 
import matplotlib
from matplotlib import pyplot as plt 
import numpy 
from line_profiler import LineProfiler

file_path = "/home/gsourpi/Desktop/LDM/Data from PicoQuant/1sec-0PM.txt" 
# file_path = "/home/gsourpi/Desktop/LDM/0.1secs-0PM.txt"

# Define the bit masks 
specialBitMask = 0x80000000
channelMask = 0x7E000000
timeTagMask = 0x1FFFFFF

# Preallocate the bins and the counts 
preallocateCounts = [0] * int(1 * 1e8)
syncTimetag = 0 
countWord = 0 
numHits = 0 
syncTimetags = []
addedCounts = []

# Define initial window indices
window_start, window_end = 0, 0
numButtonPressed = -1
numBurstsForEveryHit = []
syncEvent = False
hits_lists = []

global numHitsAfterSync
numHitsAfterSync = 0 

global totalCounts 
totalCounts = 0 

regions = []


with open(file_path, 'r') as file:
    lines = file.readlines()

words = [int(line.strip()) for line in lines]

#@profile 
def writeLists(path, list1, list2): 
            print('Started writing to pkl file...')
            # Store list in binary format 
            with open(path, 'wb') as dD: 
                pickle.dump( (list1,list2), dD)
                print(f'\nDone writing lists to {path} file!')
                
def generate_alternating_list(center, length):
    result = [center]
    offset = 1
    for i in range(1, length):
        if i % 2 == 0 and (center - offset) >= 0:
            result.append(center - offset)
            offset += 1
        
        # If the number is negative and smaller than the maximum length of the array
        elif (center - offset) < 0 and (center + offset) < length - 1: 
                result.append(center + offset)
                offset += 1
        # if the number is bigger than the maximum array length and can get smaller without going out of bounds
        elif (center + offset) > length -1 and (center - offset) >= 0 :
            result.append(center - offset)
            offset +=1
        else:  
            result.append(center + offset)
    return result

def group(current_abs, regions):   
    global group_found  
    #if current_abs < len(preallocateCounts) and current_abs >= 0:
    if current_abs >= 0:

        group_found = False
        
        # print("hits_lists:", hits_lists)
        # print("numHitsAfterSync:", numHitsAfterSync)
        if len(hits_lists) >= (numHitsAfterSync + 1):

                for numOfHit in regions[numHitsAfterSync]:
                        # In case there is an existing bin with the current timetag, don't add a new hit in the group either a new group in general
                        if current_abs in hits_lists[numOfHit]: # Supposing that the hit number corresponds to the index of the hitlist. Ex. 1st hit will belong to the 1st index - smaller hit time. 
                            group_found = True
                            break
                        
                        elif numOfHit != len(hits_lists)-1 and current_abs < min(hits_lists[numOfHit + 1]) and current_abs >= min(hits_lists[numOfHit]):
                            hits_lists[numOfHit].append(current_abs)
                            group_found = True
                            # print(f"\n Bin difference inside the window! -> {diff}")
                            # print(f"\n Timetag diff between current and one in hit list is: {current_abs - prev_abs}")
                            break 
                        
                        # If the current bin is smaller than the smallest of the bins in the first hit group, add it there. 
                        elif current_abs < min(hits_lists[0]):
                            hits_lists[0].append(current_abs)
                            group_found = True
                            break
                        
                        elif numOfHit == len(hits_lists) and current_abs > min(hits_lists[len(hits_lists) -1]): 
                            hits_lists[len(hits_lists)-1].append(current_abs)
                            group_found = True 
                            break 
                        
                        else: 
                            # print(f"The bin {current_abs} didn't belong to group number {numHitsAfterSync}\n")
                            # print(f"Loop continues to hit number {numOfHit}...")
                            continue
                        
        # If it is bigger than the last hit group add it there. 
        if not group_found and len(hits_lists) <= 256:
            #print(f" Not in group: {current_abs}")
            # Create a new hit list group
            hits_lists.append([current_abs])
        
def plotHist(ax, bins, preallocateCounts):
    
    rects = [plt.Rectangle((i - 0.5, 0), 1, preallocateCounts[i]) for i in bins]
    
    if not rects:
        print("No rectangles to plot.") 
        return
        
    for rect in rects:
        ax.add_patch(rect)
        
    ax.set_xlabel('Bins')
    ax.set_ylabel('Counts')

    xlim_min = min(bins) - 100 
    xlim_max = max(bins) + 100

    ax.set_xlim(xlim_min, xlim_max)
    ax.set_ylim(0, max(preallocateCounts)*1.1)
    

start_time = time.time()

for word in words: 
    
        # If special bit is set (1) and channel is 0 
        if (word & specialBitMask) != 0 and ( (word & channelMask) >>25) == 0: 
            
            # Update synchronization timetag
            syncTimetag = word & timeTagMask 
            syncTimetags.append(syncTimetag)
            
            numHitsAfterSync = 0
        
        # If special bit is clear (0)
        elif (word & specialBitMask == 0) and syncTimetag > 0: 
            totalCounts += 1
            current_abs =  ((word & timeTagMask) - syncTimetag)
            
            prev = len(hits_lists)
            
            group(current_abs, regions)
            # if the len after the group has changed create a new region list
            if prev != len(hits_lists): 
                regions = []
                for i in range(0,len(hits_lists)):
                    region = generate_alternating_list(i, len(hits_lists) - 1)
                    regions.append(region)
            
            # In any case add regular measurement to counts of the corresponding bin
            preallocateCounts[ current_abs  ] += 1
            numHitsAfterSync += 1
            
            
end_time = time.time()
print("\nElapsed time for decoding:", end_time - start_time, "seconds\n")

start_time = time.time()
#writeLists(path ='decodedData_v1.pkl', list1 = preallocateCounts, list2 = hits_lists)

end_time = time.time()
print("\nElapsed time for writting to pickle file:", end_time - start_time, "seconds\n")


# input("Press Enter to Plot the Histogram of the first hit.")

with open('decodedData_v1.pkl', 'rb') as f:
    # Load the data from the pickled file
    data = pickle.load(f)

plt.ion()
fig, ax = plt.subplots()
plotHist(ax, data[1][2], preallocateCounts)

plt.show()
plt.pause(1000)

# This approach considers that every time a word is not part of the sync timetag belongs to a certain group. 
# For example, if we have 120 timetags for each synchronization, then we have 120 hits. 
# This is not the case as each of these "hits" has many sub-hits inside it as we can see from the plot. 

# Decoding and grouping to hits takes 7.225682020187378 seconds while writting to a pkl file takes 0.6308410167694092 seconds.
# Total = 7.9 seconds