import h5py
import numpy as np
from matplotlib import pyplot as plt
import struct
import argparse
from matplotlib.ticker import EngFormatter

TDC_RES = 25e-12  # in seconds 
TRIGGER_PERIOD = 1/11240 # in seconds 

def bin_data(data, original_resolution, target_resolution):
   """
   Bin the histogram data to the target resolution.

   Parameters:
   data (list): Original histogram data.
   original_resolution (float): Original resolution of the data in seconds.
   target_resolution (float): Desired resolution of the data in seconds.

   Returns:
   tuple: Binned histogram and corresponding xdata (time) values.
   """
   bins_per_target_bin = int(target_resolution / original_resolution)
   
   aggregated_data = [
      sum(data[i:i + bins_per_target_bin])
      for i in range(0, len(data), bins_per_target_bin)
   ]
   
   # remove the last bin if it has fewer than bins_per_target_bin counts
   if len(data) % bins_per_target_bin != 0:
      aggregated_data = aggregated_data[:-1]
   
   xdata = np.array(range(len(aggregated_data))) * target_resolution
   
   return aggregated_data, xdata

def main(data_file, save, target_resolution):
   with h5py.File(data_file, 'r') as hf:
      available_keys = list(hf.keys())
      
      print("Enter the date and time in the format 'DD_MM_YYYY_hh_mm_ss'. For example: '07_07_2024_050001'.")
      while True:
         chosen_key = input()
         if chosen_key in available_keys:
               break
         else:
               print("No data available for this date and/or time. Please enter valid info.")
      
      binary_data_group = hf[chosen_key]
      binary_data_lists = list(binary_data_group.keys())
      
      data_lists = {
         'full_histogram': binary_data_group['full_histogram'][:].tobytes(),
         'segmented_histogram': binary_data_group['segmented_histogram'][:].tobytes(),
         'hit_rate_per_second': binary_data_group['hit_rate_per_second'][:].tobytes(),
         'hit_rate_per_trigger': binary_data_group['hit_rate_per_trigger'][:].tobytes()
      }

      print("Available data lists:")
      for i, key in enumerate(data_lists.keys(), 1):
         print(f"{i}. {key}")
      
      print("Enter the number(s) of the data lists to be plotted (comma-separated for multiple) or 'all' to plot everything: ")
      while True:
         chosen_data = input().strip()
         if chosen_data.lower() == 'all':
               chosen_data_keys = list(data_lists.keys())
               break
         else:
               chosen_data_indices = [int(x) for x in chosen_data.split(',') if x.strip().isdigit()]
               if all(1 <= index <= len(data_lists) for index in chosen_data_indices):
                  chosen_data_keys = [list(data_lists.keys())[i - 1] for i in chosen_data_indices]
                  break
               else:
                  print("Invalid selection. Please enter valid numbers or 'all'.")

      fig, axs = plt.subplots(1, len(chosen_data_keys), figsize=(10 * len(chosen_data_keys), 6))
      if len(chosen_data_keys) == 1:
         axs = [axs]

      for ax, key in zip(axs, chosen_data_keys):
         binary_data = data_lists[key]
         num_bins = len(binary_data) // 4 # as binary_data are in 32-bit integers
         data = list(struct.unpack(f'{num_bins}I', binary_data))
         
         if key == 'full_histogram' or key == 'segmented_histogram': 
            ydata, xdata = bin_data(data, original_resolution= TDC_RES, target_resolution= target_resolution)
         elif key == 'hit_rate_per_second':
            ydata = data 
            xdata = np.arange(len(data))
         elif key == 'hit_rate_per_trigger': 
            ydata = data
            xdata = np.arange(len(data))
            
         formatter = EngFormatter(unit='s')
         ax.xaxis.set_major_formatter(formatter)
         
         ax.set_xlabel('Time')
         #ax.set_ylabel('Count')
         ax.set_title(f'Plot of: {key}')
         ax.plot(xdata, ydata)
      
      if save:
         plt.savefig(f'{chosen_key}.png')
         print(f"Plot of {chosen_key} saved successfully.")
      plt.show()

if __name__ == "__main__":
   parser = argparse.ArgumentParser(description="Process and save data from an HDF5 file.")
   parser.add_argument('--data_file', type=str, required=True, help='Path to the data file (expected .h5 file)')
   parser.add_argument('--save', action='store_true', default=False, help='Flag to save the extracted plot.')
   parser.add_argument('--target_resolution', type=float, default=TDC_RES, help='Resolution for rebinning the data (in seconds).')

   args = parser.parse_args()
   main(args.data_file, args.save, args.target_resolution)