import argparse
import time
import h5py
import os

def copy_group(source_group, target_group):
    for key in source_group.keys():
        item = source_group[key]
        if isinstance(item, h5py.Group):
            target_subgroup = target_group.create_group(key)
            copy_group(item, target_subgroup)
        else:
            target_group.create_dataset(key, data=item[:], compression='gzip', compression_opts=9)

def main(source, target):
    start_time = time.time()

    with h5py.File(source, 'r') as source_h5:
        group_name = list(source_h5.keys())[0]        
        data_to_copy = source_h5[group_name]

        with h5py.File(target, 'a') as target_h5:
            if group_name in target_h5:
                del target_h5[group_name]
            
            target_group = target_h5.create_group(group_name)
            copy_group(data_to_copy, target_group)

    print(f"Successfully copied group '{group_name}' from '{source}' to '{target}'")
    print(f"Went from {os.path.getsize(source)/1e6} Mbytes into {os.path.getsize(target)/1e6} Mbytes with level 9 gzip compression.")
    print(f"Time to copy and compress the dataset: {time.time() - start_time} secs.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Copy and compress the only group from one HDF5 file to another.')
    parser.add_argument('--source_file', help='Path to the source HDF5 file', required=True)
    parser.add_argument('--target_file', help='Path to the target HDF5 file', required=True)
    args = parser.parse_args()

    main(args.source_file, args.target_file)
