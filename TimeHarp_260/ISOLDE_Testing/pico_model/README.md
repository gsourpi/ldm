# ISOLDE Testing Code

This repository contains scripts for data acquisition and data viewing for the ISOLDE testing setup.

## Overview

The ISOLDE testing code is designed to facilitate data acquisition and analysis. It includes scripts for initializing the Time-to-Digital Converter (TDC), acquiring data, and viewing the acquired data with options for rebinning.

## Components

### data_acquisition.py

This script is responsible for acquiring data from the TDC. It performs the following functions:
- Generates full and segmented histograms as well as hit rate per trigger and per second.
- Saves the results in a temporary HDF5 file under a datetime group. The datetime group is named using the format 'DD_MM_YYYY_hh_mm_ss'. For example: '07_07_2024_05_00_01'.

Within each datetime group, the following datasets are stored:
```
|-- DD_MM_YYYY_hh_mm_ss
    |-- full_histogram
    |-- segmented_histogram
    |-- hit_rate_per_second
    |-- hit_rate_per_trigger
```
### data_acquisition.sh

This script initializes the TDC and runs the data acquisition process. It performs the following tasks:
- Initializes the TDC.
- Executes `data_acquisition.py`.
- Copies and compresses the datetime group and its datasets from the temporary HDF5 dataset to a permanent HDF5 file containing all the data.

### data_viewer.py

This script is used for viewing the acquired data. It includes the following features:
- Displays user-chosen data stored in the HDF5 file.
- Provides arguments for rebinning the data to the desired resolution.

## Usage

To acquire data, follow these steps:

1. Ensure that the TDC is properly connected and configured.
2. Go to the 'ldm/TimeHarp_260/ISOLDE_Testing' directory and run:
   ```sh
   sudo ./data_acquisition.sh
   ```

For viewing specific data lists simply run: 

```
sudo python data_viewer.py --data_file BSRL_data.h5 --target_resolution 2e-9
```
