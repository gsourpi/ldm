#!/bin/bash

ISOLDE_FOLDER="$PWD"

cd $ISOLDE_FOLDER/../driver

if lsmod | grep -q th260pcie; then
    echo "Kernel module th260pcie is loaded. Continuing..."
else
    echo "Kernel module th260pcie is not loaded. Loading..."
    insmod th260pcie.ko

    if [ $? -ne 0 ]; then
    echo "Error: Failed to load kernel module th260pcie."
    exit 1
    else
    echo "Kernel module th260pcie loaded successfully."
    fi
fi
